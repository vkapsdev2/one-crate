import Vue from "vue";
import VueRouter from "vue-router";
import vbclass from "vue-body-class";
import store from "./store/index";
import Home from "./pages/Home";
import Login from "./components/LoginComponent";
import Landing from "./pages/Landing";
import ResumeBuilder from "./pages/ResumeBuilder";
import Offers from "./pages/Offers";
import Candidates from "./pages/Candidates";
import Members from "./pages/Members";
import Subscriptions from "./pages/Subscriptions";
import Pricing from "./pages/Pricing";
import Invoice from "./pages/Invoice";
import Support from "./pages/Support";
import Changeemail from "./pages/Changeemail";
import Welcome from "./pages/Congrats";
import Changepassword from "./pages/Changepassword";
import AboutUs from "./pages/AboutUs";
import Terms from "./pages/Terms";
import PrivacyPolicy from "./pages/PrivacyPolicy";
import CookiePolicy from "./pages/CookiePolicy";
import ContactUs from "./pages/ContactUs";
import JobDetail from "./pages/JobDetail";
import JobTemplateDetail from "./pages/JobTemplateDetail";
import ISOP from "./pages/ISOP";
import CompanyMessage from "./pages/CompanyMessage";
import CandidateISOP from "./pages/CandidateISOP";
import JobTemplate from "./pages/JobTemplate";
import Examples from "./pages/Examples";
import ForgotPassword from "./pages/ForgotPassword";
import ResetPassword from "./pages/ResetPassword";
import Payment from "./pages/Payment";
import CompanyDetail from "./pages/CompanyDetail";
import Resubscribe from "./pages/Resubscribe";
import InvalidMembership from "./pages/InvalidMembership";
import VerifyAccount from "./pages/VerifyAccount";
import ActivateAccount from "./pages/ActivateAccount";
import BlankIsop from "./pages/BlankIsop";
import CandidatesLanding from "./pages/CandidatesLanding";
import CandidateISOPLanding from "./pages/CandidateISOPLanding.vue";
import CompaniesLanding from "./pages/CompaniesLanding";
import CompanyISOPLanding from "./pages/CompanyISOPLanding.vue";
import ResumeBuilderPricing from "./pages/ResumeBuilderPricing.vue";
import InvoiceResumePro from "./pages/InvoiceResumePro.vue";

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "landing",
            component: Landing,
            meta: {
                bodyClass: "template-landing",
                notLoggedIn: true,
                title: "OneCrate"
            },
            children: [
                // {
                //     path: '/login',
                //     name: 'login',
                //     component: Login,
                //     meta: {
                //         bodyClass: 'template-login',
                //         notLoggedIn: true,
                //         title: 'Login'
                //     }
                // },
            ]
        },
        {
            path: "/login",
            name: "login",
            component: Login,
            meta: {
                bodyClass: "template-login",
                notLoggedIn: true,
                title: "Login"
            }
        },
        {
            path: "/home",
            name: "home",
            component: Home,
            meta: {
                bodyClass: "template-home",
                requiresAuth: true,
                title: "OneCrate"
            }
        },
        {
            path: "/forgotPassword",
            name: "forgotPassword",
            component: ForgotPassword,
            meta: {
                bodyClass: "template-has-footer",
                notLoggedIn: true,
                title: "Forgot Password"
            }
        },
        {
            path: "/verifyAccount",
            name: "verifyAccount",
            component: VerifyAccount,
            props: true,
            meta: {
                bodyClass: "template-has-footer",
                notLoggedIn: true,
                title: "Verify Account"
            }
        },
        {
            path: "/activateAccount/:token",
            name: "activateAccount",
            component: ActivateAccount,
            meta: {
                bodyClass: "template-has-footer",
                notLoggedIn: true,
                title: "Activate Account"
            }
        },
        {
            path: "/resetPassword/:token",
            name: "resetPassword",
            component: ResetPassword,
            meta: {
                bodyClass: "template-has-footer",
                notLoggedIn: true,
                title: "Reset Password"
            }
        },
        {
            path: "/landing-candidate",
            name: "candidatesLanding",
            component: CandidatesLanding,
            meta: {
                bodyClass: "template-landing-candidate inner-page",
                requiresAuth: false,
                requiresCandidate: false,
                title: "Candidates Landing"
            }
        },
        {
            path: "/landing-candidate-isop",
            name: "CandidateISOPLanding",
            component: CandidateISOPLanding,
            meta: {
                bodyClass: "template-landing-candidate-isop inner-page",
                requiresAuth: false,
                requiresCandidate: false,
                title: "Candidates ISOP Landing"
            }
        },
        {
            path: "/landing-company",
            name: "companiesLanding",
            component: CompaniesLanding,
            meta: {
                bodyClass: "template-landing-company inner-page",
                requiresAuth: false,
                requiresCandidate: false,
                title: "Companies Landing"
            }
        },
        {
            path: "/landing-company-isop",
            name: "companyISOPLanding",
            component: CompanyISOPLanding,
            meta: {
                bodyClass: "template-landing-company-isop inner-page",
                requiresAuth: false,
                requiresCandidate: false,
                title: "Companies ISOP Landing"
            }
        },
        {
            path: "/resume",
            name: "resume",
            component: ResumeBuilder,
            meta: {
                bodyClass: "template-resume",
                requiresAuth: true,
                requiresCandidate: true,
                title: "Resume"
            }
        },
        {
            path: "/offers",
            name: "offers",
            component: Offers,
            meta: {
                bodyClass: "template-offers",
                requiresAuth: true,
                requiresCandidate: true,
                title: "Offers"
            }
        },

        {
            path: "/candidates",
            name: "candidates",
            component: Candidates,
            meta: {
                bodyClass: "template-candidates",
                requiresAuth: true,
                //requiresCandidate:true,
                title: "Candidates"
            }
        },
        {
            path: "/members",
            name: "members",
            component: Members,
            meta: {
                bodyClass: "template-members",
                requiresAuth: true,
                //requiresCandidate:true,
                title: "Members"
            }
        },
        {
            path: "/subscription",
            name: "subscription",
            component: Subscriptions,
            meta: {
                bodyClass: "template-subscription",
                requiresAuth: true,
                //requiresCandidate:true,
                title: "Subscription"
            }
        },
        {
            path: "/pricing",
            name: "pricing",
            component: Pricing,
            meta: {
                bodyClass: "template-pricing",
                requiresAuth: true,
                //requiresCandidate:true,
                title: "Pricing"
            }
        },
        {
            path: "/resume-builder-pro-pricing",
            name: "resume-builder-pro-pricing",
            component: ResumeBuilderPricing,
            meta: {
                bodyClass: "template-pricing",
                requiresAuth: true,
                //requiresCandidate:true,
                title: "Pricing"
            }
        },
        {
            path: "/invoice",
            name: "invoice",
            component: Invoice,
            meta: {
                bodyClass: "template-invoice",
                requiresAuth: true,
                //requiresCandidate:true,
                title: "Invoice"
            }
        },
        {
            path: "/support",
            name: "support",
            component: Support,
            meta: {
                bodyClass: "template-support",
                requiresAuth: true,
                //requiresCandidate:true,
                title: "Support"
            }
        },
        {
            path: "/jobs",
            name: "jobs",
            component: JobTemplate,
            meta: {
                bodyClass: "template-support",
                requiresAuth: true,
                requiresCompany: true,
                title: ""
            }
        },
        {
            path: "/example",
            name: "example",
            component: Examples,
            meta: {
                bodyClass: "template-support",
                requiresAuth: true,
                //requiresCandidate:true,
                title: ""
            }
        },
        {
            path: "/changeemail",
            name: "changeemail",
            component: Changeemail,
            meta: {
                bodyClass: "template-changeemail",
                requiresAuth: true,
                //requiresCandidate:true,
                title: "Change Email"
            }
        },
        {
            path: "/welcome",
            name: "welcome",
            component: Welcome,
            meta: {
                bodyClass: "template-welcome",
                requiresAuth: false,
                //requiresCandidate:true,
                title: "Welcome"
            }
        },
        {
            path: "/changepassword",
            name: "changepassword",
            component: Changepassword,
            meta: {
                bodyClass: "template-changepassword",
                requiresAuth: true,
                //requiresCandidate:true,
                title: "Change Password"
            }
        },
        {
            path: "/aboutus",
            name: "aboutus",
            component: AboutUs,
            meta: {
                bodyClass: "template-aboutus",
                requiresAuth: false,
                title: "About Us"
            }
        },
        {
            path: "/terms",
            name: "terms",
            component: Terms,
            meta: {
                bodyClass: "template-terms",
                requiresAuth: false,
                title: "Terms"
            }
        },
        {
            path: "/privacypolicy",
            name: "privacypolicy",
            component: PrivacyPolicy,
            meta: {
                bodyClass: "template-privacypolicy",
                requiresAuth: false,
                title: "Privacy Policy"
            }
        },
        {
            path: "/cookiepolicy",
            name: "cookiepolicy",
            component: CookiePolicy,
            meta: {
                bodyClass: "template-cookiepolicy",
                requiresAuth: false,
                title: "Cookie Policy"
            }
        },
        {
            path: "/contactus",
            name: "contactus",
            component: ContactUs,
            meta: {
                bodyClass: "template-contactus",
                requiresAuth: false,
                title: "Contact Us"
            }
        },
        {
            path: "/jobDetail/:id",
            name: "jobDetail",
            component: JobDetail,
            meta: {
                bodyClass: "template-jobDetail",
                requiresAuth: false,
                title: "Job Detail"
            }
        },

        {
            path: "/jobTemplateDetail/:id",
            name: "jobTemplateDetail",
            component: JobTemplateDetail,
            meta: {
                bodyClass: "template-jobDetail",
                requiresAuth: false,
                title: "Job Detail"
            }
        },

        {
            path: "/companyDetail/:id",
            name: "companyDetail",
            component: CompanyDetail,
            meta: {
                bodyClass: "template-companyDetail",
                requiresAuth: false,
                title: "Company Detail"
            }
        },

        {
            path: "/isop",
            name: "isop",
            component: ISOP,
            meta: {
                bodyClass: "template-isop",
                requiresAuth: true,
                requiresCompany: true,
                title: "ISOP"
            }
        },
        {
            path: "/message",
            name: "message",
            component: CompanyMessage,
            meta: {
                bodyClass: "template-chat",
                requiresAuth: true,
                requiresCompany: true,
                title: "Message"
            }
        },
        {
            path: "/cisop",
            name: "cisop",
            component: CandidateISOP,
            meta: {
                bodyClass: "template-isop",
                requiresAuth: true,
                title: "ISOP"
            }
        },
        {
            path: "/noisop",
            name: "noisop",
            component: BlankIsop,
            meta: {
                bodyClass: "template-noisop",
                requiresAuth: true,
                title: "ISOP"
            }
        },

        {
            path: "/stripetest",
            name: "stripetest",
            component: Payment,
            meta: {
                bodyClass: "template-payment",
                requiresAuth: true,
                requiresCompany: true,
                title: "Payment"
            }
        },
        {
            path: "/upgrade/:id/:cycleid",
            name: "upgrade",
            component: Invoice,
            meta: {
                bodyClass: "template-upgrade",
                requiresAuth: true,
                requiresCompany: true,
                title: "Upgrade Plan"
            }
        },
        {
            path: "/downgrade/:id/:cycleid",
            name: "downgrade",
            component: Invoice,
            meta: {
                bodyClass: "template-upgrade",
                requiresAuth: true,
                requiresCompany: true,
                title: "Downgrade Plan"
            }
        },
        {
            path: "/pay-resume-builder/:id/",
            name: "pay-resume-builder",
            component: InvoiceResumePro,
            meta: {
                bodyClass: "template-upgrade",
                requiresAuth: true,
                title: "Upgrade Plan"
            }
        },
        {
            path: "/resubscribe",
            name: "resubscribe",
            component: Resubscribe,
            meta: {
                bodyClass: "template-upgrade",
                requiresAuth: true,
                requiresCompany: false,
                title: "Resubscribe Package"
            }
        },
        {
            path: "/invalidMembership",
            name: "invalidMembership",
            component: InvalidMembership,
            meta: {
                bodyClass: "template-upgrade",
                requiresAuth: true,
                requiresCompany: true,
                title: "Invalid Membership"
            }
        }
    ],
    scrollBehavior() {
        window.scrollTo(0, 0);
    }
});
router.afterEach((to, from) => {
    if (
        store.getters.userData &&
        store.getters.userData.tier &&
        store.getters.userData.tier.payment_status != "paid" &&
        (to.name == "isop" || to.name == "candidates")
    ) {
        router.push("/resubscribe");
        return;
    }
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (to.matched.some(record => record.meta.requiresCandidate)) {
            if (store.getters.isCandidate && store.getters.isLoggedIn) {
                next();
                return;
            } else {
                next("/");
            }
        } else if (to.matched.some(record => record.meta.requiresCompany)) {
            if (store.getters.isCompany && store.getters.isLoggedIn) {
                next();
                return;
            } else {
                next("/");
            }
        } else if (store.getters.isLoggedIn) {
            next();
            return;
        }
        next("/");
    } else if (to.matched.some(record => record.meta.notLoggedIn)) {
        // if user already logged in redirect to home from /login
        if (store.getters.isLoggedIn) {
            next("/home");
            return;
        }
        next();
    } else {
        next();
    }
});
export default router;

Vue.use(VueRouter);
Vue.use(vbclass, router);
