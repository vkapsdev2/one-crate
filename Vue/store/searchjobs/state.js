export default {
    status: '',
    token: localStorage.getItem('_token') || '',
    userData : {},
    candidateData:{},
    companyData:{},
    isCandidate:false,
    isCompany:false
}
