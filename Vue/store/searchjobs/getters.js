export default {
    isLoggedIn: state => !!state.token,
    isCandidate: state => state.isCandidate,
    isCompany: state => state.isCompany,
    authStatus: state => state.status,
    userData: state => state.userData,
    candidateData: state => state.candidateData,
    companyData: state => state.companyData,
}
