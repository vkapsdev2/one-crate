
import Vue from 'vue'
import Vuex from 'vuex'
import User from './user'
import Resume from './resume'
import Candidate from './candidate'
import createPersistedState from "vuex-persistedstate";
import SecureLS from "secure-ls";
const ls = new SecureLS({ isCompression: false });
// import Common from './common'
Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [
        createPersistedState({
            storage: {
                getItem: key => ls.get(key),
                setItem: (key, value) => ls.set(key, value),
                removeItem: key => ls.remove(key)
            }
        })
    ],
    modules: {
        User,
        Resume,
        Candidate
    }
})
