import axios from 'axios';
import config from '../../config/config';
import serviceAPI from '../../api';

export default {
    //Articles
    getArticles({ commit }) {
        return new Promise((resolve, reject) => {
            serviceAPI.GET('getArticles')
                .then(resp => {
                    commit('update_articles', resp);
                    resolve();
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })
        });
    },
    getArticle({ }, articleId) {
        return new Promise((resolve, reject) => {
            serviceAPI.GET('showArticle/' + articleId)
                .then(resp => {
                    resolve(resp);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })
        });
    },

    //Favorite
    showFavourites({ commit }) {
        return new Promise((resolve, reject) => {
            serviceAPI.GET('getBookmarks/')
                .then(resp => {
                    commit('update_favorites', { favorites: resp });
                    resolve(resp);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })
        });
    },
    onRemoveFavourites({ commit }, transData) {
        return new Promise((resolve, reject) => {
            serviceAPI.POST(transData.url, transData.data)
                .then(resp => {
                    commit('update_favorites', { favorites: resp });
                    resolve(resp);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })
        });
    },
    onViewJob({ commit }, transData) {
        return new Promise((resolve, reject) => {
            serviceAPI.GET(transData.url)
                .then(resp => {
                    resolve(resp);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })
        });
    },
    onViewCompanyInfo({ commit }, transData) {
        return new Promise((resolve, reject) => {
            serviceAPI.GET(transData.url)
                .then(resp => {
                    resolve(resp);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })
        });
    },

    updateFavorite({ commit }, favorites) {
        commit('update_favorites', { favorites });
    },
    setFavorites({ commit }, data) {
        return new Promise((resolve, reject) => {
            serviceAPI.POST('saveFavourites', data)
                .then(resp => {
                    const favorites = resp;
                    commit('update_favorites', { favorites });
                    resolve(resp)
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    update_appliedJobs({ commit }, appliedJobsList) {
        commit('update_appliedJobs', { appliedJobsList });
    },
    update_onLineJobs({ commit }, onLineJobsList) {
        commit('update_onLineJobs', { onLineJobsList });
    },
    setAppliedJobs({ commit }, data) {
        return new Promise((resolve, reject) => {
            serviceAPI.POST('applyJob', data)
                .then(resp => {
                    const appliedJobsList = resp;
                    commit('update_appliedJobs', { appliedJobsList });
                    resolve(resp)
                })
                .catch(err => {
                    reject(err)
                })
        })
    },

    updateJobOffer({commit}, jobOfferList){
        commit('update_totalOffer',{jobOfferList});
    },
    getJobOffer({commit}, data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'getCandidateOffersList', data: data, method: 'POST' })
                .then(resp => {
                    const jobOfferList = resp.data;
                    commit('update_totalOffer',{jobOfferList});
                    resolve(resp.data)
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
}
