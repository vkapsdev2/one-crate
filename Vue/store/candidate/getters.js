export default {
    articles: state => state.articles,
    article: state => articleId => state.articles.find(article => article.id === articleId),
    favorites: state => state.favorites,
    appliedJobsList: state => state.appliedJobsList,
    onLineJobsList: state => state.onLineJobsList,
    jobOfferList: state => state.jobOfferList,
}
