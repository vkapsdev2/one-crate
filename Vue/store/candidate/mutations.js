export default {
    update_articles(state, articles) {
        state.articles = articles;
    },
      
    update_favorites(state,favorites){
        state.favorites = favorites.favorites
    },

    update_appliedJobs(state,appliedJobsList) {
      state.appliedJobsList = appliedJobsList.appliedJobsList;
    },

    update_onLineJobs(state,onLineJobsList) {
        state.onLineJobsList = onLineJobsList.onLineJobsList;
      },

    update_totalOffer(state,jobOfferList) {
        state.jobOfferList = jobOfferList.jobOfferList ;
    },
}
