export default {
    isLoggedIn: state => !!state.token,
    isCandidate: state => state.isCandidate,
    isCompany: state => state.isCompany,
    authStatus: state => state.status,
    userData: state => state.userData,
    userTour: state => state.userTour,
    candidateData: state => state.candidateData,
    companyData: state => state.companyData,
    popus: state => state.popus,
}
