export default {
    auth_request(state) {
        state.status = 'loading'
    },
    auth_success(state, { token, user }) {
        state.status = 'success'
        state.token = token
        state.userData = user
        state.userTour = user.tour_status
        state.candidateData = user.candidate
        state.companyData = user.company
        state.isCandidate = !!user.candidate
        state.isCompany = !!user.company
    },
    auth_error(state) {
        state.status = 'error'
        state.token = ''
    },
    logout(state) {
        state.status = ''
        state.token = ''
        state.data = []
        state.userData = []
        state.userTour = []
        state.candidateData = []
        state.companyData = []

    },
    update_user(state, user) {
        state.userData = user
        state.userTour = user.tour_status
        state.candidateData = user.candidate
        state.companyData = user.company
        state.isCandidate = !!user.candidate
        state.isCompany = !!user.company
    },

    addChatPopup(state, data) {
        state.popus = data;
    }
}
