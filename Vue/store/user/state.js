export default {
    status: '',
    token: localStorage.getItem('_token') || '',
    userData: {},
    userTour: {},
    candidateData: {},
    companyData: {},
    isCandidate: false,
    isCompany: false,
    popus: []
}
