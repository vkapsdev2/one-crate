import axios from 'axios';
import config from '../../config/config';
export default {
    login({ commit }, user) {
        return new Promise((resolve, reject) => {
            commit('auth_request')
            axios({ url: config.apiURL + 'login', data: user, method: 'POST' })
                .then(resp => {
                    const token = resp.data.token
                    const user = resp.data.user
                    localStorage.setItem('_token', token)
                    localStorage.setItem('userInfo', JSON.stringify(user))
                    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token

                    commit('auth_success', { token, user })
                    resolve(resp.data)
                })
                .catch(err => {
                    commit('auth_error');
                    localStorage.clear();
                    reject(err)
                })
        })
    },
    logout({ commit }) {
        return new Promise((resolve, reject) => {
            axios({ url: config.apiURL + 'logout', method: 'GET' })
                .then(resp => {
                    commit('logout');
                    localStorage.clear();
                    delete axios.defaults.headers.common['Authorization'];
                    resolve(true);
                })
                .catch(err => {
                    commit('logout');
                    localStorage.clear();
                    delete axios.defaults.headers.common['Authorization'];
                    reject(err);
                })

        })
    },
    fetchCurrentUser({ commit }) {
        return new Promise((resolve, reject) => {
            axios({ url: config.apiURL + 'getCurrentUser', method: 'GET' })
                .then(resp => {
                    commit('update_user', resp.data.user);
                    localStorage.clear();
                    resolve(true);
                })
                .catch(err => {
                    localStorage.clear();
                    reject(err);
                })
        })
    },
    updateUser({ commit }, user) {
        commit('update_user', user)
        // return new Promise((resolve, reject) => {
        //     // commit('update_user')
        //     axios({url: config.apiURL + 'getUserData/' + user_id, method: 'GET' })
        //         .then(resp => {
        //             const user = resp.data.user
        //
        //             resolve(resp.data)
        //         })
        //         .catch(err => {
        //
        //             reject(err)
        //         })
        // })
    },
    addChatPopup({ commit }, data) {
        commit('addChatPopup', data);
    }
}
