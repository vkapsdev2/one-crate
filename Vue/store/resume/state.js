export default {
    status: '',
    objectiveSummary : {},
    contactInfo : {},
    currentExpertise : {},
    education : {},
    achievements : {},
    workExperience : {},
    video : {},
    skills : {},
    strengths : {},
    languages : {},
    interests : {},
    references : {},
    isResumePro : false
}
