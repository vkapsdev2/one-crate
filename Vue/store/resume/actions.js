import axios from 'axios';
import config from '../../config/config';
export default {
    setObjectiveSummary({commit}, data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setObjectiveSummary', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('set_objective_summary', responseData);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    setContactInfo({commit},data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setContactInfo', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('update_user', responseData)
                    //commit('set_contact_info', responseData);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    setCurrentExpertise({commit},data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setCurrentExpertise', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('set_current_expertise', responseData);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    setEducation({commit},data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setResumeEducation', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('set_education', responseData);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    setAchievements({commit},data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setResumeAchievement', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('set_achievements', responseData);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    setWorkExperience({commit},data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setResumeWorkExperience', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('set_workExperience', responseData);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    set_video({commit},data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setResumeVideo', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('set_video', data);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    setSkills({commit},data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setResumeSkill', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('set_skills', responseData);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    setStrengths({commit},data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setResumeStrength', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('set_strengths', responseData);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    setLanguages({commit},data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setResumeLanguage', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('set_languages', responseData);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    setInterests({commit},data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setResumeInterest', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('set_interests', responseData);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    setReferences({commit},data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setResumeReference', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('set_references', responseData);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    setIsResumePro({commit},data){
        return new Promise((resolve, reject) => {
            axios({url: config.apiURL + 'setResumePro', data: data, method: 'POST' })
                .then(resp => {
                    const responseData = resp.data;
                    commit('set_isResumePro', responseData);
                    resolve(resp.data);
                })
                .catch(err => {
                    reject(err)
                })
        })
    }
}
