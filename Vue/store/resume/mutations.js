export default {
    set_objective_summary(state, data){
        state.objectiveSummary = data
    },
    set_contact_info(state,data){
        state.contactInfo = data
    },
    set_current_expertise(state,data){
        state.currentExpertise = data
    },
    set_education(state,data){
        state.education = data
    },
    set_achievements(state,data){
        state.achievements = data
    },
    set_workExperience(state,data){
        state.workExperience = data
    },
    set_video(state,data){
        state.video = data
    },
    set_skills(state,data){
        state.skills= data
    },
    set_strengths(state,data){
        state.strengths= data
    },
    set_languages(state,data){
        state.languages= data
    },
    set_interests(state,data){
        state.interests = data
    },
    set_references(state,data){
        state.references = data
    },
    set_isResumePro(state,data){
        state.isResumePro = data
    }
}
