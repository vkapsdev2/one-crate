export default {
    objectiveSummary : state => state.objectiveSummary,
    contactInfo : state => state.contactInfo,
    currentExpertise : state => state.currentExpertise,
    education : state => state.education,
    achievements : state => state.achievements,
    workExperience : state => state.workExperience,
    video : state => state.video,
    skills : state => state.skills,
    strengths : state => state.strengths,
    languages : state => state.languages,
    interests : state => state.interests,
    references : state => state.references,
    isResumePro : state => !!state.isResumePro

}
