/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vuelidate from 'vuelidate';
import vSelect from 'vue-select';
import Notifications from 'vue-notification';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import Axios from 'axios';
import store from './store/index';
import Routes from './routes';
import App from './layouts/App';
import Autocomplete from 'v-autocomplete';
import 'v-autocomplete/dist/v-autocomplete.css'
import 'viewerjs/dist/viewer.css';
import Viewer from 'v-viewer';
import api from './api';
import VueMq from 'vue-mq';
import moment from 'moment';
import VueSVGIcon from 'vue-svgicon';
import TextareaAutosize from 'vue-textarea-autosize';
import VTooltip from 'v-tooltip';
import Geocoder from "@pderas/vue2-geocoder";
import VueTour from 'vue-tour';
import Vue2Filters from 'vue2-filters';
import AOS from 'aos';
import VEmojiPicker from 'v-emoji-picker';
import config from "./config/config";
import * as io from "socket.io-client";
import VueSocketIO from "vue-socket.io-extended";
import * as VueGoogleMaps from 'vue2-google-maps'

require('./bootstrap');

window.Vue = require('vue');

Vue.use(VueSocketIO, io(config.socketUrl));
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(Vuelidate);
Vue.use(vSelect);
Vue.use(Notifications);
Vue.use(Autocomplete);
Vue.use(Viewer);
Vue.use(TextareaAutosize)
Vue.use(VTooltip);
Vue.use(VueTour);
Vue.use(Vue2Filters)
Vue.use(AOS);
Vue.config.productionTip = false;
Vue.use(VEmojiPicker);

Vue.prototype.$http = Axios;
Vue.prototype.$api = api;

Vue.use(VueMq, {
    breakpoints: {
        mobile: 767,
        desktop: Infinity,
    }
})
Vue.use(VueSVGIcon, {
    tagName: 'icon'
})
const token = localStorage.getItem('_token');
if (token) {
    Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Bearer ' + token
}
axios.interceptors.response.use(
    (response) => {
        return response
    },
    (error) => {
        const originalRequest = error.config
        // token expired
        if (error.response.status === 403) {
            store.dispatch('logout');
            router.push('/');
        }
        return Promise.reject(error)
    }
);

Vue.use(Geocoder, {
    defaultCountryCode: null, // e.g. 'CA'
    defaultLanguage: null, // e.g. 'en'
    defaultMode: 'address', // or 'lat-lng'
    googleMapsApiKey: 'AIzaSyCWy6BmHWkJ9SDz0p_l4dMEsKnAhZsy25Y'
});

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyCWy6BmHWkJ9SDz0p_l4dMEsKnAhZsy25Y',
        libraries: 'places', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)

        //// If you want to set the version, you can do so:
        // v: '3.26',
    },

    //// If you intend to programmatically custom event listener code
    //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
    //// you might need to turn this on.
    // autobindAllEvents: false,

    //// If you want to manually install components, e.g.
    //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
    //// Vue.component('GmapMarker', GmapMarker)
    //// then disable the following:
    // installComponents: true,
})


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//vuejs filters
Vue.filter('formatDateAgo', (value) => {
    if (value) {
        return moment.utc(value).local().fromNow();
        // return moment(value).fromNow();
    }
});

Vue.filter('getDateOnly', (value) => {
    if (value) {
        return moment.utc(value).local().format("MMMM Do, YYYY");
        // return moment(value).format("MMMM Do, YYYY");
    }
});

Vue.filter('getDateYear', (value) => {
    if (value) {
        return moment.utc(value).local().year();
        // return moment(value).year();
    }
});

Vue.filter('getLocation', (value) => {
    if (!value)
        return '';
    let location = "";
    if (value.city) {
        location += value.city.name + ", ";
    }
    if (value.state) {
        location += value.state.name + ", ";
    }
    if (value.country) {
        location += value.country.name;
    }
    return location;
});

Vue.filter('getReferenceLocation', (value) => {
    if (!value)
        return '';
    let location = "";
    if (value.state) {
        location += value.state.name + ", ";
    }
    if (value.country) {
        location += value.country.name;
    }
    return location;
});

Vue.filter('salaryTerm', (value) => {
    let salaryTerm = "";
    if (value == 'Annually') {
        salaryTerm += "Annual";
    } else {
        salaryTerm = value;
    }

    return salaryTerm;
});


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router: Routes,
    store,
    render: h => h(App),
    created() {
        AOS.init({
            disable: 'mobile'
        });
    },
    watch: {
        '$route'(to, from) {
            document.title = to.meta.title || 'OneCrate';
            AOS.refresh();
        }
    },
});
