import Swal from 'sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'; 

const Success = (title, text) => {
    Swal.fire({
        title: title,
        text: text,
        icon: "success",

        html: "<br>" +
        '<button class="btn btn-primary SwalBtn1">' + 'Ok' + '</button>',
        showCancelButton: false,
        showConfirmButton: false,
        onOpen: function (dObj) {
            $('.SwalBtn1').on('click',function () {
                Swal.close();
            });
        }
    });
    return true;
}
const CustomSuccess = (title, text, btnText, func1) => {
    let confirmFlag = false;
    Swal.fire({
        title: title,
        text: text,
        icon: "success",

        html: "<br>" +
        '<button class="btn btn-primary SwalBtn1">' + btnText + '</button>',
        showCancelButton: false,
        showConfirmButton: false,
        onOpen: function (dObj) {
            $('.SwalBtn1').on('click',function () {
                confirmFlag = true;
                Swal.close();
                func1();
            });
        }
    })
   
}
const Warning = (title, text) => {
    Swal.fire({
        title: title,
        text: text,
        icon: "warning",

        html: "<br>" +
        '<button class="btn btn-primary SwalBtn1">' + 'Ok' + '</button>',
        showCancelButton: false,
        showConfirmButton: false,
        onOpen: function (dObj) {
            $('.SwalBtn1').on('click',function () {
                Swal.close();
            });
        }
    });
    return true;
}
const Info = (title, text) => {
    Swal.fire({
        title: title,
        text: text,
        icon: "info",

        html: "<br>" +
        '<button class="btn btn-primary SwalBtn1">' + 'Ok' + '</button>',
        showCancelButton: false,
        showConfirmButton: false,
        onOpen: function (dObj) {
            $('.SwalBtn1').on('click',function () {
                Swal.close();
            });
        }
    });
    return true;
}
const Error = (title, text) => {
    Swal.fire({
        title: title,
        text: text,
        icon: "error",

        html: "<br>" +
        '<button class="btn btn-primary SwalBtn1">' + 'Ok' + '</button>',
        showCancelButton: false,
        showConfirmButton: false,
        onOpen: function (dObj) {
            $('.SwalBtn1').on('click',function () {
                Swal.close();
            });
        }
    });
    return true;
}
const Confirm = (title, text, func1, func2) => {
    let confirmFlag = false;
    Swal.fire({
        title: title,
        text: text,
        icon: "info",

        html: "<br>" +
        '<button class="btn btn-primary SwalBtn1">' + 'Yes' + '</button>' +
        '<button class="btn btn-danger ml-5 SwalBtn2">' + 'No' + '</button>',
        showCancelButton: false,
        showConfirmButton: false,
        onOpen: function (dObj) {
            $('.SwalBtn1').on('click',function () {
                confirmFlag = true;
                Swal.close();
                func1();
            });
            $('.SwalBtn2').on('click',function () {
                confirmFlag = false;
                Swal.close();
                func2();
            });
        }
    })
   
}

export default {
    Success,
    CustomSuccess,
    Warning,
    Info,
    Error,
    Confirm,
}