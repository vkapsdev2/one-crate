<?php

namespace App\Http\Controllers;

use App\Resume;
use App\ResumeReference;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResumeReferenceController extends Controller
{
    /**
     * add resume reference.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeReference|array
     */
    public function setResumeReference(Request $request)
    {

        Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'organization' => ['required', 'string', 'max:255'],
            'reference_email' => ['required', 'email', 'max:255'],
            'reference_phone' => ['required', 'string', 'max:255'],
            'reference_type_id_value' => ['required'],
            'reference_country_id' => ['required'],
        ])->validate();

        $resume = Resume::where('user_id', Auth::user()->id)->first();
        if (!$resume) {
            return ['error' => 'resume does not exist'];
        }

        $resumeReference = new ResumeReference();
        $resumeReference->resume_id = $resume->id;
        $resumeReference->name = $request->name;
        $resumeReference->organization = $request->organization;
        $resumeReference->reference_type_id = $request->reference_type_id_value;
        $resumeReference->country_id = $request->reference_country_id;
        if ($request->reference_email) {
            $resumeReference->email = $request->reference_email;
        }
        if ($request->reference_phone) {
            $resumeReference->phone = $request->reference_phone;
        }
        if ($request->reference_state_id) {
            $resumeReference->state_id = $request->reference_state_id;
        }
        if ($request->reference_city_id) {
            $resumeReference->city_id = $request->reference_city_id;
        }
        $resumeReference->save();

        $resumeReference = ResumeReference::with(['type', 'country', 'state', 'city'])->where('resume_id', $resume->id)
            ->orderBy('sort_order', 'asc')->get();
        return $resumeReference;

    }

    public function getResumeReference()
    {
        $resume = Resume::where('user_id', Auth::user()->id)->first();
        if ($resume) {
            $resumeReference = ResumeReference::with(['type', 'country', 'state', 'city', 'resume'])->where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();
            //dd($resumeReference);
            return $resumeReference;
        } else {
            return response()->json(['errors' => ['msg' => ['Resume Does Not Exist.']]], 500);
        }
    }

    /**
     * update resume reference.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeReference|array
     */
    public function updateResumeReference(Request $request)
    {

        Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'organization' => ['required', 'string', 'max:255'],
            'reference_email' => ['required', 'email', 'max:255'],
            'reference_phone' => ['required', 'string', 'max:255'],
            'reference_type_id_value' => ['required'],
            'reference_country_id' => ['required'],
        ])->validate();
        $resumeReference = ResumeReference::find($request->id);
        $resume = Resume::where('user_id', Auth::user()->id)->first();
        if (!$resume) {
            return ['error' => 'resume does not exist'];
        }
        if ($resumeReference && $resume) {
            $resumeReference->resume_id = $resume->id;
            $resumeReference->name = $request->name;
            $resumeReference->organization = $request->organization;
            $resumeReference->reference_type_id = $request->reference_type_id_value;
            $resumeReference->country_id = $request->reference_country_id;
            if ($request->reference_email) {
                $resumeReference->email = $request->reference_email;
            }
            if ($request->reference_phone) {
                $resumeReference->phone = $request->reference_phone;
            }
            if ($request->reference_state_id) {
                $resumeReference->state_id = $request->reference_state_id;
            }
            if ($request->reference_city_id) {
                $resumeReference->city_id = $request->reference_city_id;
            }
            $resumeReference->save();
            $resumeReference = ResumeReference::with(['type', 'country', 'state', 'city'])->where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeReference;

        } else {
            return null;
        }

    }

    public function deleteReference($id)
    {
        $resumeReference = ResumeReference::find($id);
        if ($resumeReference) {
            $resume = Resume::where('user_id', Auth::user()->id)->first();
            $resumeReference->delete();

            $resumeReference = ResumeReference::with(['type', 'country', 'state', 'city'])->where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeReference;

        } else {
            return null;
        }

    }

    /**
     * update resume reference.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeReference|array
     */
    public function sortResumeReference(Request $request)
    {

        $resume = Resume::where('user_id', Auth::user()->id)->first();
        if ($resume) {
            $sortedarray = $request->sortedarray;
            foreach ($sortedarray as $key => $value) {
                $resumeReference = ResumeReference::find($value['id']);
                $resumeReference->resume_id = $resume->id;
                $resumeReference->sort_order = $value['sort_order'];
                $resumeReference->save();
            }
            $resumeReference = ResumeReference::with(['type', 'country', 'state', 'city'])->where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeReference;

        } else {
            return null;
        }

    }
}
