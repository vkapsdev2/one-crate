<?php

namespace App\Http\Controllers;

use App\Company;
use App\OrganizationChart;
use App\User;
use App\UserTier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Image;

class UserController extends Controller
{

    public function checkUserSubscription()
    {
        $user = Auth::user();
        $company = Company::where('user_id', $user->id)->first();
        $userTier = UserTier::with(['subscriptionTierNode'])->where('user_id', $user->id)->first();
        //dd($userTier);
        $userDetails = User::with([
            'company.businessType', 'company.industry', 'company.memberDetails', 'company.profiles',
            'company.department', 'company.jobs', 'company.charts', 'candidate.fieldOfStudy.blogCategory',
            'candidate.companyDetails', 'candidate.job', 'candidate.jobApplications', 'roles', 'country',
            'state', 'city', 'tier.subscriptionTierNode',
        ])->where('id', $user->id)->first();
        $userDetails->full_name = $userDetails->first_name . " " . $userDetails->last_name;
        if ($company) {
            $userChartsCount = OrganizationChart::where('company_id', $company->id)->get()->count();

            if ($userTier) {
                if ($userTier->payment_status != 'paid') {
                    return response()->json(['error' => 'Payment Not Received.', 'user' => $userDetails], 501);
                }
                if ($userChartsCount > $userTier->subscriptionTierNode->amount) {
                    return response()->json(['error' => 'Tier Limit Exceeded.', 'user' => $userDetails], 501);
                }
            } else if ($userChartsCount > env('FREE_NODES_AMOUNT', 10)) {
                return response()->json(['error' => 'Tier Limit Exceeded.', 'user' => $userDetails], 501);
            }
        } else {
            return response()->json(['error' => 'Not a company.'], 401);

        }
        return $userDetails;
    }

    /**
     * add resume education.
     *
     * @param \Illuminate\Http\Request $request
     * @return User|array
     * @throws \HttpException
     */
    public function updateTourStatus(Request $request)
    {
        $user = User::where('id', Auth::user()->id)->first();
        $user->tour_status = 1;
        $user->save();

        $user = User::with(['company.businessType', 'company.industry', 'company.memberDetails', 'company.profiles', 'company.department', 'company.jobs', 'company.charts',
            'candidate.fieldOfStudy.blogCategory', 'candidate.companyDetails', 'candidate.job', 'candidate.jobApplications', 'roles', 'country', 'state', 'city'])
            ->where('id', Auth::user()->id)->first();
        $user->full_name = $user->first_name . " " . $user->last_name;
        return $user;
    }

    /**
     * add resume education.
     *
     * @param \Illuminate\Http\Request $request
     * @return User|array
     * @throws \HttpException
     */
    public function setContactInfo(Request $request)
    {
        Validator::make($request->all(), [
            'mobile_number' => ['required'],
            'country_id' => ['required'],
            'state_id' => ['required'],
        ])->validate();

        $user = User::where('id', Auth::user()->id)->first();
        if (!$user) {
            return response()->json(['errors' => ['msg' => ['user does not exist.']]], 401);
        }

        $user->personal_email = $request->personal_email;
        $user->mobile_number = $request->mobile_number;
        $user->phone_number = $request->phone_number;
        $user->zip_code = $request->zip_code;
        $user->country_id = $request->country_id;
        $user->state_id = $request->state_id;
        $user->city_id = $request->city_id;
        $user->address = $request->address;
        $user->save();

        $user = User::with(['company.businessType', 'company.industry', 'company.memberDetails', 'company.profiles', 'company.department', 'company.jobs', 'company.charts',
            'candidate.fieldOfStudy.blogCategory', 'candidate.companyDetails', 'candidate.job', 'candidate.jobApplications', 'roles', 'country', 'state', 'city'])
            ->where('id', Auth::user()->id)->first();
        $user->full_name = $user->first_name . " " . $user->last_name;
        return $user;
    }

    public function getUserData($user_id)
    {
        $user = User::with([
            'company.businessType', 'company.industry', 'company.memberDetails', 'company.profiles', 'company.department', 'company.jobs', 'company.charts',
            'candidate.fieldOfStudy.blogCategory', 'candidate.companyDetails', 'candidate.job', 'candidate.jobApplications', 'roles', 'country', 'state', 'city',
        ])
            ->where('id', $user_id)->first();
        return response()->json([
            'user' => $user,
        ]);
    }

    public function getCurrentUser()
    {
        $user = User::with([
            'company.businessType', 'company.industry', 'company.memberDetails', 'company.profiles', 'company.department', 'company.jobs', 'company.charts',
            'candidate.fieldOfStudy.blogCategory', 'candidate.companyDetails', 'candidate.job', 'candidate.jobApplications', 'roles', 'country', 'state', 'city',
        ])
            ->where('id', Auth::user()->id)->first();
        return response()->json([
            'user' => $user,
        ]);
    }

    public function saveImage(Request $request)
    {
        $path = public_path('images/userImages/');
        $imgpath = Str::uuid()->toString() . '.png';
        File::isDirectory($path) or File::makeDirectory($path, 0775, true, true);
        $img = Image::make($request->file)->save($path . $imgpath);
        if ($img) {
            $user = User::with([
                'company.businessType', 'company.industry', 'company.memberDetails', 'company.profiles', 'company.department', 'company.jobs', 'company.charts',
                'candidate.fieldOfStudy.blogCategory', 'candidate.companyDetails', 'candidate.job', 'candidate.jobApplications', 'roles', 'country', 'state', 'city',
            ])
                ->where('id', Auth::user()->id)
                ->firstOrFail();
            $user->image = config('app.asset_path_user') . $imgpath;
            $user->save();

            return $user;
        }
    }

    public function getProfileImage()
    {

        $user = User::where('id', Auth::user()->id)->firstOrFail();
        $imgpath = $user->image;
        // $userImage = config('app.asset_path_user') . $imgpath;

        return $imgpath;
    }

    public function updateEmail(Request $request)
    {
        Validator::make($request->all(), [
            'userCurrentEmail' => ['required', 'email'],
            'userNewEmail' => ['required', 'email'],
            'userConfirmNewEmail' => ['required', 'email'],
        ])->validate();
        $user = User::where('id', Auth::user()->id)->first();
        $currentEmail = User::where('email', $request->userCurrentEmail)->first();
        if ($request->userNewEmail != $request->userConfirmNewEmail) {
            return response()->json(['errors' => ['msg' => ['New email and confirm email not matched.']]], 500);
        }
        if (!$user) {
            return response()->json(['errors' => ['msg' => ['user does not exist.']]], 401);
        }
        if (!$currentEmail || $user->email != $request->userCurrentEmail) {
            return response()->json(['errors' => ['msg' => ['Current email does not exist.']]], 500);
        }
        $user->email = $request->userNewEmail;
        $user->save();
        return $user;
    }

    public function changePassword(Request $request)
    {
        Validator::make($request->all(), [
            'current_password' => ['required', 'string', 'min:8'],
            'new_password' => ['required', 'string', 'min:8', 'confirmed'],
        ])->validate();
        $user = User::where('id', Auth::user()->id)->first();
        $checkPassword = Hash::check($request->current_password, $user->password);
        if (!$user) {
            return response()->json(['errors' => ['msg' => ['user does not exist.']]], 401);
        }
        if (!$checkPassword) {
            return response()->json(['errors' => ['msg' => ['Invalid Current Password.']]], 500);
        }
        $user->password = Hash::make($request->new_password);
        $user->save();
        return $user;
    }

    public function checkEmail(Request $request)
    {
        $currentEmail = User::where('email', $request->email)->first();
//dd($currentEmail);
        if ($currentEmail === null) {
            return response()->json(['errors' => ['msg' => ['Email does not exist.']]], 500);
        } else {
            return null;
        }

    }

}
