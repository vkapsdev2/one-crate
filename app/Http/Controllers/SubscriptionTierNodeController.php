<?php

namespace App\Http\Controllers;

use App\OrganizationChart;
use App\PaymentCycle;
use App\PaymentOption;
use App\SubscriptionTier;
use App\SubscriptionTierNode;
use App\User;
use App\UserPaymentDetails;
use App\UserPaymentCards;
use App\UserTier;
use Cartalyst\Stripe\Stripe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubscriptionTierNodeController extends Controller
{
    public function getSubscriptionTierNodeDetails($id, $cycleid)
    {
        $stripe = new Stripe(env('STRIPE_API_KEY'), env('STRIPE_API_VERSION'));
        $subscriptionDetails = SubscriptionTierNode::where('id', $id)->first();
        $subscriptionTier = SubscriptionTier::where('name', 'growth')->first();
        $paymentCycle = PaymentCycle::where('id', $cycleid)->first();

        //check if the plan is upgrade not downgrade
        $currentTier = UserTier::with('subscriptionTier', 'subscriptionTierNode')->where('user_id', Auth::user()->id)->first();
        if ($currentTier && $subscriptionDetails) {
            // if ($currentTier->subscriptionTierNode->amount === $subscriptionDetails->amount) {
            //     return response()->json(['errors' => 'Your request cannot be processed right now'], 500);
            // }
        }
        if ($paymentCycle) {
            if ($paymentCycle->name == 'monthly') {
                $subscriptionDetails->actualAmount = $subscriptionDetails->price;
                $subscriptionDetails->payableAmount = $subscriptionDetails->price;
                $subscriptionDetails->payableCycle = 'monthly';
                $subscriptionDetails->payableDiscount = 0;
            } else {
                $total = 0;
                $discount = (($subscriptionDetails->price * 12) * 20) / 100;
                $total = ($subscriptionDetails->price * 12) - $discount;
                $subscriptionDetails->actualAmount = $subscriptionDetails->price * 12;
                $subscriptionDetails->payableAmount = (int)$total;
                $subscriptionDetails->payableCycle = 'yearly';
                $subscriptionDetails->payableDiscount = (int)$discount;
            }
        }

        $subscriptionDetails->subscriptionTier = $subscriptionTier;

        $userId = Auth::user()->id;
        $userPaymentDetails = UserPaymentDetails::where('user_id', $userId)->first();
        if ($userPaymentDetails) {
            //creating payment intent for the package amount
            $paymentIntent = $stripe->paymentIntents()->create([
                'customer' => $userPaymentDetails->customer_id,
                'amount' => $subscriptionDetails->payableAmount,
                'currency' => 'usd',
                'payment_method_types' => [
                    'card',
                ],
            ]);

            $cardList = UserPaymentCards::where('user_id', $userId)->get()->toArray();

            if ($paymentIntent) {
                $subscriptionDetails->client_secret =  $paymentIntent['client_secret'];
                $subscriptionDetails->payment_intent =  $paymentIntent;
                $subscriptionDetails->cardList =  $cardList;
            }
        } else {
            $userEmail = Auth::user()->email;
            $userFullName = Auth::user()->first_name . " " . Auth::user()->last_name;

            //creating a new customer for the user on STRIPE
            $customer = $stripe->customers()->create([
                'email' => $userEmail,
                'name'  => $userFullName,
            ]);

            if ($customer) {
                //saving customer id into user payment options
                $paymentOption = PaymentOption::where('name', 'stripe')->first();
                $userPaymentDetails = new UserPaymentDetails();
                $userPaymentDetails->user_id = $userId;
                $userPaymentDetails->payment_option_id = $paymentOption->id;
                $userPaymentDetails->customer_id = $customer['id'];
                $userPaymentDetails->save();

                //creating payment intent for the package amount
                $paymentIntent = $stripe->paymentIntents()->create([
                    'customer' => $userPaymentDetails->customer_id,
                    'amount' => $subscriptionDetails->payableAmount,
                    'currency' => 'usd',
                    'payment_method_types' => [
                        'card',
                    ],
                ]);
                if ($paymentIntent) {
                    $subscriptionDetails->client_secret =  $paymentIntent['client_secret'];
                    $subscriptionDetails->cardList =  [];
                }
            }
        }
        return $subscriptionDetails;
    }

    public function getResubscriptionDetails()
    { // fixme
        $user = Auth::user();
        $userTier = UserTier::where('user_id', $user->id)->first();
        if ($userTier) {
            $paymentDetails = $this->getSubscriptionTierNodeDetails($userTier->subscription_tier_node_id, $userTier->payment_cycle_id);
            $cardList = UserPaymentCards::where('user_id', $user->id)->get()->toArray();
            $paymentDetails->cardList =  $cardList;
            return $paymentDetails;
        } else {
            return response()->json(['errors' => 'payment details does not exist'], 500);
        }
    }

    public function getSubscriptionTierNodesList()
    {
        $user = Auth::user();
        $user = User::with('company')->where('id', $user->id)->first();
        $userTier = UserTier::with('subscriptionTier', 'subscriptionTierNode')->where('user_id', $user->id)->first();
        $userTotalCurrentNodes = OrganizationChart::where('company_id', $user->company->id)->get()->count();
        if ($userTier) {
            // $subscriptionTierNodes = SubscriptionTierNode::where('amount','>=',$userTier -> subscriptionTierNode->amount)->get();
            $subscriptionTierNodes = SubscriptionTierNode::where('amount', '!=', 0)->get();
            $data['userTierId'] = $userTier['id'];
            $data['subscriptionTierNodes'] = $subscriptionTierNodes;
            $data['current_plan'] = $userTier->subscriptionTier->name;
            $data['current_nodes'] = $userTier->subscriptionTierNode->amount;
            $data['user_total_current_nodes'] = $userTotalCurrentNodes;
            $data['user_total_current_price'] = $userTier->subscriptionTierNode->price;
            $data['user_current_max_positions'] = $userTier->subscriptionTierNode->amount;
            return $data;
        } else {
            $subscriptionTierNodes = SubscriptionTierNode::all();
            $data['subscriptionTierNodes'] = $subscriptionTierNodes;
            $data['current_plan'] = "free";
            $data['current_nodes'] = 0;
            $data['user_total_current_nodes'] = $userTotalCurrentNodes;
            $data['user_total_current_price'] = 0;
            $data['user_current_max_positions'] = 10;
            return $data;
        }
    }
}
