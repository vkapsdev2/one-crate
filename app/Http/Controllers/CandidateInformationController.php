<?php

namespace App\Http\Controllers;

use App\CandidateInformation;
use Illuminate\Http\Request;

class CandidateInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CandidateInformation  $candidateInformation
     * @return \Illuminate\Http\Response
     */
    public function show(CandidateInformation $candidateInformation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CandidateInformation  $candidateInformation
     * @return \Illuminate\Http\Response
     */
    public function edit(CandidateInformation $candidateInformation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CandidateInformation  $candidateInformation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CandidateInformation $candidateInformation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CandidateInformation  $candidateInformation
     * @return \Illuminate\Http\Response
     */
    public function destroy(CandidateInformation $candidateInformation)
    {
        //
    }
}
