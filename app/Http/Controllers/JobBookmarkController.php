<?php

namespace App\Http\Controllers;

use App\JobBookmark;
use App\Candidate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Str;
use const http\Client\Curl\AUTH_ANY;

class JobBookmarkController extends Controller
{
    public function getBookmarks(){
        $candidate = Candidate::where('user_id',Auth::user()->id)->first();
        //dd($candidate->id);
        $candidate_id = $candidate->id;
        $bookmarks = JobBookmark::with(['job.title','job'=>function($query){
            $query = $query->select('id','job_title_id','qualification_id','company_id','country_id','state_id',
                'city_id')->groupBy('job_title_id');
        },'candidate','job.qualification','job.country','job.state','job.city','job.company.user'
        ])->where('candidate_id',$candidate_id)->get();
        return $bookmarks;
    }

    /**
     * Save Favourites in database.
     */
    public function saveFavourites(Request $request){
        $candidate = Candidate::where('user_id',Auth::user()->id)->first();
        $alreadyBookmarked = JobBookmark::where('job_id',$request->job_id)->where('candidate_id',$request->candidate_id)->first();
        if(!$alreadyBookmarked) {
            $candidate_id = $candidate->id;
            $saveBookmark = new JobBookmark();
            $saveBookmark->job_id = $request->job_id;
            $saveBookmark->candidate_id = $candidate_id;
            $saveBookmark->save();
        }
        return $this->getBookmarks();

    }

    /**
     * Remove Favourites in database.
     */
    public function removeFavourites(Request $request){
        $candidate = Candidate::where('user_id',Auth::user()->id)->first();

        if(isset($request->byJob) && $request->byJob){
            $bookmark = JobBookmark::where('job_id',$request -> id)->delete();
        }else{
            $bookmark = JobBookmark::where('id',$request -> id)->delete();
        }
        return $this->getBookmarks();

    }
}
