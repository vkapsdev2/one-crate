<?php

namespace App\Http\Controllers;

use App\Mail\InviteMembers;
use App\MemberImportList;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;

class MemberImportListController extends Controller
{
    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }

    public function inviteMembers(Request $request)
    {
        $user = Auth::user();
        $user = User::with(['company'])->where('id',$user->id)->first();
        $csvData = $this->csvToArray($request->file);
        if($csvData) {
            foreach ($csvData as $member) {
                 $uuid = (string) Str::uuid();
                 $invitation = new MemberImportList();
                 $invitation -> company_id = $user->company->id;
                 $invitation -> email = $member['email'];
                 $invitation -> job_title = $member['job_title'];
                 $invitation -> request_token = $uuid;
                 $invitation -> save();

                 //send invitation mail
                $invitation->url = URL::to('/acceptInvitation/'.$invitation->request_token);
                $data['invitation'] = $invitation;
                Mail::to($invitation -> email)->send(new InviteMembers($data));
            }
        }
        return response()->json(['msg' => ['Invitations Sent.']], 200);
    }
}
