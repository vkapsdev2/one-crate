<?php

namespace App\Http\Controllers;

use App\Resume;
use App\ResumeAchievement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResumeAchievementController extends Controller
{
    /**
     * get resume education.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeAchievement|array
     */
    public function getResumeAchievements(){
        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if($resume) {
            $resumeEducation = ResumeAchievement::with(['country', 'state', 'city'])->where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeEducation;
        }else{
            return response()->json(['errors' => ['msg' => ['Resume Does Not Exist.']]], 500);
        }
    }

    /**
     * add resume achievements.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeAchievement|array
     */
    public function setResumeAchievement(Request $request){

        Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:255'],
            'institution' => ['required', 'string', 'max:255'],
            //'description' => ['required', 'string',],
            'achievements_completion_year_value' => ['required'],
            'achievements_completion_month_value' => ['required'],
            'achievements_country_id_value' => ['required'],
        ])->validate();

        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }

        $resumeAchievement = new ResumeAchievement();
        $resumeAchievement -> resume_id = $resume ->id;
        $resumeAchievement -> title = $request ->title;
        $resumeAchievement -> institution = $request ->institution;
        $resumeAchievement -> completion_year = $request -> achievements_completion_year_value;
        $resumeAchievement -> completion_month = $request -> achievements_completion_month_value;
        $resumeAchievement -> country_id = $request -> achievements_country_id_value;

        if($request->description){
            $resumeAchievement -> description = $request -> description;
        }
        if($request->achievements_state_id){
            $resumeAchievement -> state_id = $request -> achievements_state_id;
        }
        if($request->achievements_city_id){
            $resumeAchievement -> city_id = $request -> achievements_city_id;
        }
        $resumeAchievement -> save();

        $resumeAchievement = ResumeAchievement::with(['country','state','city'])->where('resume_id',$resume->id)
            ->orderBy('sort_order', 'asc')->get();
        return $resumeAchievement;
    }


    public function deleteAchievement($id){
        $resumeEducation = ResumeAchievement::find($id);
        if($resumeEducation){
            $resume = Resume::where('user_id',Auth::user()->id)->first();
            $resumeEducation -> delete();

            $resumeEducation = ResumeAchievement::with(['country','state','city'])->where('resume_id',$resume ->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeEducation;

        }else{
            return null;
        }

    }

    /**
     * update resume achievements.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeAchievement|array
     */
    public function updateAchievement(Request $request){

        Validator::make($request->all(), [
            'id' => ['required'],
            'title' => ['required', 'string', 'max:255'],
            'institution' => ['required', 'string', 'max:255'],
            'achievements_completion_year_value' => ['required'],
            'achievements_completion_month_value' => ['required'],
            'achievements_country_id_value' => ['required'],
        ])->validate();

        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }

        $resumeAchievement = ResumeAchievement::find($request->id);
        if($resumeAchievement){

            $resumeAchievement -> resume_id = $resume ->id;
            $resumeAchievement -> title = $request ->title;
            $resumeAchievement -> institution = $request ->institution;
            $resumeAchievement -> completion_year = $request -> achievements_completion_year_value;
            $resumeAchievement -> completion_month = $request -> achievements_completion_month_value;
            $resumeAchievement -> country_id = $request -> achievements_country_id_value;

            if($request->description){
                $resumeAchievement -> description = $request -> description;
            }
            if($request->achievements_state_id){
                $resumeAchievement -> state_id = $request -> achievements_state_id;
            }
            if($request->achievements_city_id){
                $resumeAchievement -> city_id = $request -> achievements_city_id;
            }
            $resumeAchievement -> save();

            $resumeAchievement = ResumeAchievement::with(['country','state','city'])->where('resume_id',$resume->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeAchievement;
        }else{
            return ['error' => 'resume achievement does not exist'];
        }
    }

    /**
     * update resume achievements sort order.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeAchievement|array
     */
    public function sortAchievement(Request $request){

        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if($resume){
            $sortedarray = $request->sortedarray;
        foreach ($sortedarray as $key => $value) {
            $resumeAchievement = ResumeAchievement::find($value['id']);
            if ($resumeAchievement) {
                $resumeAchievement->resume_id = $resume->id;
                $resumeAchievement->sort_order = $value['sort_order'];
                $resumeAchievement->save();
            }
        }
                $resumeAchievement = ResumeAchievement::with(['country', 'state', 'city'])->where('resume_id', $resume->id)
                    ->orderBy('sort_order', 'asc')->get();
                return $resumeAchievement;
            } else {
                return ['error' => 'resume achievement does not exist'];
            }
    }
}
