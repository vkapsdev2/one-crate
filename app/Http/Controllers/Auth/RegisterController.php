<?php

namespace App\Http\Controllers\Auth;

use JWTAuth;
use App\Candidate;
use App\Company;
use App\CompanyProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterCandidate;
use App\Mail\RegisterCompany;
use App\Mail\ActivateAccount;
use App\OrganizationChart;
use App\Resume;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if($data['user_type'] == 'candidate'){
            return Validator::make($data, [
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'country_id' => ['required'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'candidate_agreement' => ['accepted'],
            ]);
        }else if ($data['user_type'] == 'company'){
            return Validator::make($data, [
                'company_name' => ['required', 'string', 'max:255'],
                'industry' => ['required'],
                'country_id' => ['required'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'hear_about_us' => ['required'],
                'company_agreement' => ['accepted'],
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $user = new User();
        $user -> email = $data['email'];
        $user -> password = Hash::make($data['password']);

        if(isset($data['image']))
            $user -> image = $data['image'];

        if(isset($data['first_name']))
            $user -> first_name = $data['first_name'];

        if(isset($data['last_name']))
            $user -> last_name = $data['last_name'];

        if(isset($data['middle_name']))
            $user -> middle_name = ['middle_name'];

        if(isset($data['username']))
            $user -> username = $data['username'];

        if(isset($data['personal_email']))
            $user -> personal_email = $data['personal_email'];

        if(isset($data['gender']))
            $user -> gender = $data['gender'];

        if(isset($data['date_of_birth']))
            $user -> date_of_birth = $data['date_of_birth'];

        if(isset($data['phone_number']))
            $user -> phone_number = $data['phone_number'];

        if(isset($data['mobile_number']))
            $user -> mobile_number = $data['mobile_number'];

        if(isset($data['country_id']))
            $user -> country_id = $data['country_id'];


        if(isset($data['state_id']))
            $user -> state_id = $data['state_id'];


        if(isset($data['city_id']))
            $user -> city_id = $data['city_id'];

        if(isset($data['hear_about_us']))
            $user -> hear_about_us = $data['hear_about_us'];

        $randomString = time().Str::random(32);
        $user -> email_token = $randomString;

        $user -> save();

        if($data['user_type'] == 'candidate'){
            //candidate saving here

            $candidate = new Candidate();
            $candidate -> user_id = $user->id;

            if(isset($data['field_of_study']))
                $candidate -> field_of_study = $data['field_of_study'];

            if(isset($data['qualification']))
                $candidate -> qualification = $data['qualification'];

            if(isset($data['qualification_date']))
                $candidate -> qualification_date = $data['qualification_date'];

            $candidate -> save();

            $resume = new Resume();
            $resume -> user_id = $user->id;
            $resume -> save();

            $response = [];
            $response['candidateName'] = $data['first_name'];
            $response['tokenKey'] =  $randomString;
            $response['url'] = URL::to('/');

            $to = $data['email'];
            //Mail::to($to)->send(new RegisterCandidate($response));
            Mail::to($to)->send(new ActivateAccount($response));

        }
        else if ($data['user_type'] == 'company'){
            //company saving here
            Validator::make($data, [
                'company_name' => ['required', 'string', 'max:255']
            ])->validate();

            $company = new Company();
            $company -> name = $data['company_name'];
            $company -> user_id = $user->id;

            if(isset($data['business_type']))
                $company -> business_type_id = $data['business_type'];

            if(isset($data['industry']))
                $company -> industry_id = $data['industry'];

            if(isset($data['establish_date']))
                $company -> establish_date = $data['establish_date'];

            if(isset($data['total_employees']))
                $company -> total_employees = $data['total_employees'];

            $company -> save();

            $organization_chart = new OrganizationChart();
            $organization_chart -> is_root = 1;
            $organization_chart -> company_id = $company->id;
            $organization_chart -> save();

            $company_profile = new CompanyProfile();
            $company_profile -> company_id = $company->id;
            $company_profile -> save();

            $response = [];
            $response['candidateName'] = $data['company_name'];
            $response['tokenKey'] =  $randomString;
            $response['url'] = URL::to('/');

            $to = $data['email'];
            Mail::to($to)->send(new ActivateAccount($response));

            //$to = $data['email'];
           // Mail::to($to)->send(new RegisterCompany($response));
        }



        return $user;
//        return User::create([
//            'name' => $data['name'],
//            'email' => $data['email'],
//            'password' => Hash::make($data['password']),
//        ]);
    }

    public function activateAccount(Request $request) {
       // dd($request->tokenKey);
       try {
        $userInfo = DB::table('users')->where('email_token', $request->tokenKey)->first();
        if($userInfo) {
            $userId = $userInfo->id;
            $userEmail = $userInfo->email;
            $candidateInfo = DB::table('candidates')->where('user_id', $userId)->first();
            $companyInfo = DB::table('companies')->where('user_id', $userId)->first();

            $updateUser = User::find($userId);
            $updateUser->email_token = Null;
            $updateUser->email_verified_at = time();
            $updateUser->save();

            $response = [];

            if($candidateInfo != null) {
                $response['candidateName'] = $userInfo->first_name;

                $to = $userEmail;
                Mail::to($to)->send(new RegisterCandidate($response));
            }
            if($companyInfo != null) {

                $response['companyName'] = $companyInfo->name;
                $to = $userEmail;
                Mail::to($to)->send(new RegisterCompany($response));
            }

            //login
            $authUser = User::with([
                'company.businessType','company.industry','company.memberDetails','company.profiles',
                'company.department','company.jobs','company.charts','candidate.fieldOfStudy.blogCategory',
                'candidate.companyDetails','candidate.job','candidate.jobApplications','roles','country',
                'state','city','tier.subscriptionTierNode'
            ])
                ->where('email', $userInfo->email)->first();
            if(!$authUser->email_verified_at){
                return response()->json(['error' => 'E-mail not verified.'], 412);
            }
            $authUser -> full_name = $authUser->first_name." ".$authUser->last_name;
            $token = JWTAuth::fromUser($authUser);
            return response()->json([
                'token' => $token,
                'type' => 'bearer', // you can ommit this
                'expires' => auth('api')->factory()->getTTL() * 6000, // time to expiration
                'user' => $authUser
            ]);
        }
            return response()->json(['errors' => 'Not exist'], 400);
       } catch (Exception $err) {
           return response()->json(['errors' => $err.message], 500);
       }
        //dd($candidateInfo);

    }
    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return User
     */
    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        return $user;

//        $this->guard()->login($user);


//
//        return $this->registered($request, $user)
//            ?: redirect($this->redirectPath());
    }
}
