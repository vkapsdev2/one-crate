<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Str;
use App\User;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the email verification notice.
     *
     */
    public function show()
    {
        dd("HERE");
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request)
    {
        //dd($request);
        // ->route('id') gets route user id and getKey() gets current user id()
        // do not forget that you must send Authorization header to get the user from the request
        if ($request->route('id') == $request->user()->getKey() &&
            $request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        return response()->json('Email verified!');
//        return redirect($this->redirectPath());
    }

    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function resend(Request $request)
    {

//         if ($request->user()->hasVerifiedEmail()) {
//             return response()->json('User already have verified email!', 422);
// //            return redirect($this->redirectPath());
//         }

//         $request->user()->sendEmailVerificationNotification();

        $user = User::where('email', $request->email)->first();
        $dEmail =  $request->email;
        if ($user) {
            $randomString = time().Str::random(32);
            $user -> email_token = $randomString ;
            $user->save();

            $response = [];
            $response['candidateName'] = $dEmail;
            $response['tokenKey'] =  $randomString;
            $response['url'] = URL::to('/');

            Mail::to($dEmail)->send(new ActivateAccount($response));
        }   

        return response()->json('The notification has been resubmitted');
    }

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }
}
