<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ForgotPassword;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\User;
use Hash;
use Illuminate\Support\Facades\Validator;

use Illuminate\Auth\Events\PasswordReset;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function forgotPassword(Request $request) {
         $response = $this->sendResetLinkEmail($request);
         // dd($response);
//        $currentEmail = User::where('email', $request->user_email)->first();
//         //dd($currentEmail);
//         $username = $currentEmail->first_name;
//         $email = $currentEmail->email;
//        $data = [];
//        $data['username'] = $username;
//        $data['newurl'] = "#";
//        Mail::to($email)->send(new ForgotPassword($data));
//
        if ($response === null) {
            return response()->json(['errors' => ['msg' => ['Email does not exist.']]], 500);
        } else {
            return null;
        }

    }

    public function updatePassword(Request $request)
    {
        Validator::make($request->all(), [
            'new_password_confirmation' => ['required', 'string', 'min:8'],
            'new_password' => ['required', 'string', 'min:8', 'confirmed']
        ])->validate();
        $token = $request->remember_token;
        $user = User::where('remember_token', $token)->first();
        //dd($user);
        if (!$user) {
            return response()->json(['errors' => ['msg' => ['Your token expired now.']]], 500);
        }

        $user ->password = Hash::make($request -> new_password);
        $user->remember_token = null;
        $user -> save();
        return $user;
    }

    /**
     * Get the response for a successful password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse(Request $request, $response)
    {
        return response()->json([
            'message' => 'Password reset email sent.',
            'data' => $response
        ]);
    }
    /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Email could not be sent to this email address.']);
    }

}
