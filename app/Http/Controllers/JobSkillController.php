<?php

namespace App\Http\Controllers;

use App\JobSkill;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class JobSkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param JobSkill $jobSkill
     * @return Response
     */
    public function show(JobSkill $jobSkill)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param JobSkill $jobSkill
     * @return Response
     */
    public function edit(JobSkill $jobSkill)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param JobSkill $jobSkill
     * @return Response
     */
    public function update(Request $request, JobSkill $jobSkill)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param JobSkill $jobSkill
     * @return Response
     */
    public function destroy(JobSkill $jobSkill)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function removeJobSkill($id) {
        $removeSkill = JobSkill::findorFail($id);
        $removeSkill -> delete();
    }
}
