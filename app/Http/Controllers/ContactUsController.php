<?php

namespace App\Http\Controllers;

use App\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Image;

class ContactUsController extends Controller
{
    public function contactUsMessage(Request $request)
    {
        Validator::make($request->all(), [
            'user_name' => ['required'],
            'user_email' => ['required', 'email'],
            'user_message' => ['required'],
        ])->validate();
        $contactUs = new ContactUs();
        if ($request->hasFile('attachment')) {
            //dd($_FILES);
            $path = public_path('images/userImages/');
            $imgpath = Str::uuid()->toString() . '.png';
            File::isDirectory($path) or File::makeDirectory($path, 0775, true, true);
            $img = Image::make($request->attachment)->save($path . $imgpath);
            $contactUs->file = config('app.asset_path_user') . $imgpath;
        }
        $contactUs->userName = $request->user_name;
        $contactUs->userEmail = $request->user_email;
        $contactUs->message = $request->user_message;
        $contactUs->save();

        // $data = array();
        // $data = [
        //     'userName' => $request->user_name,
        //     'userEmail' => $request->user_email,
        //     'message' => $request->user_message,
        //     'target' => $request->target,
        //     'subject' => $request->subject,
        // ];
        $data= [];
        $data['userName'] = $request->user_name;
        $data['userEmail'] = $request->user_email;
        $data['message'] = $request->user_message;
        $data['senderName'] = $request->user_name;
        $data['senderEmail'] = $request->user_emai;
        $data['target'] = $request->title;
        $data['subject'] = $request->subject;
        $this -> realEmail( $data );
        // Mail::to($data -> userEmail)->send(new \App\Mail\ContactUs($data));
        return $contactUs;
    }
    public function realEmail($data) {
        Mail::to($data['target'])->send(new \App\Mail\ContactUs($data));  //fixme
    }

    public function testEmail() {
        $data = [];
        $data['message'] = 'This is a test!';
        $data['senderName'] = "sender";
        $data['senderEmail'] = "sender@gmail.com";
        Mail::to('khababhassan@gmail.com')->send(new \App\Mail\ContactUs($data));  //fixme
    }
}
