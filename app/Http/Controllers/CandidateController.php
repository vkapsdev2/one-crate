<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Company;
use App\CompanyMember;
use App\Job;
use App\OrganizationChart;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function show(Candidate $candidate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function edit(Candidate $candidate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Candidate $candidate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Candidate $candidate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function updateCandidateInfo(Request $request)
    {
//        Validator::make($request->all(), [
//            'objective_summary' => ['required'],
//        ])->validate();

        $user = User::where('id',Auth::user()->id)->first();
        if(!$user){
            return response()->json(['errors' => ['msg' => ['User does not exist.']]], 401);
        }
        if($request -> first_name)
            $user -> first_name = $request -> first_name;

        if($request -> last_name)
            $user -> last_name = $request -> last_name;

        if($request -> email)
            $user -> email = $request -> email;

        if($request -> personal_email)
            $user -> personal_email = $request -> personal_email;

        if($request -> address)
            $user -> address = $request -> address;

        if($request -> date)
            $user -> date_of_birth = $request -> date;

        if($request -> gender)
            $user -> gender = $request -> gender;

        if($request -> mobile_number)
            $user -> mobile_number = $request ->mobile_number;
        if($request -> phone_number)
            $user -> phone_number = $request -> phone_number;

        if($request -> city_id)
            $user -> city_id = $request -> city_id;

        if($request -> state_id)
            $user -> state_id = $request -> state_id;

        if($request -> country_id)
            $user -> country_id = $request -> country_id;

        if($request -> zip_code)
            $user -> zip_code = $request -> zip_code;
        $user -> save();

        $candidate = Candidate::where('user_id',Auth::user()->id)->first();
        if(!$candidate){
            return response()->json(['errors' => ['msg' => ['Candidate does not exist.']]], 401);
        }
        if($request -> qualification)
            $candidate -> qualification = $request -> qualification;
        if($request -> field_of_study_id)
            $candidate -> field_of_study_id = $request -> field_of_study_id;
        $candidate -> save();

        $user = User::with(['company.businessType','company.industry','company.memberDetails','company.profiles','company.department','company.jobs','company.charts',
            'candidate.fieldOfStudy.blogCategory','candidate.companyDetails','candidate.job','candidate.jobApplications','roles','country','state','city'])
            ->where('id',Auth::user()->id)->first();
        $user -> full_name = $user->first_name." ".$user->last_name;
        return $user;

    }


    /**
     * add resume current experience.
     *
     * @param \Illuminate\Http\Request $request
     * @return Candidate|array
     * @throws \HttpException
     */
    public function setCurrentExpertise(Request $request){

        Validator::make($request->all(), [
            'qualification' => ['required'],
            'field_of_study_id' => ['required'],
        ])->validate();

        $candidate = Candidate::where('user_id',Auth::user()->id)->first();
        if(!$candidate){
            return response()->json(['errors' => ['msg' => ['candidate does not exist.']]], 401);
        }

        $candidate -> qualification = $request -> qualification;
        $candidate -> field_of_study_id = $request -> field_of_study_id;
        $candidate -> save();

        return $candidate;
    }

    public function getAllResume() {

        $company = Company::where('user_id',Auth::user()->id)->first(); //fixme
        $candidateList = CompanyMember::with(['candidate.user'])->where('company_id',$company->id)->get()->toArray();
    
        $resumeList = Candidate::with(['user'=>function($select){
            $select = $select->select('id','first_name','last_name','email','image','country_id','state_id','city_id');
        },'fieldOfStudy','user.country','user.state','user.city'])
            ->where('status',1)
            ->doesntHave('companyDetails')
            ->get()->toArray();

        $result = [];
        foreach($resumeList as $key => $resume) {
            if ($resume['user_id']) {
                $shouldRemovedList = array_filter($candidateList, function ($candidateItem) use ($resume){
                    if($candidateItem && $candidateItem['candidate'] && $candidateItem['candidate']['user']) {
                        return $candidateItem['candidate']['user']['id'] == $resume['user_id'];
                    }
                  });
                if(count($shouldRemovedList) === 0) {
                    array_push($result, $resume);
                }
            }
        }

        return ($result);
    }

    public function getCandidateList(){
        $company_id =Company::where('user_id',Auth::user()->id)->value('id');
        $candidateList = [];
        if($company_id) {
            $candidateList = CompanyMember::with(['candidate.user'])->where('company_id',$company_id)->where('job_id',null)->get();
        }
        return $candidateList;
    }

    public function assignCandidate(Request $request){
        Validator::make($request->all(), [
            'candidate_id' => ['required','numeric'],
            'node_id' => ['required','numeric'],
        ])->validate();
        $chartNode = null;
        $company_id =Company::where('user_id',Auth::user()->id)->value('id');
        $member = CompanyMember::where('company_id',$company_id)->where('candidate_id',$request->candidate_id)->where('job_id',null)->first();
        if($member){
            $chartNode = OrganizationChart::where('id',$request->node_id)->first();
            if($chartNode){
                //Update company members table
                $member -> job_id = $chartNode -> job_id;
                $member -> save();

                //Update Jobs table to reflect the changes
                $updateJob = Job::where('id',$chartNode->job_id)->first();
                $updateJob -> candidate_id = $request -> candidate_id;
                $updateJob -> save();

                //fetching updated relations
                $chartNode = OrganizationChart::with(['children','job.candidate.user','job.title'])->where('id',$request->node_id)->first();
                $chartNode -> username = $chartNode -> job -> candidate -> user -> first_name;
                $chartNode -> jobTitle = $chartNode -> job -> title -> name;
            }
        }
        return $chartNode;
    }

    public function searchCandidates(Request $request){
        $resumeList = Candidate::with(['fieldOfStudy','user.country','user.state','user.city',
            'user'=>function($select){
                $select = $select->select('id','first_name','last_name','email','image','country_id','state_id','city_id');
            }
        ])
        ->where('status',1)
        ->doesntHave('companyDetails');

        if($request->filled("skill")){
            $resumeList->whereHas('user.resume.resumeSkills', function ($query) use ($request) {
                $query->where('id', $request->skill);
            });
        }
        if($request->filled("expertise")){
            $resumeList->whereHas('fieldOfStudy', function ($query) use ($request) {
                $query->where('id', $request->expertise);
            });
        }
        if($request->filled("state")){
            $resumeList->whereHas('user.state', function ($query) use ($request) {
                $query->where('id', $request->state);
            });
        }
        if($request->filled("city")){
            $resumeList->whereHas('user.city', function ($query) use ($request) {
                $query->where('id', $request->city);
            });
        }

        $resumeList = $resumeList->get();
        return $resumeList;
    }

    public function getCandidateDirectory($chartId){
        $node = OrganizationChart::with(['job.title'])->where('id',$chartId)->first();
        if($node->job->candidate_id){
            $candidate = Candidate::with(['fieldOfStudy','user.country','user.state','user.city',
                'user'=>function($select){
                    $select = $select->select('id','first_name','last_name','email','image','country_id','state_id','city_id','phone_number');
                }
            ])->where('id',$node->job->candidate_id)->first();
            $candidate->jobTitle = $node->job->title;
            return $candidate;
        }else{
            return response()->json(['errors' => ['msg' => ['No Candidate Assigned !']]], 500);
        }

    }
}
