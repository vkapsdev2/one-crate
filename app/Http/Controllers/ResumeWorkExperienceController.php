<?php

namespace App\Http\Controllers;

use App\Resume;
use App\ResumeWorkExperience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResumeWorkExperienceController extends Controller
{


    public function getResumeWorkExperience(){
        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }
        $resumeWorkExperience = ResumeWorkExperience::with(['country', 'state', 'city','resume'])->where('resume_id',$resume->id)
            ->orderBy('sort_order', 'asc')->get();

        return $resumeWorkExperience ? $resumeWorkExperience : null;
    }

    /**
     * add resume work experience.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeWorkExperience|array
     */
    public function setResumeWorkExperience(Request $request){

        Validator::make($request->all(), [
            'title' => ['required','string','max:255'],
            'organization' => ['required','string','max:255'],
            'start_year_value' => ['required','string','max:255'],
            'start_month_value' => ['required','string','max:255'],
            'work_experience_country_id' => ['required','string','max:255']
        ])->validate();

        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }

        $resumeWorkExperience = new ResumeWorkExperience();
        $resumeWorkExperience -> resume_id = $resume -> id;
        $resumeWorkExperience ->title = $request->title;
        $resumeWorkExperience -> organization = $request-> organization;
        $resumeWorkExperience -> country_id = $request-> work_experience_country_id;
        $resumeWorkExperience -> state_id = $request-> work_experience_state_id;
        $resumeWorkExperience -> city_id = $request-> work_experience_city_id;
        $resumeWorkExperience -> start_year = $request-> start_year_value;
        $resumeWorkExperience -> start_month = $request-> start_month_value;
        $resumeWorkExperience -> end_year = $request-> end_year_value;
        $resumeWorkExperience -> end_month = $request-> end_month_value;
        if($request->currently_working){
            $resumeWorkExperience -> currently_working = $request-> currently_working;
        }
        if($request->description){
            $resumeWorkExperience -> description = $request-> description;
        }
        $resumeWorkExperience ->save();

        $resumeWorkExperience  = ResumeWorkExperience::with(['country', 'state', 'city','resume'])->where('resume_id',$resume->id)
            ->orderBy('sort_order', 'asc')->get();
        return $resumeWorkExperience;
    }

    /**
     * update resume work experience.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeWorkExperience|array
     */
    public function updateWorkExperience(Request $request){

        Validator::make($request->all(), [
            'id' => ['required'],
            'title' => ['required','string','max:255'],
            'organization' => ['required','string','max:255'],
            'start_year_value' => ['required','string','max:255'],
            'start_month_value' => ['required','string','max:255'],
            'work_experience_country_id' => ['required','string','max:255']
        ])->validate();

        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }

        $resumeWorkExperience = ResumeWorkExperience::find($request->id);
        if($resumeWorkExperience){
            $resumeWorkExperience -> resume_id = $resume -> id;
            $resumeWorkExperience ->title = $request->title;
            $resumeWorkExperience -> organization = $request-> organization;
            $resumeWorkExperience -> country_id = $request-> work_experience_country_id;
            $resumeWorkExperience -> state_id = $request-> work_experience_state_id;
            $resumeWorkExperience -> city_id = $request-> work_experience_city_id;
            $resumeWorkExperience -> start_year = $request-> start_year_value;
            $resumeWorkExperience -> start_month = $request-> start_month_value;
            $resumeWorkExperience -> end_year = $request-> end_year_value;
            $resumeWorkExperience -> end_month = $request-> end_month_value;
            if($request->currently_working){
                $resumeWorkExperience -> currently_working = $request-> currently_working;
            }
            if($request->description){
                $resumeWorkExperience -> description = $request-> description;
            }
            $resumeWorkExperience ->save();

            $resumeWorkExperience  = ResumeWorkExperience::with(['country', 'state', 'city','resume'])->where('resume_id',$resume->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeWorkExperience;
        }else{
            return null;
        }
    }

    /**
     * Remove resume work experience.
     *
     * @return ResumeWorkExperience|array
     */
    public function deleteWorkExperience($id){
        $resumeEducation = ResumeWorkExperience::find($id);
        if($resumeEducation){
            $resume = Resume::where('user_id',Auth::user()->id)->first();
            $resumeEducation -> delete();

            $resumeEducation = ResumeWorkExperience::with(['country','state','city'])->where('resume_id',$resume ->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeEducation;

        }else{
            return null;
        }

    }

    /**
     * update resume work experience sort order.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeWorkExperience|array
     */
    public function sortWorkExperience(Request $request){


        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if($resume){
            $sortedarray = $request->sortedarray;
            foreach ($sortedarray as $key => $value) {
                $resumeWorkExperience = ResumeWorkExperience::find($value['id']);
                $resumeWorkExperience->resume_id = $resume->id;
                $resumeWorkExperience->sort_order = $value['sort_order'];
                $resumeWorkExperience->save();
            }
            $resumeWorkExperience  = ResumeWorkExperience::with(['country', 'state', 'city','resume'])->where('resume_id',$resume->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeWorkExperience;
        }else{
            return null;
        }
    }


}
