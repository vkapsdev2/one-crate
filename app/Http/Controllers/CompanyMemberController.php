<?php

namespace App\Http\Controllers;

use App\CompanyMember;
use App\Job;
use App\User;
use Illuminate\Http\Request;
use App\JobApplicant;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use const http\Client\Curl\AUTH_ANY;

class CompanyMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyMember  $companyMember
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyMember $companyMember)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyMember  $companyMember
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyMember $companyMember)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyMember  $companyMember
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyMember $companyMember)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyMember  $companyMember
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyMember $companyMember)
    {
        //
    }

    public function getMembersList() {
        $user = User::with(['company'])->where('id',Auth::user()->id)->first();
        $company_id = $user->company->id;
        if($company_id) {
          $memberlist =  CompanyMember::with(['candidate','candidate.user'=>function($query){
              $query = $query->select('id','first_name','last_name','email','image','gender');
            },'job'=>function($select){
              $select = $select->select('id','job_title_id','company_id','department_id','candidate_id');
          },'job.title','job.department'])->where('company_id',$company_id)
              ->where('status',1)->get();
          return $memberlist;
        } else {
            return response()->json(['errors' => ['msg' => ['Company Does Not Exist.']]], 500);
        }
    }

    public function getPendingCandidates() {
        $user = User::with(['company'])->where('id',Auth::user()->id)->first();
        $company_id = $user->company->id;
        $pendingList = JobApplicant::with(['candidate.fieldOfStudy','candidate.user'=>function($select){
            $select = $select->select('id','first_name','last_name','email','image');
        },'job'=>function($query) use ($company_id){
            $query = $query->select('id','company_id')->where('company_id',$company_id);
        }])->where('is_offered',1)->groupBy('candidate_id')->get()->toArray();
        $pendingListRes = $this->filterArray($pendingList, $company_id);
        return $pendingListRes;
    }

    function filterArray ($pendingList, $company_id) {
        $res = [];
        foreach ($pendingList as $item) {
            if (($item['candidate']['user'] != null)  && ($item['job'] != null ) ) {
                array_push($res, $item);
            }
        }
        return $res;
    }

    public function deleteMember(Request $request){

        $id = $request->id;
        $companyMember = CompanyMember::with(['job'])->where('id',$id)->first();

        if($companyMember){
            $companyMember -> status = 0;
            // $companyMember->save();
            if($companyMember->job != null) {
                $job = Job::where('id', $companyMember->job->id)->first();
                $job -> candidate_id = null;
                $job -> save();
            }

            $user = User::with(['company'])->where('id',Auth::user()->id)->first();
            $company_id = $user->company->id;

            $memberlist =  CompanyMember::with(['candidate','candidate.user'=>function($query){
                $query = $query->select('id','first_name','last_name','email','image');
            },'job'=>function($select){
                $select = $select->select('id','job_title_id','company_id','department_id','candidate_id');
            },'job.title','job.department'])->where('company_id',$company_id)
                ->where('status',1)->get();
            CompanyMember::where('id', $companyMember->id)->delete(); //fixme

//            $memberlist =  CompanyMember::with(['candidate','candidate.user'=>function($query){
//                $query = $query->select('id','first_name','last_name','email','image');
//            },'job'=>function($select){
//                $select = $select->select('id','job_title_id','company_id','department_id');
//            },'job.title','job.department'])->where('company_id',$company_id)
//                ->where('status',1)->get();
            return $memberlist;
        }else{
            return null;
        }

    }

    public function getMemberProfile(Request $request) {
        $user = User::with(['company'])->where('id',Auth::user()->id)->first();
        $company_id = $user->company->id;
        $id = $request->id;
        if($company_id) {
            //DB::enableQueryLog();

            $memberlist =  CompanyMember::with(['candidate','candidate.user'=>function($query){
                $query = $query->select('id','first_name','last_name','email','image','country_id','state_id','city_id','phone_number');
            },'job'=>function($select){
                $select = $select->select('id','job_title_id','company_id','department_id');
            },'job.title','job.department','candidate.user.country','candidate.user.state','candidate.user.city'])->where('company_id',$company_id)
                ->where('candidate_id',$id)->first();
            //$quries = DB::getQueryLog();
            //dd($quries);

            return $memberlist;
        } else {
            return response()->json(['errors' => ['msg' => ['Company Does Not Exist.']]], 500);
        }
    }
}
