<?php

namespace App\Http\Controllers;

use App\Resume;
use App\ResumeStrength;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResumeStrengthController extends Controller
{
    /**
     * add resume Strength.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeStrength|array
     */
    public function setResumeStrength(Request $request)
    {
        Validator::make($request->all(), [
            'tags' => ['required', 'string'],
        ])->validate();
        $tagsArray = explode(',', $request->tags);
        $resume = Resume::where('user_id', Auth::user()->id)->first();
        if (!$resume) {
            return ['error' => 'resume does not exist'];
        }
        //dd($tagsArray);
        foreach ($tagsArray as $key => $strength) {
            //echo $strength;
            $oldStrength = ResumeStrength::where([
                'resume_id' => $resume->id,
                'name' => $strength,
            ])->first();
            if (!$oldStrength) {
                $resumeStrength = new ResumeStrength();
                $resumeStrength->resume_id = $resume->id;
                $resumeStrength->name = $strength;
                $resumeStrength->save();
            }
        }

        $resumeStrengths = ResumeStrength::where('resume_id', $resume->id)
            ->orderBy('sort_order', 'asc')->get();
        return $resumeStrengths;
    }

    public function getResumeStrength()
    {
        $resume = Resume::where('user_id', Auth::user()->id)->first();
        if ($resume) {
            $resumeStrength = ResumeStrength::with(['resume'])->where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();
            //dd($resumeStrength);
            return $resumeStrength;
        } else {
            return response()->json(['errors' => ['msg' => ['Resume Does Not Exist.']]], 500);
        }
    }

    public function deleteStrength($id)
    {
        $resumeStrength = ResumeStrength::find($id);
        if ($resumeStrength) {
            $resume = Resume::where('user_id', Auth::user()->id)->first();
            $resumeStrength->delete();

            $resumeStrengths = ResumeStrength::where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeStrengths;

        } else {
            return null;
        }

    }

    /**
     * update resume strength sort order.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeStrength|array
     */
    public function sortStrength(Request $request)
    {
        $resume = Resume::where('user_id', Auth::user()->id)->first();
        if ($resume) {
            $sortedarray = $request->sortedarray;
            foreach ($sortedarray as $key => $value) {
                $resumeStrengths = ResumeStrength::find($value['id']);
                $resumeStrengths->resume_id = $resume->id;
                $resumeStrengths->sort_order = $value['sort_order'];
                $resumeStrengths->save();
            }
            $resumeStrengths = ResumeStrength::where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();

            return $resumeStrengths;

        } else {
            return null;
        }
    }

}
