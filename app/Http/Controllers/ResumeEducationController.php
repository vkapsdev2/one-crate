<?php

namespace App\Http\Controllers;

use App\Resume;
use App\ResumeEducation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResumeEducationController extends Controller
{
    /**
     * get resume education.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeEducation|array
     */
    public function getResumeEducation(){
        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if($resume) {
            $resumeEducation = ResumeEducation::with(['degreelevel','country', 'state', 'city','resume'])
                ->where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')
                ->get();
            return $resumeEducation;
        }else{
            return response()->json(['errors' => ['msg' => ['Resume Does Not Exist.']]], 500);
        }
    }

    /**
     * add resume education.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeEducation|array
     */
    public function setResumeEducation(Request $request){

        Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:255'],
            'institution' => ['required', 'string', 'max:255'],
            'degree_level_id' => ['required', 'string','max:255'],
            'completion_year_value' => ['required'],
            'completion_month_value' => ['required'],
            'country_id_value' => ['required'],
        ])->validate();
        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }

        $resumeEducation = new ResumeEducation();
        $resumeEducation -> resume_id = $resume ->id;
        $resumeEducation -> title = $request ->title;
        $resumeEducation -> institution = $request ->institution;
        $resumeEducation -> degree_level = $request -> degree_level_id;
        $resumeEducation -> completion_year = $request -> completion_year_value;
        $resumeEducation -> completion_month = $request -> completion_month_value;
        $resumeEducation -> country_id = $request -> country_id_value;
        if($request->education_state_id){
            $resumeEducation ->state_id = $request -> education_state_id;
        }
        if($request->education_city_id){
            $resumeEducation -> city_id = $request -> education_city_id;
        }
        $resumeEducation ->save();

        $resumeEducation = ResumeEducation::with(['degreelevel','country','state','city'])
            ->where('resume_id',$resume ->id)
            ->orderBy('sort_order', 'asc')
            ->get();
        return $resumeEducation;
    }

    /**
     * delete resume education.
     *
     * @param $id
     * @return ResumeEducation|array
     */
    public function deleteEducation($id){
        $resumeEducation = ResumeEducation::find($id);
        if($resumeEducation){
            $resume = Resume::where('user_id',Auth::user()->id)->first();
            $resumeEducation -> delete();

            $resumeEducation = ResumeEducation::with(['degreelevel','country','state','city'])
                ->where('resume_id',$resume ->id)
                ->orderBy('sort_order', 'asc')
                ->get();
            return $resumeEducation;

        }else{
            return null;
        }

    }


    /**
     * update resume education.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeEducation|array
     */
    public function updateEducation(Request $request){

        Validator::make($request->all(), [
            'id' => ['required'],
            'title' => ['required', 'string', 'max:255'],
            'institution' => ['required', 'string', 'max:255'],
            'degree_level_id' => ['required', 'string','max:255'],
            'completion_year_value' => ['required'],
            'completion_month_value' => ['required'],
            'country_id_value' => ['required'],
        ])->validate();

        $resumeEducation = ResumeEducation::find($request->id);
        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if($resumeEducation && $resume){
            $resumeEducation -> resume_id = $resume ->id;
            $resumeEducation -> title = $request ->title;
            $resumeEducation -> institution = $request ->institution;
            $resumeEducation -> degree_level = $request -> degree_level_id;
            $resumeEducation -> completion_year = $request -> completion_year_value;
            $resumeEducation -> completion_month = $request -> completion_month_value;
            $resumeEducation -> country_id = $request -> country_id_value;
            if($request -> education_state_id){
                $resumeEducation ->state_id = $request -> education_state_id;
            }
            if($request -> education_city_id){
                $resumeEducation -> city_id = $request -> education_city_id;
            }
            $resumeEducation -> save();


            $resumeEducation = ResumeEducation::with(['degreelevel','country','state','city'])
                ->where('resume_id',$resume ->id)
                ->orderBy('sort_order', 'asc')
                ->get();
            return $resumeEducation;

        }else{
            return null;
        }

    }

    /**
     * update resume education sort order.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeEducation|array
     */
    public function sortEducation(Request $request)
    {
            $resume = Resume::where('user_id', Auth::user()->id)->first();
            if ($resume) {
                $sortedarray = $request->sortedarray;
                foreach ($sortedarray as $key => $value) {
                    $resumeEducation = ResumeEducation::find($value['id']);
                    $resumeEducation->resume_id = $resume->id;
                    $resumeEducation->sort_order = $value['sort_order'];
                    $resumeEducation->save();
                }
                $resumeEducation = ResumeEducation::with(['degreelevel', 'country', 'state', 'city'])
                    ->where('resume_id', $resume->id)
                    ->orderBy('sort_order', 'asc')
                    ->get();
                return $resumeEducation;

            } else {
                return null;
            }
    }


}
