<?php

namespace App\Http\Controllers;

use App\Events\MessageSeenEvent;
use App\Events\PrivateMessageEvent;
use App\Message;
use App\User;
use App\UserMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserMessageController extends Controller
{
    public function saveUserMessage(Request $request)
    {
        $request->validate([
            'message' => 'required',
            'sender_id' => 'required',
            'receiver_id' => 'required',
        ]);
        $input = $request->all();
        $newMessage = new Message();
        $newMessage->parent_id = 0;
        $newMessage->message = $request->message;
        $newMessage->save();
        if ($input['sender_id'] < $input['receiver_id']) {
            $conversationId = $input['sender_id'] . "" . $input['receiver_id'];
        } else {
            $conversationId = $input['receiver_id'] . "" . $input['sender_id'];
        }
        $userChat = new UserMessage;
        $userChat->message_id = $newMessage->id;
        $userChat->sender_id = $input['sender_id'];
        $userChat->receiver_id = $input['receiver_id'];
        $userChat->conversation_id = $conversationId;
        $userChat->seen_status = 0;
        $userChat->deliver_status = 1;
        $userChat->save();
        $chats = UserMessage::with(['getBody', 'sender', 'sender.company'])->find($userChat->id);
        event(new PrivateMessageEvent($chats));
        return response()->json([
            'newMessage' => $chats,
            'success' => true,
            'message' => 'Message saved successfully',
        ]);
    }

    public function startChat($senderId, $receiverId){
        if ($senderId < $receiverId) {
            $conversationId = $senderId . "" . $receiverId;
        } else {
            $conversationId = $receiverId . "" . $senderId;
        }
        $userChat = [];
        $userChat['message_id'] = 0;
        $userChat['sender_id'] = $senderId;
        $userChat['receiver_id'] = $receiverId;
        $userChat['conversation_id'] = $conversationId;
        $userChat['seen_status'] = 1;
        $userChat['deliver_status'] = 1;
        $userChat['sender'] = User::with(['company'])->where("id", $senderId)->first();
        $userChat = json_encode($userChat);
        $userChat = json_decode($userChat);
        event(new PrivateMessageEvent($userChat));
        return response()->json([
            'newMessage' => $userChat,
            'success' => true,
            'message' => 'Chat Start',
        ]);
    }

    public function getAllChats($senderId, $receiverId)
    {
        $firstWhere = [
            ['sender_id', '=', $senderId],
            ['receiver_id', '=', $receiverId],
        ];
        $secondWhere = [
            ['sender_id', '=', $receiverId],
            ['receiver_id', '=', $senderId],
        ];
        $chats = UserMessage::with(['getBody'])->where($firstWhere)->orwhere($secondWhere)->orderBy('created_at', 'desc')->paginate(10);
        // $chats['unseen'] = UserMessage::where('seen_status', 0)->count();
        return response()->json(['chat' => $chats, 'success' => true]);
    }

    public function updateSeenStatus($conversationId)
    {
        $value = ['seen_status' => 1];
        $chats = UserMessage::with(['getBody'])->where('conversation_id', $conversationId)->where('seen_status', 0)->get();
        UserMessage::where('conversation_id', $conversationId)->update($value);
        event(new MessageSeenEvent($chats));
        return response()->json(['message' => 'status changed', 'success' => true, 'chats' => $chats]);
    }

    public function getChatUserList($id)
    {
        $user = Auth::user();
        $userId = $user->id;
        $payload = ['sender', 'receiver', 'sender.company', 'receiver.company'];
        $userData = UserMessage::with($payload)->where("sender_id", $userId)
            ->orWhere("receiver_id", $userId)
            ->orderBy('message_id', 'desc')
            ->groupBy("conversation_id")
            ->get();
        foreach ($userData as $userKey => &$user) {
            $latestMessage = UserMessage::where("conversation_id", $user->conversation_id)->latest()->first();
            $user['latest_message_id'] = $latestMessage->message_id;
            $user['unseen'] = UserMessage::where('sender_id', $user->sender_id)->where('receiver_id', $userId)->where('seen_status', 0)->count();
            if ($user->sender->id != $userId) {
                $userData[$userKey]->user = $user->sender;
            } else {
                $userData[$userKey]->user = $user->receiver;
            }
            if (!empty($userData[$userKey]->user->company)) {
                $userData[$userKey]->user->first_name = $userData[$userKey]->user->company->name;
            }
        }
        $userData = $userData->toArray();
        usort($userData, function ($item1, $item2) {
            return $item2['latest_message_id'] <=> $item1['latest_message_id'];
        });
        return $userData;
    }

    public function saveMessageWithFile(Request $request)
    {
        // phpinfo();
        // dd(ini_get('post_max_size'));
        // 'file' => 'required|max:32192',
        $request->validate([
            'sender_id' => 'required',
            'receiver_id' => 'required',
        ]);
        $fileName = time() . '.' . $request->file->extension();
        $request->file->move(public_path('uploads'), $fileName);
        $input = $request->all();
        $newMessage = new Message();
        $newMessage->parent_id = 0;
        $newMessage->message = $fileName;
        $newMessage->type = $input['type'];
        $newMessage->save();
        if ($input['sender_id'] < $input['receiver_id']) {
            $conversationId = $input['sender_id'] . "" . $input['receiver_id'];
        } else {
            $conversationId = $input['receiver_id'] . "" . $input['sender_id'];
        }
        $userChat = new UserMessage;
        $userChat->message_id = $newMessage->id;
        $userChat->sender_id = $input['sender_id'];
        $userChat->receiver_id = $input['receiver_id'];
        $userChat->conversation_id = $conversationId;
        $userChat->seen_status = 0;
        $userChat->deliver_status = 1;
        $userChat->save();
        $userChat->getBody;
        $userChat->sender;
        $userChat->sender->company;
        event(new PrivateMessageEvent($userChat));
        return response()->json([
            'newMessage' => $userChat,
            'success' => true,
            'message' => 'Message saved successfully',
        ]);
    }

    public function uploadFile(Request $request)
    {
        $request->validate([
            'file' => 'required|max:2048',
        ]);
        $fileName = time() . '.' . $request->file->extension();
        $request->file->move(public_path('uploads'), $fileName);
        return response()->json([
            'success' => true,
            'filename' => $fileName,
            'message' => 'file saved successfully',
        ]);
    }

    public function printData($arr)
    {
        echo '<pre>';
        print_r($arr);
        die();
    }
}

// $sql = UserMessage::where($firstWhere)->orwhere($secondWhere)->toSql();
// php artisan make:event MessageSeenEvent
