<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PaymentCycleController extends Controller
{
    public function paymentInvoice()
    {


        $data['invoice'] = 'Payment';

        // Send data to the view using loadView function of PDF facade
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,'dpi' => 150, 'defaultFont' => 'Poppins'])
            ->loadView('pdf.invoice',$data)->setPaper('letter', 'portrait');
        //$pdf = PDF::loadHTML('<h1>Test</h1>');
        return $pdf->stream();
        //return view('pdf.invoice')->with('invoice',$data);
    }
}
