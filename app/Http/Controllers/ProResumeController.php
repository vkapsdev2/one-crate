<?php

namespace App\Http\Controllers;

use App\SubscriptionTierNode;
use App\ProResumeAmount;
use App\PaymentOption;
use App\UserPaymentDetails;
use App\UserPaymentCards;
use App\ProResumePaymentHistory;
use App\UserProResume;
use App\User;
use Cartalyst\Stripe\Stripe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProResumeController extends Controller
{
    public function getProResumeList(){
        $userId = Auth::user()->id;
        $userProResume = UserProResume::where('user_id', $userId)->orderBy("created_at", "DESC")->paginate(20);
        $checkActive = UserProResume::where('user_id', $userId)->where('active', 1)->count();
        $custom = collect(['active' => $checkActive]);
        $data = $custom->merge($userProResume);
        return $data;
    }

    public function getProResumeNodeDetails($id){
        $stripe             = new Stripe(env('STRIPE_API_KEY'), env('STRIPE_API_VERSION'));
        $paymentDetails     = SubscriptionTierNode::where('id', $id)->first();
        $getProResumeAmount = ProResumeAmount::find(1);

        $paymentDetails->actualAmount    = $getProResumeAmount->amount;
        $paymentDetails->payableAmount   = $getProResumeAmount->amount;
        $paymentDetails->payableCycle    = 'One Time';
        $paymentDetails->payableDiscount = 0;

        $userId = Auth::user()->id;
        $userPaymentDetails = UserPaymentDetails::where('user_id', $userId)->first();
        if ($userPaymentDetails) {
            //creating payment intent for the package amount
            $paymentIntent = $stripe->paymentIntents()->create([
                'customer' => $userPaymentDetails->customer_id,
                'amount' => $paymentDetails->payableAmount,
                'currency' => 'usd',
                'payment_method_types' => [
                    'card',
                ],
            ]);

            $cardList = UserPaymentCards::where('user_id', $userId)->get()->toArray();

            if ($paymentIntent) {
                $paymentDetails->client_secret =  $paymentIntent['client_secret'];
                $paymentDetails->payment_intent =  $paymentIntent;
                $paymentDetails->cardList =  $cardList;
            }
        } else {
            $userEmail = Auth::user()->email;
            $userFullName = Auth::user()->first_name . " " . Auth::user()->last_name;

            //creating a new customer for the user on STRIPE
            $customer = $stripe->customers()->create([
                'email' => $userEmail,
                'name'  => $userFullName,
            ]);

            if ($customer) {
                //saving customer id into user payment options
                $paymentOption = PaymentOption::where('name', 'stripe')->first();
                $userPaymentDetails = new UserPaymentDetails();
                $userPaymentDetails->user_id = $userId;
                $userPaymentDetails->payment_option_id = $paymentOption->id;
                $userPaymentDetails->customer_id = $customer['id'];
                $userPaymentDetails->save();

                //creating payment intent for the package amount
                $paymentIntent = $stripe->paymentIntents()->create([
                    'customer' => $userPaymentDetails->customer_id,
                    'amount' => $paymentDetails->payableAmount,
                    'currency' => 'usd',
                    'payment_method_types' => [
                        'card',
                    ],
                ]);
                if ($paymentIntent) {
                    $paymentDetails->client_secret =  $paymentIntent['client_secret'];
                    $paymentDetails->cardList =  [];
                }
            }
        }
        return $paymentDetails;
    }

    public function updateUserProResumePaymentData(Request $request)
    {
        $requestPaymentDetails = $request->details;
        return $requestPaymentDetails;
        if ($requestPaymentDetails) {
            $userId = Auth::user()->id;
            $userPaymentDetails = UserPaymentDetails::where('user_id', $userId)->first();
            if ($userPaymentDetails) {
                return $userPaymentDetails;
            } else {
                return response()->json(['errors' => ['msg' => ['User Payment Details Not Found.']]], 501);
            }
        } else {
            return response()->json(['errors' => ['msg' => ['Payment Method Not found.']]], 501);
        }
    }

    public function proResumeUpdateUserPaymentId(Request $request)
    {
        $paymentMethodID = $request->paymentMethodID;
        $paymentIntentID = $request->paymentIntentID;
        $requestPaymentDetails = json_decode($request->details);
        $cardInfo = json_decode($request->cardInfo);

        if ($paymentMethodID) {
            $userId = Auth::user()->id;
            $userPaymentDetails = UserPaymentDetails::where('user_id', $userId)->first();
            if ($userPaymentDetails) {
                $userPaymentDetails->payment_method_id = $paymentMethodID;
                $userPaymentDetails->save();

                $userCard = UserPaymentCards::where([
                    'user_id'   => $userId,
                    'brand'     => $cardInfo->brand,
                    'last4'     => $cardInfo->last4,
                ])->first();

                if ($userCard) {
                    $userCard->card_id      = $cardInfo->id;
                    $userCard->brand        = $cardInfo->brand;
                    $userCard->cvc_check    = $cardInfo->cvc_check;
                    $userCard->exp_month    = $cardInfo->exp_month;
                    $userCard->exp_year     = $cardInfo->exp_year;
                    $userCard->funding      = $cardInfo->funding;
                    $userCard->last4        = $cardInfo->last4;
                    $userCard->save();

                } else {
                    $newUserCard            = new UserPaymentCards();
                    $newUserCard->user_id   = $userId;
                    $newUserCard->card_id   = $cardInfo->id;
                    $newUserCard->brand     = $cardInfo->brand;
                    $newUserCard->cvc_check = $cardInfo->cvc_check;
                    $newUserCard->exp_month = $cardInfo->exp_month;
                    $newUserCard->exp_year  = $cardInfo->exp_year;
                    $newUserCard->funding   = $cardInfo->funding;
                    $newUserCard->last4     = $cardInfo->last4;
                    $newUserCard->save();
                }

                $proResumePaymentHistory = new ProResumePaymentHistory();
                $proResumePaymentHistory->user_id = $userId;
                $proResumePaymentHistory->invoice_number = "";
                $proResumePaymentHistory->payment_option_id = 1;
                $proResumePaymentHistory->transaction_id = $paymentIntentID;
                $proResumePaymentHistory->amount = $requestPaymentDetails->payableAmount;
                $proResumePaymentHistory->save();

                $userProResume = new UserProResume();
                $userProResume->user_id = $userId;
                $userProResume->payment_id = $proResumePaymentHistory->id;
                $userProResume->attempts = 3;
                $userProResume->active = 1;
                $userProResume->save();

                return response()->json(['success' => 1], 200);
            } else {
                return response()->json(['errors' => ['msg' => ['User Payment Details Not Found.']]], 501);
            }
        } else {
            return response()->json(['errors' => ['msg' => ['Payment Method Not found.']]], 501);
        }
    }
}
