<?php

namespace App\Http\Controllers;

use App\OrganizationChart;
use App\Resume;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use PDF;

ini_set('max_execution_time', 180);

class ResumeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     *
     */
    public function index($user_id)
    {
        $resume = Resume::with('resumeAchievements', 'resumeEducation', 'resumeInterests',
            'resumeReferences', 'resumeSkills', 'resumeStrength', 'resumeVideos', 'resumeWorkExperience')
            ->where('user_id', $user_id)
            ->first();

        return $resume;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Resume
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'objective_summary' => ['required', 'string'],
        ])->validate();

        $resume = new Resume();
        $resume->user_id = Auth::user()->id;
        $resume->objective_summary = $request->objective_summary;
        $resume->save();

        return $resume;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resume  $resume
     * @return \Illuminate\Http\Response
     */
    public function show(Resume $resume)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resume  $resume
     * @return \Illuminate\Http\Response
     */
    public function edit(Resume $resume)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resume  $resume
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resume $resume)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resume  $resume
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resume $resume)
    {
        //
    }
    /**
     * add resume education.
     *
     * @param \Illuminate\Http\Request $request
     * @return Resume|array
     */
    public function setObjectiveSummary(Request $request)
    {

//        Validator::make($request->all(), [
        //            'objective_summary' => ['required'],
        //        ])->validate();

        $resume = Resume::where('user_id', Auth::user()->id)->first();
        if (!$resume) {
            return response()->json(['errors' => ['msg' => ['resume does not exist.']]], 500);
        }
        $resume->objective_summary = $request->objective_summary;
        $resume->save();

        return $resume;
    }

    /**
     * get Objective Summary.
     *
     * @param \Illuminate\Http\Request $request
     * @return Resume|array
     */
    public function getObjectiveSummary()
    {

        $resume = Resume::where('user_id', Auth::user()->id)->first();
        //dd($resume);
        if (!$resume) {
            return response()->json(['errors' => ['msg' => ['resume does not exist.']]], 500);
        }

        return $resume;
    }

    /**
     * Display candidate resume.
     *
     *
     */
    public function candidateResume($user_id)
    {

        $resume = Resume::with(['user' => function ($select) {
            $select = $select->select('id', 'image', 'first_name', 'last_name', 'email', 'phone_number', 'country_id', 'state_id', 'city_id');
        }, 'user.candidate.fieldOfStudy', 'user.country', 'user.state',
            'user.city', 'resumeAchievements' => function ($selectachievement) {
                $selectachievement = $selectachievement->select('id', 'resume_id', 'title', 'completion_month', 'completion_year')
                    ->orderBy('sort_order', 'asc');
            }, 'resumeEducation' => function ($selecteducation) {
                $selecteducation = $selecteducation->select('id', 'resume_id', 'title', 'institution', 'completion_month', 'completion_year')
                    ->orderBy('sort_order', 'asc');
            }, 'resumeInterests', 'resumeReferences' => function ($selectReference) {
                $selectReference = $selectReference->select('*')->orderBy('sort_order', 'asc');
            }, 'resumeSkills' => function ($selectSkill) {
                $selectSkill = $selectSkill->select('*')->orderBy('sort_order', 'asc');
            }, 'resumeStrength' => function ($selectStrength) {
                $selectStrength = $selectStrength->select('*')->orderBy('sort_order', 'asc');
            }, 'resumeWorkExperience' => function ($selectWorkExperience) {
                $selectWorkExperience = $selectWorkExperience->select('*')->orderBy('sort_order', 'asc');
            }, 'resumeLanguage' => function ($selectLanguage) {
                $selectLanguage = $selectLanguage->select('*')->orderBy('sort_order', 'asc');
            },
            'resumeReferences.country', 'resumeReferences.state', 'resumeReferences.city', 'resumeSkills.skill',
            'resumeVideos', 'resumeLanguage.language', 'resumeLanguage.proficiency'])
            ->where('user_id', $user_id)
            ->first();

        return $resume;
    }

    /**
     * Display candidate resume.
     *
     *
     */
    public function candidateResumeByChartId($nodeId)
    {
        $node = OrganizationChart::with(['job.candidate.user'])->where('id', $nodeId)->first();
        if ($node->job->candidate_id) {
            $resume = Resume::with(['user' => function ($select) {
                $select = $select->select('id', 'image', 'first_name', 'last_name', 'email', 'phone_number', 'country_id', 'state_id', 'city_id');
            }, 'user.candidate.fieldOfStudy', 'user.country', 'user.state',
                'user.city', 'resumeAchievements' => function ($selectachievement) {
                    $selectachievement = $selectachievement->select('id', 'resume_id', 'title', 'completion_month', 'completion_year');
                }, 'resumeEducation' => function ($selecteducation) {
                    $selecteducation = $selecteducation->select('id', 'resume_id', 'title', 'completion_month', 'completion_year');
                }, 'resumeInterests',
                'resumeReferences.country', 'resumeReferences.state', 'resumeReferences.city', 'resumeSkills.skill', 'resumeStrength',
                'resumeVideos', 'resumeWorkExperience', 'resumeLanguage.language', 'resumeLanguage.proficiency'])
                ->where('user_id', $node->job->candidate->user->id)
                ->first();
            return $resume;
        } else {
            return response()->json(['errors' => ['msg' => ['No Candidate Assigned !']]], 500);
        }

    }

    /**
     * Display candidate resume.
     *
     *
     */
    public function resumePDF($user_id)
    {

        $resume = Resume::with(['user' => function ($select) {
            $select = $select->select('id', 'image', 'first_name', 'last_name', 'email', 'phone_number', 'country_id', 'state_id', 'city_id');
        }, 'user.candidate.fieldOfStudy', 'user.country', 'user.state',
            'user.city', 'resumeAchievements' => function ($selectachievement) {
                $selectachievement = $selectachievement->select('id', 'resume_id', 'title', 'completion_month', 'completion_year');
            }, 'resumeEducation' => function ($selecteducation) {
                $selecteducation = $selecteducation->select('id', 'resume_id', 'title', 'institution', 'completion_month', 'completion_year');
            }, 'resumeInterests', 'resumeReferences.country', 'resumeReferences.state', 'resumeReferences.city', 'resumeSkills.skill', 'resumeStrength',
            'resumeWorkExperience', 'resumeLanguage.language', 'resumeLanguage.proficiency'])
            ->where('user_id', $user_id)
            ->first();
        //dd($resume);
        $data['resume'] = $resume;
        $fullName = $resume->user->first_name . '-' . $resume->user->last_name;
        // return view('pdf.resume', $data);
        // Send data to the view using loadView function of PDF facade
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'dpi' => 150,
            'defaultFont' => 'Poppins'])->loadView('pdf.resume', $data)->setPaper('letter', 'portrait');
        //  return $pdf->stream();
        //$pdf->save(storage_path().'_filename.pdf');
        // Finally, you can download the file using download function
        return $pdf->download($fullName . '.pdf');

        // return view('pdf.resume')->with('resume',$resume);
    }

}
