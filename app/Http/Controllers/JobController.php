<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Company;
use App\Job;
use App\JobApplicant;
use App\JobCompanyTemplate;
use App\JobRewards;
use App\JobSkill;
use App\JobTemplateSkill;
use App\JobTitle;
use App\Mail\ApplyJob;
use App\OrganizationChart;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use PDF;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    public function generateAvatar()
    {
        $avatarArray = [
            '/assets/images/avatar-1.png',
            '/assets/images/avatar-2.png',
            '/assets/images/avatar-3.png',
            '/assets/images/avatar-4.png',
            '/assets/images/avatar-5.png',
        ];
        return $avatarArray[array_rand($avatarArray)];
    }
    /**
     * get job titles
     *
     * @param null $query
     * @return Response
     */
    public function getJobTitle($query = null)
    {
        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        if ($user->company) {
            if ($query) {
                // $jobtitle = JobTitle::where('name', 'like', $query.'%')->where(['is_public'=>1])->orWhere(['company_id'=>$user->company->id, 'job_index'=>1])->get(); //fixme
                $jobtitle = JobTitle::where('name', 'like', $query . '%')
                    ->where('job_index', '>', 0)
                    ->orWhere(function ($query) use ($user) {
                        $query->where('company_id', $user->company->id)
                            ->where('job_index', 0);
                    })
                    ->get();
            } else {
                $jobtitle = JobTitle::where('is_public', 1)->orWhere('company_id', $user->company->id)->get();
            }
        } else {
            if ($query) {
                $jobtitle = JobTitle::where('name', 'like', $query . '%')->get();
            } else {
                $jobtitle = JobTitle::all();
            }
        }
        return $jobtitle;
    }

    public function getJobTitleTotal($query = null)
    {
        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        if ($user->company) {
            if ($query) {
                // $jobtitle = JobTitle::where('name', 'like', $query.'%')->where(['is_public'=>1])->orWhere(['company_id'=>$user->company->id, 'job_index'=>1])->get(); //fixme
                $jobtitle = JobTitle::where('name', 'like', $query . '%')
                    ->where('job_index', '<>', null)
                    ->orWhere(function ($query) use ($user) {
                        $query->where('company_id', $user->company->id)
                            ->where('job_index', 0);
                    })
                    ->get();
            } else {
                $jobtitle = JobTitle::where('is_public', 1)->orWhere('company_id', $user->company->id)->get();
            }
        } else {
            if ($query) {
                $jobtitle = JobTitle::where('name', 'like', $query . '%')->get();
            } else {
                $jobtitle = JobTitle::all();
            }
        }
        return $jobtitle;
    }

    public function getJobCompanyTitle($query = null)
    {
        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        if ($user->company) {
            if ($query) {
                $jobtitle = JobTitle::where('name', 'like', $query . '%')->where(['company_id' => $user->company->id])->get(); //fixme
            } else {
                $jobtitle = JobTitle::where('company_id', $user->company->id)->get();
            }
        } else {
            if ($query) {
                $jobtitle = JobTitle::where('name', 'like', $query . '%')->get();
            } else {
                $jobtitle = JobTitle::all();
            }
        }
        return $jobtitle;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function store(Request $request)
    {
        $data = $request->all();

        Validator::make($data, [
            'job_title_id' => ['required', 'numeric'],
            'job_category_id' => ['required', 'numeric'],
            'department_id' => ['required', 'numeric'],
            'qualification_id' => ['required', 'numeric'],
            'job_experience_id' => ['required', 'numeric'],
            'job_type_id' => ['required', 'numeric'],
            'country_id' => ['required', 'numeric'],
            'state_id' => ['required', 'numeric'],
            'job_salary_type_id' => ['required'],
            'job_salary_term_id' => ['required'],
            'description' => ['required', 'string'],
            'responsibilities' => ['required', 'string'],
            'required_skills' => ['required', 'string'],
            'parent_id' => ['required', 'numeric'],
        ])->validate();

        $request->skillsList != "" ? $skills = explode(',', $request->skillsList) : $skills = null;

        $user = User::with(['company', 'tier.subscriptionTierNode'])->where('id', Auth::user()->id)->first();
        //checking for user node creation limit
        if ($user->tier) {
            $alreadyCreatedNodes = OrganizationChart::where('company_id', $user->company->id)->get();
            $nodesTotal = count($alreadyCreatedNodes) - 1; // minus 1 because we don't count root node
            $nodeLimit = $user->tier->subscriptionTierNode->amount;
            if ($nodesTotal >= $nodeLimit) {
                return response()->json(['errors' => ['msg' => ['Job cannot be added, Tier limit reached.']]], 500);
            }
        } else {
            $alreadyCreatedNodes = OrganizationChart::where('company_id', $user->company->id)->get();
            $nodesTotal = count($alreadyCreatedNodes) - 1; // minus 1 because we don't count root node
            $freeNodeLimit = env('FREE_NODES_AMOUNT', 10);
            if ($nodesTotal >= $freeNodeLimit) {
                return response()->json(['errors' => ['msg' => ['Job cannot be added, Free tier limit reached.']]], 500);
            }
        }
        //----------------

        //Saving Job first in Database
        $job = new Job();
        $job->job_key = Str::uuid();
        $job->job_title_id = $request->job_title_id;
        $job->company_id = $user->company->id;
        $job->department_id = $request->department_id;
        $job->job_category_id = $request->job_category_id;
        $job->job_experience_id = $request->job_experience_id;
        $job->qualification_id = $request->qualification_id;
        $job->job_type_id = $request->job_type_id;
        $job->country_id = $request->country_id;
        $job->state_id = $request->state_id;
        $job->city_id = $request->city_id;
        $job->description = $request->description;
        $job->responsibilities = $request->responsibilities;
        $job->required_skills = $request->required_skills;
        $job->job_salary_term_id = $request->job_salary_term_id;

        if ($request->min_salary && $request->min_salary) {
            $job->min_salary = $request->min_salary;
            $job->max_salary = $request->max_salary;
            $job->job_salary_type_id = $request->job_salary_type_id;
        } else {
            $job->job_salary_type_id = $request->job_salary_type_id;
        }

        $job->save();

        //Saving job skills
        if ($skills) {
            foreach ($skills as $skill) {
                $jobSkill = new JobSkill();
                $jobSkill->job_id = $job->id;
                $jobSkill->skill_id = $skill;
                $jobSkill->save();
            }
        }

        // Saving job rewards
        if (!empty($request->rewards)) {
            $rewards = $request->rewards;
            foreach ($rewards as $key => $val) {
                $jobRewards = new JobRewards();
                $jobRewards->job_id = $job->id;
                $jobRewards->reward_id = $val;
                $jobRewards->save();
            }
        }
        //Creating respective Node in Chart for this Job
        $chartNode = new OrganizationChart();
        $chartNode->parent_id = $request->parent_id;
        $chartNode->company_id = $user->company->id;
        $chartNode->job_id = $job->id;
        $chartNode->dp = $this->generateAvatar();
        $chartNode->save();

        $chartNode = OrganizationChart::with(['job.title'])->where('id', $chartNode->id)->first();

        // //create new job template
        // $jobIndex = 0;
        // $existingJobTemplate = JobCompanyTemplate::where(['company_id' => $user->company->id, 'job_title_id' => $request->job_title_id])->latest('created_at')->first();
        // if($existingJobTemplate) {
        //     $jobIndex = $existingJobTemplate -> job_index + 1;
        // };

        // $jobTemplate = new JobCompanyTemplate();
        // $jobTemplate -> job_key = Str::uuid();
        // $jobTemplate -> added_by = $user->username;
        // $jobTemplate -> job_title_id = $request->job_title_id;
        // $jobTemplate -> job_index = $jobIndex;
        // $jobTemplate -> company_id = $user->company->id;
        // $jobTemplate -> department_id = $request->department_id;
        // $jobTemplate -> job_category_id = $request->job_category_id;
        // $jobTemplate -> job_experience_id = $request->job_experience_id;
        // $jobTemplate -> qualification_id = $request->qualification_id;
        // $jobTemplate -> job_type_id = $request->job_type_id;
        // $jobTemplate -> job_salary_term_id = $request->job_salary_term_id;
        // $jobTemplate -> description = $request->description;
        // $jobTemplate -> responsibilities = $request->responsibilities;
        // $jobTemplate -> required_skills = $request->required_skills;

        // if($request->min_salary && $request->min_salary) {
        //     $jobTemplate -> min_salary = $request->min_salary;
        //     $jobTemplate -> max_salary = $request->max_salary;
        //     $jobTemplate -> job_salary_type_id = $request->job_salary_type_id;
        // }else{
        //     $jobTemplate -> job_salary_type_id = $request->job_salary_type_id;
        // }

        // $jobTemplate->save();

        // //Saving job skills
        // if($skills) {
        //     foreach ($skills as $skill) {
        //         $jobSkill = new JobTemplateSkill();
        //         $jobSkill->job_template_id = $jobTemplate->id;
        //         $jobSkill->skill_id = $skill;
        //         $jobSkill->save();
        //     }
        // }

        //Returning Node because It has to be added in the chart on runtime.
        return $chartNode;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Job  $job
     * @return Response
     */
    public function show(Job $job)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job  $job
     * @return Response
     */
    public function edit(Request $request)
    {
        $data = $request->all();

        Validator::make($data, [
            'job_title_id' => ['required', 'numeric'],
            'job_category_id' => ['required', 'numeric'],
            'department_id' => ['required', 'numeric'],
            'qualification_id' => ['required', 'numeric'],
            'job_experience_id' => ['required', 'numeric'],
            'job_type_id' => ['required', 'numeric'],
            'country_id' => ['required', 'numeric'],
            'state_id' => ['required', 'numeric'],
            'job_salary_type_id' => ['required'],
            'description' => ['required', 'string'],
            'responsibilities' => ['required', 'string'],
            'required_skills' => ['required', 'string'],
            'job_id' => ['required', 'numeric'],
        ])->validate();

        if (isset($request->skillsList)) {
            $skills = explode(',', $request->skillsList);
        } else {
            $skills = null;
        }

        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $job = Job::findOrFail($request->job_id);
        if ($job) {
            $job->job_title_id = $request->job_title_id;
            $job->company_id = $user->company->id;
            $job->department_id = $request->department_id;
            $job->job_category_id = $request->job_category_id;
            $job->job_experience_id = $request->job_experience_id;
            $job->qualification_id = $request->qualification_id;
            $job->job_type_id = $request->job_type_id;
            $job->country_id = $request->country_id;
            $job->state_id = $request->state_id;
            $job->city_id = $request->city_id;
            $job->description = $request->description;
            $job->responsibilities = $request->responsibilities;
            $job->required_skills = $request->required_skills;
            $job->job_salary_term_id = $request->job_salary_term_id;

            if ($request->min_salary && $request->max_salary) {
                $job->min_salary = $request->min_salary;
                $job->max_salary = $request->max_salary;
                $job->job_salary_type_id = $request->job_salary_type_id;
            } else {
                $job->job_salary_type_id = $request->job_salary_type_id;
            }

            $job->save();

            //Deleting old skills
            $oldSkills = JobSkill::where('job_id', $job->id)->delete();
            //Saving job skills
            if ($skills) {
                foreach ($skills as $skill) {
                    $jobSkill = new JobSkill();
                    $jobSkill->job_id = $job->id;
                    $jobSkill->skill_id = $skill;
                    $jobSkill->save();
                }
            }
            //Deleting old rewards
            $oldRewards = JobRewards::where('job_id', $job->id)->delete();
            // Saving job rewards
            if (isset($request->rewards)) {
                $rewards = $request->rewards;
            } else {
                $rewards = null;
            }

            if ($rewards) {
                foreach ($rewards as $key => $val) {
                    $jobRewards = new JobRewards();
                    $jobRewards->job_id = $job->id;
                    $jobRewards->reward_id = $val;
                    $jobRewards->save();
                }
            }

        }
        $chartNode = OrganizationChart::with(['job.title', 'children', 'job.candidate.user'])->where('job_id', $request->job_id)->first();
        $children = [];
        $orgchartObject = new OrganizationChartController();
        $orgchartObject->findAllChildren($chartNode->id, $children);
        $chartNode->childCount = count($children);

        //update job company template
        if ($request->job_template_id) {
            $jobTemplate = JobCompanyTemplate::findOrFail($request->job_template_id); //fixme
            if ($jobTemplate) {
                $jobTemplate->job_title_id = $request->job_title_id;
                // $jobTemplate -> added_by = $request->added_by;
                $jobTemplate->company_id = $user->company->id;
                $jobTemplate->department_id = $request->department_id;
                $jobTemplate->job_category_id = $request->job_category_id;
                $jobTemplate->job_experience_id = $request->job_experience_id;
                $jobTemplate->qualification_id = $request->qualification_id;
                $jobTemplate->job_type_id = $request->job_type_id;
                $jobTemplate->job_salary_term_id = $request->job_salary_term_id;
                $jobTemplate->description = $request->description;
                $jobTemplate->responsibilities = $request->responsibilities;
                $jobTemplate->required_skills = $request->required_skills;

                if ($request->min_salary && $request->min_salary) {
                    $jobTemplate->min_salary = $request->min_salary;
                    $jobTemplate->max_salary = $request->max_salary;
                    $jobTemplate->job_salary_type_id = $request->job_salary_type_id;
                } else {
                    $jobTemplate->job_salary_type_id = $request->job_salary_type_id;
                }

                $jobTemplate->save();

                //Deleting old skills
                $oldSkills = JobTemplateSkill::where('job_template_id', $jobTemplate->id)->delete();
                //Saving job skills
                if ($skills) {
                    foreach ($skills as $skill) {
                        $jobSkill = new JobTemplateSkill();
                        $jobSkill->job_template_id = $jobTemplate->id;
                        $jobSkill->skill_id = $skill;
                        $jobSkill->save();
                    }
                }
            }
        }

        //Returning Node because It has to be added in the chart on runtime.
        return $chartNode;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Job  $job
     * @return Response
     */
    public function update(Request $request, Job $job)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Job  $job
     * @return Response
     */
    public function destroy(Job $job)
    {
        //
    }

    public function getJobDetails($nodeId)
    {
        try {
            $user = User::with('candidate')->where('id', Auth::user()->id)->first();
            $chartNode = OrganizationChart::where('id', $nodeId)->first();
            if ($chartNode->job_id) {
                if ($user->candidate) {
                    $jobDetails = Job::with([
                        'jobApplications' => function ($query) use ($user) {
                            $query->where('candidate_id', $user->candidate->id);
                        },
                        'title', 'company.user', 'candidate', 'department', 'rewards.reward', 'skills.skill',
                        'category', 'experience', 'qualification', 'jobType', 'salaryType', 'country', 'state', 'city', 'salaryTerm', 'favourites'])
                        ->where('id', $chartNode->job_id)
                        ->first();
                } else {
                    $jobDetails = Job::with([
                        'title', 'company.user', 'candidate', 'department', 'rewards.reward', 'skills.skill',
                        'category', 'experience', 'qualification', 'jobType', 'salaryType', 'country', 'state', 'city', 'salaryTerm', 'favourites'])
                        ->where('id', $chartNode->job_id)
                        ->first();
                }

                return $jobDetails;

            } else {
                return response()->json(['errors' => ['msg' => ['Job Does Not Exist.']]], 401);
            }
        } catch (Exception $err) {
            return response()->json(['errors' => ['msg' => $err . message]], 500);
        }
    }

    public function showJobDetails($job_id)
    {

        if (Auth::user()) { //For Logged In User
            $user = User::with('candidate')->where('id', Auth::user()->id)->first();

            $jobDetails = Job::with([
                'jobApplications' => function ($query) use ($user) {
                    $query->where('candidate_id', $user->candidate->id);
                },
                'company.user' => function ($select) {
                    $select->select('id', 'image', 'country_id', 'state_id', 'city_id', 'date_of_birth');
                },
                'department', 'rewards.reward', 'skills.skill', 'category', 'experience', 'title',
                'qualification', 'jobType', 'salaryTerm', 'salaryType', 'country', 'state', 'city', 'favourites',
                'company.user.country', 'company.user.state', 'company.user.city',
            ])
                ->where('id', $job_id)
                ->first();
            return $jobDetails;

        } else { //For Guest Users
            $jobDetails = Job::with(['company.user' => function ($select) {
                $select->select('id', 'image', 'country_id', 'state_id', 'city_id');
            },
                'department', 'rewards.reward', 'skills.skill', 'category', 'experience', 'title',
                'qualification', 'jobType', 'salaryTerm', 'salaryType', 'country', 'state', 'city', 'favourites',
                'company.user.country', 'company.user.state', 'company.user.city',
            ])
                ->where('id', $job_id)
                ->first();
            return $jobDetails;
        }

    }

    /**
     * Download job detail in pdf .
     *
     * @param  \App\Job  $job
     * @return Response
     */
    public function downloadJobDetails($job_id)
    {

        $jobDetails = Job::with([
            'company.user' => function ($select) {
                $select->select('id', 'image', 'country_id', 'state_id', 'city_id');
            },
            'department', 'rewards.reward', 'skills.skill', 'category', 'experience', 'title',
            'qualification', 'jobType', 'salaryTerm', 'salaryType', 'country', 'state', 'city',
            'company.user.country', 'company.user.state', 'company.user.city',
        ])
            ->where('id', $job_id)
            ->first();
        $data['jobDetail'] = $jobDetails;
        $jobTitle = $jobDetails->title->name;
        // Send data to the view using loadView function of PDF facade
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'dpi' => 150,
            'defaultFont' => 'Poppins'])->loadView('pdf.job', $data)->setPaper('letter', 'portrait');
        // return $pdf->stream();
        // $pdf->save(storage_path().'_filename.pdf');
        // Finally, you can download the file using download function
        return $pdf->download($jobTitle . '.pdf');
        // return view('pdf.job')->with('jobDetail',$jobDetails);
    }

    /**
     * Apply on job and save in database.
     *
     * @param  \App\Job  $job
     * @return Response
     */
    public function applyJob(Request $request)
    {
        try {
            $jobId = $request->jobId;
            $totalApplicant = $this->getTotalApplicant($jobId);
            $candidate = Candidate::where('user_id', Auth::user()->id)->first();
            $candidate_id = $candidate->id;
            $user_id = $candidate->user_id;
            $user = User::where('id', $user_id)->first();
            $candidateName = $user->first_name;
            $email = $user->email;

            $jobDetails = Job::with(['title', 'company'])->where('id', $jobId)->first();
            //dd($jobDetails->company);
            $jobTitle = $jobDetails->title->name;
            $companyName = $jobDetails->company->name;
            $sameJobs = Job::where('job_key', $jobDetails->job_key)->pluck('id')->toArray();
            $checkApplicant = JobApplicant::where('candidate_id', $candidate->id)->whereIn('job_id', $sameJobs)->first();

            if (!$checkApplicant) {
                $jobApplicant = new JobApplicant();
                $jobApplicant->job_id = $jobId;
                $jobApplicant->candidate_id = $candidate_id;
                $jobApplicant->is_trash = 0;
                $jobApplicant->is_shortlisted = 0;
                $jobApplicant->is_offered = 0;
                $jobApplicant->is_rejected = 0;
                $jobApplicant->save();

                $updateJob = Job::find($jobId);
                $updateJob->has_applicants = 1;
                $updateJob->total_applies = $totalApplicant + 1;
                $updateJob->save();

                $data = [];
                $data['candidateName'] = $candidateName;
                $data['jobTitle'] = $jobTitle;
                $data['companyName'] = $companyName;
                $to = $email;
                Mail::to($to)->send(new ApplyJob($data));

                $appliedJobs = JobApplicant::with(['candidate', 'job.qualification', 'job.country', 'job.state', 'job.city', 'job.title',
                    'job.company.user' => function ($query) {
                        $query->select('id', 'image');
                    }
                    , 'job' => function ($query) {
                        $query->where('status', 1)->limit(10)->select('id', 'job_key', 'job_title_id', 'qualification_id', 'company_id',
                            'country_id', 'state_id', 'city_id', 'status');
                    },
                ])->where('candidate_id', $candidate_id)->where('candidate_viewable', 1)->get();

                return $appliedJobs;

            } else {
                return response()->json(['errors' => ['msg' => ['You already applied for this job.']]], 401);
            }
        } catch (Exception $err) {
            return response()->json(['errors' => $err . message], 500);
        }
    }

    public function applyJobMail()
    {
        $id = 3;
        DB::enableQueryLog(); // Enable query log
        $jobDetail = JobApplicant::with(['candidate', 'job.title', 'candidate.user' => function ($query) {
            $query->select('id', 'first_name');
        }, 'job' => function ($query) {
            $query->select('id', 'job_key', 'job_title_id');
        },
        ])->where('id', $id)->first();
        // dd(DB::getQueryLog()); // Show results of log
        // dd($jobDetail);
        return $jobDetail;
        $data = [];
        $data['candidateName'] = 'David';
        $data['jobTitle'] = "Vue Js Developer";
        $data['companyName'] = "Company Name";
        // Mail::to('khababhassan@gmail.com')->send(new ApplyJob($data));
    }

    public function getTotalApplicant($jobId)
    {

        $totalApplicant = Job::where('id', $jobId)->value('total_applies'); //inside controller function
        return $totalApplicant;
    }

    public function getNewJobs()
    {
        $user = User::with(['candidate'])->where('id', Auth::user()->id)->first();
        $candidate_id = $user->candidate->id;
        //DB::enableQueryLog(); // Enable query log

        $getJobs = JobApplicant::with(['candidate', 'job', 'job.qualification', 'job.country', 'job.state', 'job.city', 'job.title',
            'job.company.user' => function ($query) {
                $query->select('id', 'image');
            },
        ])->where('candidate_id', '!=', $candidate_id)->get();
        //dd(DB::getQueryLog()); // Show results of log
        return $getJobs;
    }

    public function getAppliedJobs()
    {
        $user = User::with(['candidate'])->where('id', Auth::user()->id)->first();
        $candidate_id = $user->candidate->id;

        $appliedJobs = JobApplicant::with(['candidate', 'job', 'job.qualification', 'job.country', 'job.state', 'job.city', 'job.title',
            'job.company.user' => function ($query) {
                $query->select('id', 'image');
            },
        ])->where('candidate_id', $candidate_id)->get();

        return $appliedJobs;
    }

    public function addJobTitle(Request $request)
    {
        Validator::make($request->all(), [
            'title' => ['required', 'string'],
        ])->validate();

        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $newJobTitle = JobTitle::where('name', ucwords($request->title))->first();
        if (!$newJobTitle) {
            $newJobTitle = new JobTitle();
            $newJobTitle->name = ucwords($request->title);
            $newJobTitle->company_id = $user->company->id;
            $newJobTitle->job_index = 0;
            $newJobTitle->is_public = 0; //fixme
            $newJobTitle->save();
        }
        return $newJobTitle;
    }

    public function searchJobs(Request $request)
    {
        $user = User::with(['candidate'])->where('id', Auth::user()->id)->first();
        $jobList = Job::with(
            ['title', 'qualification', 'country', 'state', 'city',
                'favourites' => function ($query) use ($user) {
                    $query->where('candidate_id', $user->candidate->id);
                },
                'company.user' => function ($select) {
                    $select->select('id', 'image');
                },
            ]
        )
            ->select('id', 'job_key', 'job_title_id', 'qualification_id', 'company_id', 'country_id', 'state_id', 'city_id', 'status')
            ->where('status', $request->isOnlineValue)
            ->groupBy('job_title_id');

        if ($request->filled("jobTitle")) {
            $jobList->whereHas('title', function ($query) use ($request) {
                $query->where('id', $request->jobTitle);
            });
        }
        if ($request->filled("jobCategory")) {
            $jobList->whereHas('category', function ($query) use ($request) {
                $query->where('id', $request->jobCategory);
            });
        }
        if ($request->filled("state")) {
            $jobList->whereHas('state', function ($query) use ($request) {
                $query->where('id', $request->state);
            });
        }
        if ($request->filled("city")) {
            $jobList->whereHas('city', function ($query) use ($request) {
                $query->where('id', $request->city);
            });
        }
        if ($request->filled("skill")) {
            $jobList->whereHas('skills', function ($query) use ($request) {
                $query->where('id', $request->skill);
            });
        }
        if ($request->filled("qualification")) {
            $jobList->whereHas('qualification', function ($query) use ($request) {
                $query->where('id', $request->qualification);
            });
        }
        if ($request->filled("experience")) {
            $jobList->whereHas('experience', function ($query) use ($request) {
                $query->where('id', $request->experience);
            });
        }
        if ($request->filled("jobType")) {
            $jobList->whereHas('jobType', function ($query) use ($request) {
                $query->where('id', $request->jobType);
            });
        }
        if ($request->filled("department")) {
            $jobList->whereHas('department', function ($query) use ($request) {
                $query->where('id', $request->department);
            });
        }

        $jobList = $jobList->get();
        return $jobList;
    }

    public function getJobsList()
    {
        $user = User::with(['candidate'])->where('id', Auth::user()->id)->first();
        $jobDetails = Job::with(['title', 'company.user', 'qualification', 'country', 'state', 'city',
            'favourites' => function ($query) use ($user) {
                $query->where('candidate_id', $user->candidate->id);
            },
        ])
            ->select('id', 'job_key', 'job_title_id', 'qualification_id', 'company_id', 'country_id', 'state_id',
                'city_id', 'status')
            ->where('status', 1)
            ->groupBy('job_title_id')->get();
        return $jobDetails;
    }
}
