<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Company;
use App\CompanyMember;
use App\Job;
use App\JobTemplate;
use App\JobApplicant;
use App\Mail\AcceptJobOffer;
use App\Mail\JobOffer;
use App\Mail\RejectCandidate;
use App\Mail\RejectJobOffer;
use App\Mail\ShortlistCandidate;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class JobApplicantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobApplicant  $jobApplicant
     * @return \Illuminate\Http\Response
     */
    public function show(JobApplicant $jobApplicant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobApplicant  $jobApplicant
     * @return \Illuminate\Http\Response
     */
    public function edit(JobApplicant $jobApplicant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobApplicant  $jobApplicant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobApplicant $jobApplicant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobApplicant  $jobApplicant
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobApplicant $jobApplicant)
    {
        //
    }

    /**
     * Get Online jobs from storage.
     */
    public function getOnlineJobsList()
    {

        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $company_id = $user->company->id;
        //DB::enableQueryLog();
        $onlineJobs = Job::with(['title', 'company'])->select('id', 'job_key', 'job_title_id', 'company_id', 'status', 'end_date')
            ->where('status', 1)
            ->where('company_id', $company_id)
            ->groupBy('job_key')
            ->get();
        return $onlineJobs;
    }

    /**
     * Get Offline jobs from storage.
     */
    public function getOfflineJobsList()
    {

        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $company_id = $user->company->id;
        //DB::enableQueryLog();
        $onlineJobs = Job::with(['title', 'company'])->select('id', 'job_key', 'job_title_id', 'company_id', 'status')
            ->where('status', 0)
            ->where('company_id', $company_id)->whereHas('jobApplications', function ($query) {
            $query->where('is_trash', 0);
        })
            ->groupBy('job_key')
            ->get();
        return $onlineJobs;
    }

    public function getOfflineJobsTotal($page)
    {
        // $onlineJobs = JobTemplate::with(['title', 'company.user'])->select('id', 'job_key', 'job_title_id', 'status')
        //     ->where('status', 0)
        //     ->where('company_id', $company_id)
        //     ->groupBy('job_key')
        //     ->get();

        $onlineJobs = JobTemplate::with(['title', 'company.user' => function ($select) {
            $select->select('id', 'image');
        }, 'department', 'rewards.reward', 'skills.skill',
            'category', 'experience', 'qualification', 'jobType', 'salaryType', 'country', 'state', 'city'])
            ->where('status', 0)
            ->paginate(20, ['*'], 'page', $page);
            // ->get();
        return $onlineJobs;
    }

    /**
     * Get Online jobs resume from storage.
     */
    public function getOnlineJobsResume()
    {

        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $company_id = $user->company->id;
        //DB::enableQueryLog();
        $jobResume = JobApplicant::with(['candidate.user' => function ($select) {
            $select->select('id', 'image', 'first_name', 'last_name', 'email');
        }, 'job.title' => function ($selectQuery) {
            $selectQuery->select('id', 'name');
        }, 'job' => function ($query) use ($company_id) {

            $query->select('id', 'job_key', 'job_title_id', 'company_id', 'status')
                ->where('status', 1)->where('company_id', $company_id);
        }])->where('is_offered', 0)
            ->where('is_shortlisted', 0)
            ->where('is_rejected', 0)->whereHas('job', function ($query) use ($company_id) {
            $query->where('status', 1)->where('company_id', $company_id);
        })->get();
        //dd(DB::getQueryLog());
        return $jobResume ? $jobResume : null;
    }

    /**
     * Get Offline jobs resume from storage.
     */
    public function getOfflineJobsResume()
    {

        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $company_id = $user->company->id;
        //DB::enableQueryLog();
        $jobResume = JobApplicant::with(['candidate.user' => function ($select) {
            $select->select('id', 'image', 'first_name', 'last_name', 'email');
        }, 'job.title' => function ($selectQuery) {
            $selectQuery->select('id', 'name');
        }, 'job' => function ($query) use ($company_id) {
            $query->where('status', 0)->where('company_id', $company_id)->select('id', 'job_key', 'job_title_id', 'company_id', 'status');
        }])->where('is_offered', 0)->where('is_shortlisted', 0)->where('is_rejected', 0)
            ->whereHas('job', function ($query) use ($company_id) {
                $query->where('status', 0)->where('company_id', $company_id);
            })->get();
        return $jobResume;
    }

    /**
     * Get Online job Applicants resume from storage.
     */
    public function getCandidateJobApplicants($column, $value, $status = 1)
    {
        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $company_id = $user->company->id;
        $jobResume = JobApplicant::with(['candidate.user' => function ($select) {
            $select->select('id', 'image', 'first_name', 'last_name', 'email');
        }, 'job.title' => function ($selectQuery) {
            $selectQuery->select('id', 'name');
        }, 'job' => function ($query) use ($company_id, $status) {
            $query->where('status', $status)->where('company_id', $company_id)->select('id', 'job_key', 'job_title_id', 'company_id', 'status');
        }])->where($column, $value)->whereHas('job', function ($query) use ($status, $company_id) {
            $query->where('status', $status)->where('company_id', $company_id);
        })->get();
        return $jobResume ? $jobResume : null;
    }

    /**
     * Get Online job Applicants resume from storage.
     */
    public function getCandidateOfflineJobApplicants($column, $value, $status = 0)
    {
        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $company_id = $user->company->id;
        $jobResume = JobApplicant::with(['candidate.user' => function ($select) {
            $select->select('id', 'image', 'first_name', 'last_name', 'email');
        }, 'job.title' => function ($selectQuery) {
            $selectQuery->select('id', 'name');
        }, 'job' => function ($query) use ($company_id, $status) {
            $query->where('status', $status)->where('company_id', $company_id)->select('id', 'job_key', 'job_title_id', 'company_id', 'status');
        }])->where($column, $value)
            ->whereHas('job', function ($query) use ($company_id) {
                $query->where('status', 0)->where('company_id', $company_id);
            })->get();
        return $jobResume ? $jobResume : null;
    }

    /**
     * Offered job.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function updateApplicant(Request $request)
    {
        $updateApplicant = JobApplicant::find($request->id);
        $response = new \stdClass();
        if ($updateApplicant) {
            $jobDetail = JobApplicant::with(['candidate', 'job.title', 'candidate.user' => function ($query) {
                $query->select('id', 'first_name', 'last_name', 'email');
            }, 'job' => function ($query) {
                $query->select('id', 'job_key', 'job_title_id');
            },
            ])->where('id', $request->id)->first();
            $jobId = $jobDetail->job_id;
            $candidate_id = $jobDetail->candidate_id;
            $candidateName = $jobDetail->candidate->user->first_name;
            $candidateEmail = $jobDetail->candidate->user->email;
            $job_title = $jobDetail->job->title->name;
            $companyid = Job::where('id', $jobId)->value('company_id');
            $companyname = Company::where('id', $companyid)->value('name');

            if ($request->type == 'is_shortlisted') {
                $updateApplicant->is_shortlisted = 1;
                $updateApplicant->is_offered = 0;
                $updateApplicant->is_rejected = 0;
                $data = [];
                $data['candidateName'] = $candidateName;
                $data['jobTitle'] = $job_title;
                $data['companyName'] = $companyname;
                //$to = env('MAIL_TO_ADDRESS') ?:"info@onecrate.com";
                $to = $candidateEmail;
                Mail::to($to)->send(new ShortlistCandidate($data));

            }
            if ($request->type == 'is_offered') {
                $updateApplicant->is_offered = 1;
                $updateApplicant->is_rejected = 0;
                $updateApplicant->is_shortlisted = 0;
                $data = [];
                $data['candidateName'] = $candidateName;
                $data['jobTitle'] = $job_title;
                $data['companyName'] = $companyname;
                //$to = env('MAIL_TO_ADDRESS') ?:"info@onecrate.com";
                $to = $candidateEmail;
                Mail::to($to)->send(new JobOffer($data));
            }
            if ($request->type == 'is_rejected') {
                $updateApplicant->is_offered = 0;
                $updateApplicant->is_rejected = 1;
                $updateApplicant->is_shortlisted = 0;

                $data = [];
                $data['candidateName'] = $candidateName;
                $data['jobTitle'] = $job_title;
                $data['companyName'] = $companyname;
                //$to = env('MAIL_TO_ADDRESS') ?:"info@onecrate.com";
                $to = $candidateEmail;
                Mail::to($to)->send(new RejectCandidate($data));
            }
            $updateApplicant->save();
            $response->rejectedCandidates = $this->getCandidateJobApplicants('is_rejected', '1');
            $response->offeredCandidates = $this->getCandidateJobApplicants('is_offered', '1');
            $response->shortlistedCandidates = $this->getCandidateJobApplicants('is_shortlisted', '1');
            $response->allCandidates = $this->getOnlineJobsResume();
        }
        return json_encode($response);
    }

    /**
     * Offered job.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function updateOfflineApplicant(Request $request)
    {
        $updateApplicant = JobApplicant::find($request->id);
        $response = new \stdClass();
        if ($updateApplicant) {
            $jobDetail = JobApplicant::with(['candidate', 'job.title', 'candidate.user' => function ($query) {
                $query->select('id', 'first_name', 'last_name', 'email');
            }, 'job' => function ($query) {
                $query->select('id', 'job_key', 'job_title_id');
            },
            ])->where('id', $request->id)->first();
            $jobId = $jobDetail->job_id;
            $candidate_id = $jobDetail->candidate_id;
            $candidateName = $jobDetail->candidate->user->first_name;
            $candidateEmail = $jobDetail->candidate->user->email;
            $job_title = $jobDetail->job->title->name;
            $companyid = Job::where('id', $jobId)->value('company_id');
            $companyname = Company::where('id', $companyid)->value('name');

            if ($request->type == 'is_shortlisted') {
                $updateApplicant->is_shortlisted = 1;
                $updateApplicant->is_offered = 0;
                $updateApplicant->is_rejected = 0;
                $data = [];
                $data['candidateName'] = $candidateName;
                $data['jobTitle'] = $job_title;
                $data['companyName'] = $companyname;
                //$to = env('MAIL_TO_ADDRESS') ?:"info@onecrate.com";
                $to = $candidateEmail;
                Mail::to($to)->send(new ShortlistCandidate($data));
            }
            if ($request->type == 'is_offered') {
                $updateApplicant->is_offered = 1;
                $updateApplicant->is_rejected = 0;
                $updateApplicant->is_shortlisted = 0;
                $data = [];
                $data['candidateName'] = $candidateName;
                $data['jobTitle'] = $job_title;
                $data['companyName'] = $companyname;
                //$to = env('MAIL_TO_ADDRESS') ?:"info@onecrate.com";
                $to = $candidateEmail;
                Mail::to($to)->send(new JobOffer($data));
            }
            if ($request->type == 'is_rejected') {
                $updateApplicant->is_offered = 0;
                $updateApplicant->is_rejected = 1;
                $updateApplicant->is_shortlisted = 0;
                $data = [];
                $data['candidateName'] = $candidateName;
                $data['jobTitle'] = $job_title;
                $data['companyName'] = $companyname;
                //$to = env('MAIL_TO_ADDRESS') ?:"info@onecrate.com";
                $to = $candidateEmail;
                Mail::to($to)->send(new RejectCandidate($data));
            }
            $updateApplicant->save();
            $response->rejectedCandidates = $this->getCandidateOfflineJobApplicants('is_rejected', '1');
            $response->offeredCandidates = $this->getCandidateOfflineJobApplicants('is_offered', '1');
            $response->shortlistedCandidates = $this->getCandidateOfflineJobApplicants('is_shortlisted', '1');
            $response->allCandidates = $this->getOfflineJobsResume();
        }
        return json_encode($response);
    }

    public function getCandidateOffersList()
    {
        $candidate = Candidate::where('user_id', Auth::user()->id)->first();
        $candidate_id = $candidate->id;
        $companyList = JobApplicant::with(['job.company' => function ($selectCompany) {
            $selectCompany = $selectCompany->select('id', 'name', 'user_id');
        }, 'job.company.user' => function ($select) {
            $select->select('id', 'image', 'email');
        }, 'job' => function ($query) {
            $query = $query->select('id', 'company_id', 'status');
        }])->where('candidate_id', $candidate_id)->where('is_offered', 1)
            ->where('candidate_status', null)->get();
        return $companyList;

    }

    public function getNewOffersList()
    {
        $candidate = Candidate::where('user_id', Auth::user()->id)->first();
        $candidate_id = $candidate->id;
        $companyList = JobApplicant::with(['job.company' => function ($selectCompany) {
            $selectCompany = $selectCompany->select('id', 'name', 'user_id');
        }, 'job.company.user' => function ($select) {
            $select->select('id', 'image', 'email');
        }, 'job' => function ($query) {
            $query = $query->select('id', 'company_id', 'status');
        }])->where('candidate_id', $candidate_id)->where('is_offered', 1)
            ->where('view_offer', 0)->where('candidate_status', null)->get();
        return $companyList;

    }

    public function getJobOffers(Request $request)
    {
        $id = $request->id;
        $candidate = Candidate::where('user_id', Auth::user()->id)->first();
        $candidate_id = $candidate->id;
        $companyList = JobApplicant::with(['job.title', 'job.company', 'job.company.businessType', 'job.company.industry',
            'job.company.user', 'job.company.user.country', 'job.company.user.state', 'job.company.user.city',
            'candidate.companyDetails.company', 'job' => function ($query) {
                $query = $query->select('id', 'company_id', 'status', 'job_title_id');
            }])->where('id', $id)->first();

        $jobApplicant = JobApplicant::find($id);
        $jobApplicant->view_offer = 1;
        $jobApplicant->save();

        $companyName = $companyList->job->company->name;
        $oldCompany = 'Old Company';
        $companyList->jobOffer = __('messages.jobOffer');
        //$companyList->message = __('messages.AlreadyCompanyjobOffer',['companyName' => $companyName,'oldCompany'=>$oldCompany]);

        return $companyList;
    }

    public function alreadyCompanyMember(Request $request)
    {
        $id = $request->id;
        $candidate = Candidate::where('user_id', Auth::user()->id)->first();
        $candidate_id = $candidate->id;
        $memberInfo = CompanyMember::with(['company'])->where('candidate_id', $candidate_id)
            ->where('status', 1)->first();
        if ($memberInfo) {

            $memberInfo->alreadyMember = __('messages.alreadyMember');
            $memberInfo->removeCompany = __('messages.removeCompany');
        }
        return $memberInfo;
    }

    public function acceptOffer(Request $request)
    {
        $applicantId = $request->applicantId;
        $candidate = Candidate::where('user_id', Auth::user()->id)->first();
        if ($candidate) {
            $candidate_id = $candidate->id;
            $jobInfo = JobApplicant::with(['job' => function ($query) {
                $query = $query->select('id', 'company_id', 'job_key');
            }])->where('id', $applicantId)->first();

            $company_id = $jobInfo->job->company_id;
            $job_key = $jobInfo->job->job_key;
            $job_id = Job::where('job_key', $job_key)->where('candidate_id', null)->value('id');

            //soft deleting old company entry to remove user from that company
            $old_company = CompanyMember::where('candidate_id', $candidate_id)->where('company_id', '!=', $company_id)->first();
            if ($old_company) {
                if ($old_company->job_id) {
                    $old_job = Job::where('id', $old_company->job_id)->first();
                    //detach old job
                    if ($old_job) {
                        $old_job->candidate_id = null;
                        $old_job->save();
                    }
                }
                $old_company->delete();
            }

            //if there is an available job then hire the candidate on that job
            if ($job_id) {
                //checking if user is already a member of company
                $isAlreadyMember = CompanyMember::where('company_id', $company_id)->where('candidate_id', $candidate_id)->first();

                if ($isAlreadyMember) {
                    if (isset($isAlreadyMember->job_id) && $isAlreadyMember->job_id != $job_id) {
                        $updateJob = Job::where('id', $isAlreadyMember->job_id)->first();
                        $updateJob->candidate_id = null;
                        $updateJob->save();
                    }
                    $isAlreadyMember->status = 1;
                    $isAlreadyMember->job_id = $job_id;
                    $isAlreadyMember->save();
                } else {
                    $companyMember = new CompanyMember();
                    $companyMember->company_id = $company_id;
                    $companyMember->candidate_id = $candidate_id;
                    $companyMember->job_id = $job_id;
                    $companyMember->status = 1;
                    $companyMember->save();
                }

                $updateJob = Job::where('id', $job_id)->first();
                $updateJob->candidate_id = $candidate_id;
                $updateJob->save();
            } else {
                //if there is no available job, just hire the candidate and leave him in company
                $isAlreadyMember = CompanyMember::where('company_id', $company_id)->where('candidate_id', $candidate_id)->first();

                if ($isAlreadyMember) {
                    if (isset($isAlreadyMember->job_id)) {
                        $updateJob = Job::where('id', $isAlreadyMember->job_id)->first();
                        $updateJob->candidate_id = null;
                        $updateJob->save();
                    }
                } else {
                    $companyMember = new CompanyMember();
                    $companyMember->company_id = $company_id;
                    $companyMember->candidate_id = $candidate_id;
                    $companyMember->status = 1;
                    $companyMember->save();
                }
            }

            $jobDetail = JobApplicant::with(['candidate', 'job.title',
                'candidate.user' => function ($query) {
                    $query->select('id', 'first_name', 'last_name', 'email');
                }, 'job' => function ($query) {
                    $query->select('id', 'job_key', 'job_title_id');
                },
            ])->where('id', $applicantId)->first();

            //removing application from DB
            $removeApplicant = JobApplicant::where('id', $applicantId)->first();
            $removeApplicant->delete();

            /***************** Retrun Data ******************/
            $companyList = JobApplicant::with(['job.company' => function ($selectCompany) {
                $selectCompany = $selectCompany->select('id', 'name', 'user_id');
            }, 'job.company.user' => function ($select) {
                $select->select('id', 'image', 'email');
            }, 'job' => function ($query) {
                $query = $query->select('id', 'company_id', 'status');
            }])->where([['candidate_id', $candidate_id], ['is_offered', 1], ['candidate_status', null]])->get();

            //Sending email in end
            //Mailing Data
            $candidateName = $jobDetail->candidate->user->first_name;
            $candidateEmail = $jobDetail->candidate->user->email;
            $job_title = $jobDetail->job->title->name;
            $companyname = Company::where('id', $company_id)->value('name');
            $data = [];
            $data['candidateName'] = $candidateName;
            $data['jobTitle'] = $job_title;
            $data['companyName'] = $companyname;
            //$to = env('MAIL_TO_ADDRESS') ?:"info@onecrate.com";
            $to = $candidateEmail;
            Mail::to($to)->send(new AcceptJobOffer($data));

            //returning back the data
            return $companyList;

        } else {
            return response()->json(['errors' => ['msg' => ['Candidate Does Not Exist.']]], 500);
        }

    }

    public function rejectOffer(Request $request)
    {
        $applicantId = $request->applicantId;
        $candidate = Candidate::where('user_id', Auth::user()->id)->first();
        if ($candidate) {
            $candidate_id = $candidate->id;

            $jobDetail = JobApplicant::with(['candidate', 'job.title', 'candidate.user' => function ($query) {
                $query->select('id', 'first_name', 'last_name', 'email');
            }, 'job' => function ($query) {
                $query->select('id', 'job_key', 'job_title_id', 'company_id');
            }, 'job.company', 'job.company.user' => function ($select) {
                $select->select('id', 'first_name', 'last_name', 'email');
            },
            ])->where('id', $applicantId)->first();
            //dd($jobDetail);

            $jobApplicant = JobApplicant::find($applicantId);
            $jobApplicant->candidate_status = 0;
            $jobApplicant->save();

            $jobId = $jobDetail->job_id;
            $candidateName = $jobDetail->candidate->user->first_name;
            $candidateEmail = $jobDetail->candidate->user->email;
            $companyname = $jobDetail->job->company->name;
            $companyEmail = $jobDetail->job->company->user->email;
            $job_title = $jobDetail->job->title->name;
            $data = [];
            $data['candidateName'] = $candidateName;
            $data['jobTitle'] = $job_title;
            $data['companyName'] = $companyname;
            //$to = env('MAIL_TO_ADDRESS') ?:"info@onecrate.com";
            $to = $companyEmail;
            Mail::to($to)->send(new RejectJobOffer($data));

            // Return data result:
            $candidate = Candidate::where('user_id', Auth::user()->id)->first();
            $candidate_id = $candidate->id;
            $companyList = JobApplicant::with(['job.company' => function ($selectCompany) {
                $selectCompany = $selectCompany->select('id', 'name', 'user_id');
            }, 'job.company.user' => function ($select) {
                $select->select('id', 'image', 'email');
            }, 'job' => function ($query) {
                $query = $query->select('id', 'company_id', 'status');
            }])->where('candidate_id', $candidate_id)->where('is_offered', 1)
                ->where('candidate_status', null)->get();
            return $companyList;

        }
    }

    public function getInterestedCandidates()
    {
        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $company_id = $user->company->id;
        $pendingList = JobApplicant::with(['candidate.fieldOfStudy',
            'candidate.user' => function ($select) {
                $select = $select->select('id', 'first_name', 'last_name', 'email', 'image');
            },
            'job' => function ($query) use ($company_id) {
                $query = $query->select('id', 'company_id');
            }])
            ->whereHas('job.company', function ($query) use ($company_id) {
                $query->where('id', $company_id);
            })
            ->groupBy('candidate_id')->whereNull('candidate_status')->get();
        return $pendingList;
    }

    public function removeInterestedCandidate(Request $request)
    {
        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $company_id = $user->company->id;
        if ($company_id) {
            $id = $request->id;
            $jobApplicant = JobApplicant::where('id', $id)->first();

            $jobApplicant = JobApplicant::where('candidate_id', $jobApplicant->candidate_id)->whereHas('job.company', function ($query) use ($company_id) {
                $query->where('id', $company_id);
            })->update(['candidate_status' => 0]);

            $responseData = JobApplicant::with(['candidate.fieldOfStudy',
                'candidate.user' => function ($select) {
                    $select = $select->select('id', 'first_name', 'last_name', 'email', 'image');
                },
                'job' => function ($query) use ($company_id) {
                    $query = $query->select('id', 'company_id')->where('company_id', $company_id);
                }])
                ->whereHas('job.company', function ($query) use ($company_id) {
                    $query->where('id', $company_id);
                })
                ->groupBy('candidate_id')->whereNull('candidate_status')->get();
            return $responseData;
        }

    }

    public function removeCandidateSidebarJob(Request $request)
    {
        $candidate = Candidate::where('user_id', Auth::user()->id)->first();
        if ($candidate) {
            $candidate_id = $candidate->id;
            $id = $request->id;
            $updateJobApplicant = JobApplicant::find($id);
            $updateJobApplicant->candidate_viewable = 0;
            $updateJobApplicant->save();

            $appliedJobs = JobApplicant::with(['candidate', 'job.qualification', 'job.country', 'job.state', 'job.city', 'job.title',
                'job.company.user' => function ($query) {
                    $query->select('id', 'image');
                }
                , 'job' => function ($query) {
                    $query->where('status', 1)->limit(10)->select('id', 'job_key', 'job_title_id', 'qualification_id', 'company_id',
                        'country_id', 'state_id', 'city_id', 'status');
                },
            ])->where('candidate_id', $candidate_id)->where('candidate_viewable', 1)->get();

            return $appliedJobs;
        }

    }

}
