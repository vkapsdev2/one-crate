<?php

namespace App\Http\Controllers;

use App\Company;
use App\Job;
use App\JobCompanyTemplate;
use App\JobCompanyTemplateReward;
use App\JobCompanyTemplateSkill;
use App\JobTemplate;
use App\JobTitle;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use PDF;

class JobTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Builder[]|Collection
     */
    public function getJobTemplates(Request $request)
    {
        try {
            $company_id = Company::where('user_id', Auth::user()->id)->value('id');

            $user = User::with(['company'])->where('id', Auth::user()->id)->first();
            $jobTemplates = JobCompanyTemplate::with(['title'])
                ->join('job_titles', 'job_titles.id', '=', 'job_company_templates.job_title_id')
                ->where('job_company_templates.company_id', $company_id)
                ->orderBy('job_titles.name')->select('job_company_templates.*')
                ->paginate(50);

            // $jobTemplates = JobTemplate::with(['title'])
            //     ->join('job_titles','job_titles.id','=','job_templates.job_title_id')
            //     ->where('job_templates.company_id', $company_id)
            //     ->orderBy('job_titles.name')->select('job_templates.*')
            //     ->paginate(50);

            // return $jobTemplates;
            return response()->json($jobTemplates);
            // $candidateList = CompanyMember::with(['candidate.user'])->where('company_id',$company_id)->where('job_id',null)->get();
        } catch (Exception $error) {
            return response()->$error;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        Validator::make($request->all(), [
            'job_title_id' => ['required', 'numeric'], //'unique:job_company_templates,job_title_id'
            'job_category_id' => ['required', 'numeric'],
            'department_id' => ['required', 'numeric'],
            'qualification_id' => ['required', 'numeric'],
            'job_experience_id' => ['required', 'numeric'],
            'job_type_id' => ['required', 'numeric'],
            'job_salary_term_id' => ['required', 'numeric'],
            'job_salary_type_id' => ['required'],
            'description' => ['required', 'string'],
            'responsibilities' => ['required', 'string'],
            'required_skills' => ['required', 'string'],
        ])->validate();

        $request->skillsList != "" ? $skills = explode(',', $request->skillsList) : $skills = null;

        $user = User::with(['company'])->where('id', Auth::user()->id)->first();

        //job title
        $jobTitleId = $request->job_title_id;
        $jobIndex = 1;
        $existingJobTemplate = JobCompanyTemplate::where(['company_id' => $user->company->id, 'job_title_id' => $request->job_title_id])->latest('created_at')->first();
        if ($existingJobTemplate) {

            $existingTitle = JobTitle::where('id', $request->job_title_id)->first();
            $maxTitle = JobTitle::where('name', '=', $existingTitle->name)->latest('job_index')->first();
            $jobIndex = $maxTitle->job_index + 1;

            $newJobTitle = new JobTitle();
            $newJobTitle->name = ($existingTitle->name);
            $newJobTitle->company_id = $user->company->id;
            $newJobTitle->job_index = $jobIndex;
            $newJobTitle->is_public = 0; //fixme
            $newJobTitle->save();
            $jobTitleId = JobTitle::where(['name' => $existingTitle->name, 'job_index' => $jobIndex])->first()->id;
        } else {
            $existingTitle = JobTitle::where('id', $request->job_title_id)->first();
            $maxTitle = JobTitle::where('name', '=', $existingTitle->name)->latest('job_index')->first();
            $jobIndex = $maxTitle->job_index + 1;

            $newJobTitle = new JobTitle();
            $newJobTitle->name = ($existingTitle->name);
            $newJobTitle->company_id = $user->company->id;
            $newJobTitle->job_index = $jobIndex;
            $newJobTitle->is_public = 0; //fixme
            $newJobTitle->save();
            $jobTitleId = JobTitle::where(['name' => $existingTitle->name, 'job_index' => $jobIndex])->first()->id;
        }

        $jobTemplate = new JobCompanyTemplate();
        $jobTemplate->job_key = Str::uuid();
        $jobTemplate->added_by = $user->username;
        $jobTemplate->job_title_id = $jobTitleId;
        $jobTemplate->job_index = $jobIndex;
        $jobTemplate->company_id = $user->company->id;
        $jobTemplate->department_id = $request->department_id;
        $jobTemplate->job_category_id = $request->job_category_id;
        $jobTemplate->job_experience_id = $request->job_experience_id;
        $jobTemplate->qualification_id = $request->qualification_id;
        $jobTemplate->job_type_id = $request->job_type_id;
        $jobTemplate->job_salary_term_id = $request->job_salary_term_id;
        $jobTemplate->description = $request->description;
        $jobTemplate->responsibilities = $request->responsibilities;
        $jobTemplate->required_skills = $request->required_skills;
        $jobTemplate->country_id = $request->country_id;
        $jobTemplate->state_id = $request->state_id;
        $jobTemplate->city_id = $request->city_id;

        if ($request->min_salary && $request->min_salary) {
            $jobTemplate->min_salary = $request->min_salary;
            $jobTemplate->max_salary = $request->max_salary;
            $jobTemplate->job_salary_type_id = $request->job_salary_type_id;
        } else {
            $jobTemplate->job_salary_type_id = $request->job_salary_type_id;
        }

        $jobTemplate->save();

        // Saving job skills
        // if($skills) {
        //     foreach ($skills as $skill) {
        //         $jobSkill = new JobCompanyTemplateSkill();
        //         $jobSkill->job_company_template_id = $jobTemplate->id;
        //         $jobSkill->skill_id = $skill;
        //         $jobSkill->save();
        //     }
        // }

        // Return data
        $jobTemplates = JobCompanyTemplate::with(['title'])
            ->join('job_titles', 'job_titles.id', '=', 'job_company_templates.job_title_id')
            ->orderBy('job_titles.name')->select('job_company_templates.*')
            ->where('job_company_templates.company_id', $user->company->id)
            ->paginate(50);
        return $jobTemplates;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $templateId
     * @return Builder|Model|object|null
     */
    public function edit($templateId)
    {
        $jobDetails = JobCompanyTemplate::with(['title', 'company.user' => function ($select) {
            $select->select('id', 'image');
        }, 'department', 'rewards.reward', 'skills.skill',
            'category', 'experience', 'qualification', 'jobType', 'salaryType', 'country', 'state', 'city'])
            ->where('id', $templateId)
            ->first();

        return $jobDetails;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $templateId
     * @return Builder|Model|object|null
     */
    public function show($templateId)
    {
        $jobDetails = JobCompanyTemplate::with(['title', 'company.user' => function ($select) {
            $select->select('id', 'image');
        }, 'department', 'rewards.reward', 'skills.skill',
            'category', 'experience', 'qualification', 'jobType', 'salaryTerm', 'salaryType', 'country', 'state', 'city'])
            ->where('id', $templateId)
            ->first();

        return $jobDetails;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $templateId
     * @return Builder|Model|object|null
     */
    public function getJobTemplateByJobTitle($jobTitleId)
    {
        $existingTitle = JobTitle::where('id', $jobTitleId)->first();
        $jobDetails = null;

        if ($existingTitle->job_index > 0) {

            $jobDetails = JobCompanyTemplate::with(['title', 'company.user' => function ($select) {
                $select->select('id', 'image');
            }, 'department', 'rewards.reward', 'skills.skill',
                'category', 'experience', 'qualification', 'jobType', 'salaryType', 'country', 'state', 'city'])
                ->where(['job_title_id' => $jobTitleId, 'job_index' => $existingTitle->job_index])
                ->first();
        } else {
            $jobDetails = JobTemplate::with(['title', 'company.user' => function ($select) {
                $select->select('id', 'image');
            }, 'department', 'rewards.reward', 'skills.skill',
                'category', 'experience', 'qualification', 'jobType', 'salaryType', 'country', 'state', 'city'])
                ->where('job_title_id', $jobTitleId)
                ->first();
        }

        return $jobDetails;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return Builder|Model|object|null
     */
    public function update(Request $request)
    {

        $data = $request->all();

        Validator::make($data, [
            'job_title_id' => ['required', 'numeric'],
            'job_category_id' => ['required', 'numeric'],
            'department_id' => ['required', 'numeric'],
            'qualification_id' => ['required', 'numeric'],
            'job_experience_id' => ['required', 'numeric'],
            'job_type_id' => ['required', 'numeric'],
            'job_salary_term_id' => ['required', 'numeric'],
            'job_salary_type_id' => ['required'],
            'description' => ['required', 'string'],
            'responsibilities' => ['required', 'string'],
            'required_skills' => ['required', 'string'],
            'job_template_id' => ['required', 'numeric'],
        ])->validate();

        if (isset($request->skillsList)) {
            $skills = explode(',', $request->skillsList);
        } else {
            $skills = null;
        }

        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $jobTemplate = JobCompanyTemplate::findOrFail($request->job_template_id);
        if ($jobTemplate) {
            $jobTemplate->job_title_id = $request->job_title_id;
            // $jobTemplate -> added_by = $request->added_by;
            $jobTemplate->company_id = $user->company->id;
            $jobTemplate->department_id = $request->department_id;
            $jobTemplate->job_category_id = $request->job_category_id;
            $jobTemplate->job_experience_id = $request->job_experience_id;
            $jobTemplate->qualification_id = $request->qualification_id;
            $jobTemplate->job_type_id = $request->job_type_id;
            $jobTemplate->job_salary_term_id = $request->job_salary_term_id;
            $jobTemplate->description = $request->description;
            $jobTemplate->responsibilities = $request->responsibilities;
            $jobTemplate->required_skills = $request->required_skills;
            $jobTemplate->country_id = $request->country_id;
            $jobTemplate->state_id = $request->state_id;
            $jobTemplate->city_id = $request->city_id;

            if ($request->min_salary && $request->min_salary) {
                $jobTemplate->min_salary = $request->min_salary;
                $jobTemplate->max_salary = $request->max_salary;
                $jobTemplate->job_salary_type_id = $request->job_salary_type_id;
            } else {
                $jobTemplate->job_salary_type_id = $request->job_salary_type_id;
            }

            $jobTemplate->save();

            //Deleting old skills
            $oldSkills = JobCompanyTemplateSkill::where('job_company_template_id', $jobTemplate->id)->delete();
            //Saving job skills
            if ($skills) {
                foreach ($skills as $skill) {
                    $jobSkill = new JobCompanyTemplateSkill();
                    $jobSkill->job_company_template_id = $jobTemplate->id;
                    $jobSkill->skill_id = $skill;
                    $jobSkill->save();
                }
            }

            if ($request->rewards) {
                foreach ($request->rewards as $reward) {
                    $jobReward = new JobCompanyTemplateReward();
                    $jobReward->job_company_template_id = $jobTemplate->id;
                    $jobReward->reward_id = $reward;
                    $jobReward->save();
                }
            }

            // Return data
            $jobTemplates = JobCompanyTemplate::with(['title'])
                ->join('job_titles', 'job_titles.id', '=', 'job_company_templates.job_title_id')
                ->orderBy('job_titles.name')->select('job_company_templates.*')
                ->where('job_company_templates.company_id', $user->company->id)
                ->paginate(50);
            return $jobTemplates;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $templateId
     * @return Builder[]|Collection
     */
    public function destroy($templateId)
    {
        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $jobTemplate = JobCompanyTemplate::findOrFail($templateId);
        $jobTitle = JobTitle::findOrFail($jobTemplate->job_title_id);

        if ($jobTemplate && $jobTemplate->company_id == $user->company->id) {
            $jobTemplate->delete();
            $jobTitle->delete();

            // Return data
            $jobTemplates = JobCompanyTemplate::with(['title'])
                ->join('job_titles', 'job_titles.id', '=', 'job_company_templates.job_title_id')
                ->orderBy('job_titles.name')->select('job_company_templates.*')
                ->where('job_company_templates.company_id', $user->company->id)
                ->paginate(50);

            return $jobTemplates;
        }
    }

    /**
     * Search job title from storage.
     */
    public function searchJobsTemplate(Request $request)
    {
        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $jobList = JobCompanyTemplate::with(['title'])
            ->select('id', 'job_key', 'job_title_id', 'job_category_id', 'department_id', 'updated_at', 'added_by')
        //->where('status',1)
            ->groupBy('job_title_id');

        if ($request->filled("jobTitle")) {
            $jobList->whereHas('title', function ($query) use ($request) {
                $query->where('id', $request->jobTitle);
            });
        }
        $jobList = $jobList->paginate(50);
        return $jobList;
    }

    /**
     * Search job title from storage.
     */
    public function searchJobsTemplateAuthor(Request $request)
    {
        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $author = $request->author;
        $jobList = JobTemplate::with(['title'])
            ->select('id', 'job_key', 'job_title_id', 'job_category_id', 'department_id', 'updated_at', 'added_by')
            ->where('added_by', $author)
            ->groupBy('job_title_id');

//        if($request->filled("jobTitle")){
        //            $jobList->whereHas('title', function ($query) use ($request) {
        //                $query->where('id', $request->jobTitle);
        //            });
        //        }
        $jobList = $jobList->paginate(50);
        return $jobList;
    }

    /**
     * Download job detail in pdf .
     *
     * @param  \App\Job  $job
     * @return Response
     */
    public function downloadJobTemplateDetails($template_id)
    {

        $jobDetails = JobCompanyTemplate::with([
            'company.user' => function ($select) {
                $select->select('id', 'image', 'country_id', 'state_id', 'city_id');
            },
            'department', 'rewards.reward', 'skills.skill', 'category', 'experience', 'title',
            'qualification', 'jobType', 'salaryTerm', 'salaryType', 'country', 'state', 'city',
            'company.user.country', 'company.user.state', 'company.user.city',
        ])
            ->where('id', $template_id)
            ->first();
        // dd($jobDetails);
        $data['jobDetail'] = $jobDetails;
        $jobTitle = $jobDetails->title->name;
        // Send data to the view using loadView function of PDF facade
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'dpi' => 150,
            'defaultFont' => 'Poppins'])->loadView('pdf.job', $data)->setPaper('letter', 'portrait');
        // return $pdf->stream();
        // $pdf->save(storage_path().'_filename.pdf');
        // Finally, you can download the file using download function
        return $pdf->download($jobTitle . '.pdf');
        // return view('pdf.job')->with('jobDetail',$jobDetails);
    }
}
