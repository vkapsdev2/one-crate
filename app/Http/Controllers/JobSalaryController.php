<?php

namespace App\Http\Controllers;

use App\JobSalary;
use Illuminate\Http\Request;

class JobSalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobSalary  $jobSalary
     * @return \Illuminate\Http\Response
     */
    public function show(JobSalary $jobSalary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobSalary  $jobSalary
     * @return \Illuminate\Http\Response
     */
    public function edit(JobSalary $jobSalary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobSalary  $jobSalary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobSalary $jobSalary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobSalary  $jobSalary
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobSalary $jobSalary)
    {
        //
    }
}
