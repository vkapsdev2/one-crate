<?php

namespace App\Http\Controllers;

use App\CancelSubscriptionRequest;
use App\PaymentHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CancelSubscriptionRequestController extends Controller
{
    public function cancelSubscriptionRequest(){
        $user = Auth::user();
        $paymentHistory = PaymentHistory::where('user_id',$user->id)->where('current',1)->first();
        $previousCancellationRequest = CancelSubscriptionRequest::where('user_id',$user->id);
        if($paymentHistory){
            if($previousCancellationRequest){
                $end_date = Carbon::createFromDate($paymentHistory->end_date)->toFormattedDateString();
                $previousCancellationRequest->update([
                    'due_date' => $paymentHistory -> end_date
                ]);
                return response()->json(['msg' => 'Request already received, your subscription will end on '.$end_date, $previousCancellationRequest], 200);
            }
        
            $end_date = Carbon::createFromDate($paymentHistory->end_date)->toFormattedDateString();
            $cancellationRequest = new CancelSubscriptionRequest();
            $cancellationRequest -> user_id = $user->id;
            $cancellationRequest -> due_date = $paymentHistory -> end_date;
            $cancellationRequest -> save();
            return response()->json(['msg' => 'Request Received, your subscription will end on '.$end_date], 200);
        }else{
            return response()->json(['error' => 'Payment History Not Found'], 501);
        }
    }
}
