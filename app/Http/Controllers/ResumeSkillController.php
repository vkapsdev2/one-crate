<?php

namespace App\Http\Controllers;

use App\Resume;
use Illuminate\Http\Request;
use App\ResumeSkill;
use App\Skill;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
class ResumeSkillController extends Controller
{
    /**
     * add resume education.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeSkill|array
     */
    public function setResumeSkill(Request $request){

        Validator::make($request->all(), [
            'tags' => ['required'],
        ])->validate();

        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }
        $resumeSkill = new ResumeSkill();
//dd($request ->skill_id);
        $tagsArray= explode(',', $request->tags);
        foreach ($tagsArray as $key => $skill){
            //echo $skill;
            $olderSkill = ResumeSkill::where([
                'resume_id'=>$resume->id,
                'skill_id'=>$skill
            ])->first();
            if(!$olderSkill){
                $resumeSkill = new ResumeSkill();
                $resumeSkill -> resume_id = $resume -> id;
                $resumeSkill -> skill_id = $skill;
                $resumeSkill ->save();
            }
        }

//        if($request ->skill_id === null) {
//            $skill = new Skill();
//            $skill->name = $request->searched_query;
//            $skill->is_public = 0;
//            $skill->save();
//            $skill_id = $skill->id;
//          $resumeSkill->resume_id = $resume->id;
//         $resumeSkill->skill_id = $skill_id;
//            $resumeSkill->save();
//        } else {
//            $resumeSkill->resume_id = $resume->id;
//
//            $resumeSkill->skill_id = $request->skill_id;
//            $resumeSkill->save();
//        }

        $resumeSkill = ResumeSkill::with(['skill'])->where('resume_id',$resume ->id)
            ->orderBy('sort_order', 'asc')->get();
        return $resumeSkill;
    }

    public function getResumeSkill(){
        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if($resume) {
            $resumeSkill = ResumeSkill::with(['skill','resume'])->where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();
            //dd($resumeSkill);
            return $resumeSkill;
        }else{
            return response()->json(['errors' => ['msg' => ['Resume Does Not Exist.']]], 500);
        }
    }

    public function deleteSkill($id){
        $resumeSkill = ResumeSkill::find($id);
        if($resumeSkill){
            $resume = Resume::where('user_id',Auth::user()->id)->first();
            $resumeSkill -> delete();

            $resumeSkill = ResumeSkill::with(['skill'])->where('resume_id',$resume ->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeSkill;

        }else{
            return null;
        }

    }

    /**
     * update resume skill sort order.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeSkill|array
     */
    public function sortSkills(Request $request)
    {
        $resume = Resume::where('user_id', Auth::user()->id)->first();
        if ($resume) {
            $sortedarray = $request->sortedarray;
            foreach ($sortedarray as $key => $value) {
                $resumeSkill = ResumeSkill::find($value['id']);
                $resumeSkill->resume_id = $resume->id;
                $resumeSkill->sort_order = $value['sort_order'];
                $resumeSkill->save();
            }
            $resumeSkill = ResumeSkill::with(['skill'])->where('resume_id',$resume ->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeSkill;

        } else {
            return null;
        }
    }
}
