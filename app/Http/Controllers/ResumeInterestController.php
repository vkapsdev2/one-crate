<?php

namespace App\Http\Controllers;

use App\Resume;
use App\ResumeInterest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResumeInterestController extends Controller
{

    public function getResumeInterest(){
        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if($resume) {
            $resumeInterest = ResumeInterest::with(['resume'])->where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeInterest;
        }else{
            return response()->json(['errors' => ['msg' => ['Resume Does Not Exist.']]], 500);
        }
    }

    /**
     * add resume education.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeInterest|array
     */
    public function setResumeInterest(Request $request){


        Validator::make($request->all(), [
            'tags' => ['required', 'string'],
        ])->validate();

        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }
        $tagsArray= explode(',', $request->tags);
        foreach ($tagsArray as $interest){
            $oldStrength = ResumeInterest::where([
                'resume_id'=>$resume->id,
                'name'=>$interest
            ])->first();
            if(!$oldStrength){
                $resumeInterests = new ResumeInterest();
                $resumeInterests -> resume_id = $resume -> id;
                $resumeInterests -> name = $request ->tags;
                $resumeInterests ->save();
            }
        }
        $resumeInterests = ResumeInterest::with(['resume'])->where('resume_id', $resume->id)
            ->orderBy('sort_order', 'asc')->get();
        return $resumeInterests;
    }

    public function deleteInterest($id){
        $resumeInterest = ResumeInterest::find($id);
        if($resumeInterest){
            $resume = Resume::where('user_id',Auth::user()->id)->first();
            $resumeInterest -> delete();

            $resumeInterests = ResumeInterest::with(['resume'])->where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeInterests;

        }else{
            return null;
        }

    }

    /**
     * update resume interest sort order.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeEducation|array
     */
    public function sortInterest(Request $request)
    {
        $resume = Resume::where('user_id', Auth::user()->id)->first();
        if ($resume) {
            $sortedarray = $request->sortedarray;
            foreach ($sortedarray as $key => $value) {
                $resumeInterest = ResumeInterest::find($value['id']);
                $resumeInterest->resume_id = $resume->id;
                $resumeInterest->sort_order = $value['sort_order'];
                $resumeInterest->save();
            }
            $resumeInterests = ResumeInterest::with(['resume'])->where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeInterests;

        } else {
            return null;
        }
    }
}
