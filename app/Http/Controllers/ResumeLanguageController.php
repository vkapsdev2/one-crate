<?php

namespace App\Http\Controllers;

use App\Resume;
use App\ResumeLanguage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResumeLanguageController extends Controller
{

    /**
     * add resume Language.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeLanguage|array
     */
    public function setResumeLanguage(Request $request){

        Validator::make($request->all(), [
            'language_id_value' => ['required'],
            'proficiency_id_value' => ['required'],
        ])->validate();

        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }

        $resumeLanguage = new ResumeLanguage();
        $resumeLanguage -> resume_id = $resume -> id;
        $resumeLanguage -> language_id = $request -> language_id_value;
        $resumeLanguage -> proficiency_id = $request -> proficiency_id_value;
        $resumeLanguage ->save();
        $resumeLanguage = ResumeLanguage::with(['proficiency','language'])->where('resume_id',$resume ->id)
            ->orderBy('sort_order', 'asc')->get();
        return $resumeLanguage;
    }

    /**
     * update resume Language.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeLanguage|array
     */
    public function updateResumeLanguage(Request $request){

        Validator::make($request->all(), [
            'language_id_value' => ['required'],
            'proficiency_id_value' => ['required'],
        ])->validate();
        $resumeLanguage = ResumeLanguage::find($request->id);
        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }
        if($resumeLanguage && $resume){
        $resumeLanguage = ResumeLanguage::find($request->id);
        $resumeLanguage -> resume_id = $resume -> id;
        $resumeLanguage -> language_id = $request -> language_id_value;
        $resumeLanguage -> proficiency_id = $request -> proficiency_id_value;
        $resumeLanguage ->save();
            $resumeLanguage = ResumeLanguage::with(['proficiency','language'])->where('resume_id',$resume ->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeLanguage;

        }else{
            return null;
        }
    }

    public function getResumeLanguage(){
        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if($resume) {
            $resumeLanguage = ResumeLanguage::with(['proficiency','language','resume'])->where('resume_id', $resume->id)
                ->orderBy('sort_order', 'asc')->get();
            //dd($resumeLanguage);
            return $resumeLanguage;
        }else{
            return response()->json(['errors' => ['msg' => ['Resume Does Not Exist.']]], 500);
        }
    }

    public function deleteLanguage($id){
        $resumeLanguage = ResumeLanguage::find($id);
        if($resumeLanguage){
            $resume = Resume::where('user_id',Auth::user()->id)->first();
            $resumeLanguage -> delete();

            $resumeLanguage = ResumeLanguage::with(['proficiency','language'])->where('resume_id',$resume ->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeLanguage;

        }else{
            return null;
        }

    }

    /**
     * update resume Language sort order.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeLanguage|array
     */
    public function sortResumeLanguage(Request $request){

        $resumeLanguage = ResumeLanguage::find($request->id);
        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if($resume){
            $sortedarray = $request->sortedarray;
            foreach ($sortedarray as $key => $value) {
                $resumeLanguage = ResumeLanguage::find($value['id']);
                $resumeLanguage->resume_id = $resume->id;
                $resumeLanguage->sort_order = $value['sort_order'];
                $resumeLanguage->save();
            }
            $resumeLanguage = ResumeLanguage::with(['proficiency','language'])->where('resume_id',$resume ->id)
                ->orderBy('sort_order', 'asc')->get();
            return $resumeLanguage;

        }else{
            return null;
        }
    }
}
