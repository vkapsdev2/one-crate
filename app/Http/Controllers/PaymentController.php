<?php

namespace App\Http\Controllers;

use App\CancelSubscriptionRequest;
use App\OrganizationChart;
use App\PaymentCycle;
use App\PaymentHistory;
use App\User;
use App\UserPaymentCards;
use App\UserPaymentDetails;
use App\UserTier;
use Carbon\Carbon;
use Cartalyst\Stripe\Stripe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{

    public function updateUserPaymentId(Request $request)
    {
        $paymentMethodID = $request->paymentMethodID;
        $paymentIntentID = $request->paymentIntentID;
        $requestPaymentDetails = json_decode($request->details);
        $cardInfo = json_decode($request->cardInfo);
        if ($paymentMethodID) {
            $userId = Auth::user()->id;
            $userPaymentDetails = UserPaymentDetails::where('user_id', $userId)->first();
            if ($userPaymentDetails) {
                $userPaymentDetails->payment_method_id = $paymentMethodID;
                $userPaymentDetails->save();

                $userCard = UserPaymentCards::where([
                    'user_id' => $userId,
                    'brand' => $cardInfo->brand,
                    'last4' => $cardInfo->last4,
                ])->first();

                if ($userCard) {
                    $userCard->card_id = $cardInfo->id;
                    $userCard->brand = $cardInfo->brand;
                    $userCard->cvc_check = $cardInfo->cvc_check;
                    $userCard->exp_month = $cardInfo->exp_month;
                    $userCard->exp_year = $cardInfo->exp_year;
                    $userCard->funding = $cardInfo->funding;
                    $userCard->last4 = $cardInfo->last4;
                    $userCard->save();

                } else {
                    $newUserCard = new UserPaymentCards();
                    $newUserCard->user_id = $userId;
                    $newUserCard->card_id = $cardInfo->id;
                    $newUserCard->brand = $cardInfo->brand;
                    $newUserCard->cvc_check = $cardInfo->cvc_check;
                    $newUserCard->exp_month = $cardInfo->exp_month;
                    $newUserCard->exp_year = $cardInfo->exp_year;
                    $newUserCard->funding = $cardInfo->funding;
                    $newUserCard->last4 = $cardInfo->last4;
                    $newUserCard->save();
                }
                //Create User Tier and save it in payment history as well
                $alreadySubscribed = UserTier::where('user_id', $userId)->first();
                if ($alreadySubscribed) {
                    $alreadySubscribed->subscription_tier_id = $requestPaymentDetails->subscriptionTier->id;
                    $alreadySubscribed->subscription_tier_node_id = $requestPaymentDetails->id;
                    $alreadySubscribed->payment_cycle_id = PaymentCycle::where('name', $requestPaymentDetails->payableCycle)->first()->id;
                    $alreadySubscribed->payment_status = 'paid';
                    $alreadySubscribed->save();
                } else {
                    $userTier = new UserTier();
                    $userTier->user_id = $userId;
                    $userTier->subscription_tier_id = $requestPaymentDetails->subscriptionTier->id;
                    $userTier->subscription_tier_node_id = $requestPaymentDetails->id;
                    $userTier->payment_cycle_id = PaymentCycle::where('name', $requestPaymentDetails->payableCycle)->first()->id;
                    $userTier->payment_status = 'paid';
                    $userTier->save();
                }

                //updating old payment's status
                PaymentHistory::where('user_id', $userId)->update(['current' => 0]);
                //checking the existing history
                $existingPaymentHistory = PaymentHistory::where('user_id', $userId)->latest('created_at')->first();
                $invoiceNumber = (!$existingPaymentHistory->invoice_number) ? 100001 : $existingPaymentHistory->invoice_number + 1;
                //creating history for this payment
                $paymentHistory = new PaymentHistory();
                $paymentHistory->user_id = $userId;
                $paymentHistory->invoice_number = $invoiceNumber;
                $paymentHistory->payment_option_id = 1; //making it static to 1 for STRIPE , will be made dynamic once we have multiple payment options
                $paymentHistory->transaction_id = $paymentIntentID; //saving payment intent ID for now since we do not have transaction ID
                $paymentHistory->start_date = Carbon::now()->toDateString();
                if ($requestPaymentDetails->payableCycle == "monthly") {
                    $paymentHistory->end_date = Carbon::now()->addMonth()->toDateString();
                } else if ($requestPaymentDetails->payableCycle == "yearly") {
                    $paymentHistory->end_date = Carbon::now()->addYear()->toDateString();
                }
                $paymentHistory->amount = $requestPaymentDetails->payableAmount;
                $paymentHistory->current = 1;
                $paymentHistory->save();

                //saving due date in user payment details as well;
                $userPaymentDetails->due_date = $paymentHistory->end_date;
                $userPaymentDetails->save();

                $userDetails = User::with([
                    'company.businessType', 'company.industry', 'company.memberDetails', 'company.profiles',
                    'company.department', 'company.jobs', 'company.charts', 'candidate.fieldOfStudy.blogCategory',
                    'candidate.companyDetails', 'candidate.job', 'candidate.jobApplications', 'roles', 'country',
                    'state', 'city', 'tier.subscriptionTierNode',
                ])->where('id', $userId)->first();
                $userDetails->full_name = $userDetails->first_name . " " . $userDetails->last_name;
                return $userDetails;
            } else {
                return response()->json(['errors' => ['msg' => ['User Payment Details Not Found.']]], 501);
            }
        } else {
            return response()->json(['errors' => ['msg' => ['Payment Method Not found.']]], 501);
        }

    }

    public function updateUserPaymentData(Request $request)
    {
        $requestPaymentDetails = ($request->details);
        // dd(($request->details));
        if ($requestPaymentDetails) {
            $userId = Auth::user()->id;
            $userPaymentDetails = UserPaymentDetails::where('user_id', $userId)->first();
            if ($userPaymentDetails) {
                //Create User Tier and save it in payment history as well
                $alreadySubscribed = UserTier::where('user_id', $userId)->first();
                if ($alreadySubscribed) {
                    $alreadySubscribed->subscription_tier_id = $requestPaymentDetails['subscriptionTier']['id'];
                    $alreadySubscribed->subscription_tier_node_id = $requestPaymentDetails['id'];
                    $alreadySubscribed->payment_cycle_id = PaymentCycle::where('name', $requestPaymentDetails['payableCycle'])->first()->id;
                    $alreadySubscribed->payment_status = 'paid';
                    $alreadySubscribed->save();
                } else {
                    $userTier = new UserTier();
                    $userTier->user_id = $userId;
                    $userTier->subscription_tier_id = $requestPaymentDetails[subscriptionTier][id];
                    $userTier->subscription_tier_node_id = $requestPaymentDetails[id];
                    $userTier->payment_cycle_id = PaymentCycle::where('name', $requestPaymentDetails['payableCycle'])->first()->id;
                    $userTier->payment_status = 'paid';
                    $userTier->save();
                }

                //updating old payment's status
                PaymentHistory::where('user_id', $userId)->update(['current' => 0]);

                //checking the existing history
                $existingPaymentHistory = PaymentHistory::where('user_id', $userId)->latest('created_at')->first();
                $invoiceNumber = (!$existingPaymentHistory || $existingPaymentHistory->invoice_number) ? 100001 : $existingPaymentHistory->invoice_number + 1;

                //creating history for this payment
                $paymentHistory = new PaymentHistory();
                $paymentHistory->user_id = $userId;
                $paymentHistory->invoice_number = $invoiceNumber;
                $paymentHistory->payment_option_id = 1;
                $paymentHistory->start_date = Carbon::now()->toDateString();
                if ($requestPaymentDetails['payableCycle'] == "monthly") {
                    $paymentHistory->end_date = Carbon::now()->addMonth()->toDateString();
                } else if ($requestPaymentDetails['payableCycle'] == "yearly") {
                    $paymentHistory->end_date = Carbon::now()->addYear()->toDateString();
                }
                $paymentHistory->amount = $requestPaymentDetails['payableAmount'];
                $paymentHistory->current = 1;
                $paymentHistory->save();

                //saving due date in user payment details as well;
                $userPaymentDetails->due_date = $paymentHistory->end_date;
                $userPaymentDetails->save();

                $userDetails = User::with([
                    'company.businessType', 'company.industry', 'company.memberDetails', 'company.profiles',
                    'company.department', 'company.jobs', 'company.charts', 'candidate.fieldOfStudy.blogCategory',
                    'candidate.companyDetails', 'candidate.job', 'candidate.jobApplications', 'roles', 'country',
                    'state', 'city', 'tier.subscriptionTierNode',
                ])->where('id', $userId)->first();
                $userDetails->full_name = $userDetails->first_name . " " . $userDetails->last_name;
                return $userDetails;
            } else {
                return response()->json(['errors' => ['msg' => ['User Payment Details Not Found.']]], 501);
            }
        } else {
            return response()->json(['errors' => ['msg' => ['Payment Method Not found.']]], 501);
        }

    }

    public function deleteUserPaymentData(Request $request)
    {
        try {
            $userTier = UserTier::where('user_id', Auth::user()->id)->delete();
            $userPaymentDetails = UserPaymentDetails::where('user_id', Auth::user()->id)->delete();
            return response()->json(['success' => ['msg' => ['Success for delete payment info.']]], 200);
        } catch (Exception $error) {
            return response()->json(['errors' => ['msg' => ['User Payment Details Not Found.']]], 500);
        }

    }

    public function getPaymentHistory()
    {
        $user = User::with(['paymentHistory' => function ($query) {
            $query->orderBy('created_at', 'DESC');
        }, 'tier.subscriptionTierNode'])->where('id', Auth::user()->id)->first();
        // dd($user);
        $data['paymentHistory'] = $user->paymentHistory;

        if ($user->tier) {
            $data['currentTier'] = $user->tier->subscriptionTierNode;
        } else {
            $data['currentTier'] = 'free';
        }
        return $data;
    }

    public function getUserCurrentPlan()
    {
        try {
            $user = Auth::user();
            $user = User::with('company')->where('id', $user->id)->first();
            $userTier = UserTier::with('subscriptionTier', 'subscriptionTierNode')->where('user_id', $user->id)->first();
            $userTotalCurrentNodes = OrganizationChart::where('company_id', $user->company->id)->get()->count();
            $userPaymentDetails = UserPaymentDetails::where('user_id', $user->id)->first();
            $previousCancellationRequest = CancelSubscriptionRequest::where('user_id', $user->id)->first();
            $existingPaymentHistory = PaymentHistory::where('user_id', $user->id)->latest('created_at')->first();

            $currentStatus = '';
            if ($previousCancellationRequest && (Carbon::createFromDate($previousCancellationRequest->due_date) >= Carbon::createFromDate($existingPaymentHistory->end_date))) {
                if ($previousCancellationRequest->updated_at->gt($existingPaymentHistory->updated_at)) {
                    $currentStatus = 'Resubscribe';
                } else {
                    $currentStatus = 'Cancel';
                }
            } else {
                $currentStatus = 'Cancel';
            }

            $end_date = '';
            if ($userPaymentDetails) {
                $end_date = Carbon::createFromDate($userPaymentDetails->due_date)->toFormattedDateString();
            }
            if ($userTier) {
                $data['current_plan'] = $userTier->subscriptionTier->name;
                $data['current_nodes'] = $userTier->subscriptionTierNode->amount;
                $data['user_total_current_nodes'] = $userTotalCurrentNodes;
                $data['user_total_current_price'] = $userTier->subscriptionTierNode->price;
                $data['user_current_max_positions'] = $userTier->subscriptionTierNode->amount;
                $data['end_date'] = $end_date;
                $data['current_status'] = $currentStatus;

                return $data;
            } else {
                $data['current_plan'] = 'free';
                $data['current_nodes'] = 10;
                $data['user_total_current_nodes'] = $userTotalCurrentNodes;
                $data['user_total_current_price'] = 0;
                $data['user_current_max_positions'] = 10;
                $data['end_date'] = $end_date;
                $data['current_status'] = $currentStatus;

                return $data;
            }
        } catch (Exception $error) {
            return response()->json(['errors' => ['msg' => ['User Plans Not Found.']]], 500);
        }

    }

    public function test()
    {
        $today = Carbon::now();
        $tomorrow = Carbon::now()->addDay()->toDateString();
        dd($tomorrow <= $today);
//        dd(env('STRIPE_API_KEY'));
        $stripe = new Stripe(env('STRIPE_API_KEY'), env('STRIPE_API_VERSION'));
//        $customer = $stripe->customers()->create([
        //            'email' => 'test@future.com',
        //            'name'  => 'Hamza future',
        ////            'payment_method' => 'pm_1GNbf9IrTZ3bRFpEkCXtGCYq'
        //        ]);
        //dd($customer['id']);
        //        $card = $stripe->cards()->create('cus_Gv65ZCTNR2X9I0', "tok_1GNHmLIrTZ3bRFpEaADJxaBn");
        //

//        $paymentIntent = $stripe->paymentIntents()->create([
        //            'customer' => 'cus_GvSQLzpETdDCfb',
        //            'amount' => 50.49,
        //            'currency' => 'usd',
        //            'payment_method_types' => [
        //                'card',
        //            ],
        //        ]);
        //        dd($paymentIntent['client_secret']);
        //
        //pm_1GNbj1IrTZ3bRFpEC6eAGvo5
        //recurring payment
        //        try {
        ////
        ////
        //            $paymentIntent = $stripe->paymentIntents()->create([
        //                'amount' => 69.69,
        //                'currency' => 'usd',
        //                'payment_method_types' => [
        //                    'card',
        //                ],
        //                'customer' => 'cus_GvSaDl61Ajjo85',
        //                'payment_method' => 'pm_1GNbf9IrTZ3bRFpEkCXtGCYq',
        //                'off_session' => true,
        //                'confirm' => true,
        //            ]);
        //
        //            dd("pi",$paymentIntent['id']);
        //        }catch (CardErrorException $e){
        //            dd('error',$e);
        //        }
        //        dd($card);
        //        $charge = $stripe->charges()->create([
        //            'customer' => 'cus_Gv65ZCTNR2X9I0',
        //            'currency' => 'USD',
        //            'amount'   => 50.49,
        //        ]);

//        $paymentMethod = $stripe->paymentMethods()->create([
        //            'type' => 'card',
        //            'card' => [
        //                'number' => '4242424242424242',
        //                'exp_month' => 9,
        //                'exp_year' => 2020,
        //                'cvc' => '314'
        //            ],
        //        ]);
        //        $customer = $stripe->customers()->find('cus_Gv65ZCTNR2X9I0');
        $customers = $stripe->customers()->all();
        dd($stripe, $customers);
    }
}
