<?php

namespace App\Http\Controllers;

use App\Resume;
use App\ResumeVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class ResumeVideoController extends Controller
{
    /**
     * add resume education.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ResumeVideo|array
     */
    public function setResumeVideo(Request $request){
        Validator::make($request->all(), [
            'type' => ['required','string','max:255'],
            'candidateVideo' => ['required'],
        ])->validate();

        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }

            if(\Request::hasFile('candidateVideo')){
                $file = \Request::file('candidateVideo');
                $filename = $file->getClientOriginalName();
                $path = public_path('/uploads/videos/'.Auth::user()->id);
                File::isDirectory($path) or File::makeDirectory($path, 0755, true, true);
                $file->move($path, $filename);
                $filepath = '/uploads/videos/'.Auth::user()->id.'/'.$filename;
                $oldResumeVideo = ResumeVideo::where(['resume_id'=>$resume->id,'title'=>$request->type])->first();
                if($oldResumeVideo){
                    $oldResumeVideo -> path = $filepath;
                    $oldResumeVideo -> save();

                }else{
                    $resumeVideo = new ResumeVideo();
                    $resumeVideo -> resume_id = $resume -> id;
                    $resumeVideo -> title = $request-> type;
                    $resumeVideo -> path = $filepath;
                    $resumeVideo ->save();
                }

            }
        $resumeVideo = ResumeVideo::where('resume_id',$resume->id)->get();
        return $resumeVideo;
    }

    public function getResumeVideo(){
        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }
        $resumeVideo = ResumeVideo::where('resume_id',$resume->id)->get();
        return $resumeVideo;
    }

    public function deleteResumeVideo($type){
        $resume = Resume::where('user_id',Auth::user()->id)->first();
        if(!$resume){
            return ['error' => 'resume does not exist'];
        }
        $video = ResumeVideo::where(['resume_id'=>$resume->id,'title'=>$type])->first();
        if($video){
            $video -> delete();
        }
        $resumeVideo = ResumeVideo::where('resume_id',$resume->id)->get();
        return $resumeVideo;
    }
}
