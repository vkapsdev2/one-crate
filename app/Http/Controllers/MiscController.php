<?php

namespace App\Http\Controllers;

use App\BlogCategory;
use App\BusinessType;
use App\City;
use App\Country;
use App\FieldOfStudy;
use App\Industry;
use App\JobSalaryTerm;
use App\Qualification;
use App\State;
use App\SubscriptionTierNode;
use App\User;
use App\ReferenceType;
use App\Language;
use App\Proficiency;
use App\Skill;
use App\DegreeLevel;
use App\JobCategory;
use App\Department;
use App\JobType;
use App\JobExperience;
use App\JobTitle;
use App\Rewards;
use App\JobSalaryType;
use App\Job;
use App\JobCompanyTemplate;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;

class MiscController extends Controller
{
    public function getJobSalaryType(){
        return JobSalaryType::all();

    }

    public function getJobSalaryTerms(){
        return JobSalaryTerm::all();
    }

    public function getRewards(){
        $rewards = Rewards::all();
        return $rewards;
    }

    public function getJobCategory($query = null){
        if($query){
            $jobcategory = JobCategory::where('name','like',$query.'%')->get();
        }else{
            $jobcategory = JobCategory::orderBy('name', 'asc')->get();
        }

        return $jobcategory;
    }

    public function getJobDepartment(){
        $department = Department::orderBy('name', 'asc')->get();
        return $department;
    }

    public function getJobTypes(){
        $jobtype = JobType::all();
        return $jobtype;
    }

    public function getJobExperience(){
        $jobexperience = JobExperience::all();
        return $jobexperience;
    }

    public function getCountryList(){
        $countries = Country::all();
        return $countries;
    }
    public function getLanguesList(){
        $languages = Language::orderBy('name', 'asc')->get();

        return $languages;
    }

    public function getProfieinecies(){
        $proficiency = Proficiency::all();

        return $proficiency;
    }

    public function getSkills($query = null){
        if($query){
            $skills = Skill::where([['is_public','1'],['name','like',$query.'%']])->get();
        }else{
            $skills = Skill::where('is_public','1')->get();
        }

        return $skills;
    }


    public function getQualificationList(){
        $qualifications = Qualification::all();

        return $qualifications;
    }

    public function getDegreeLevel(){

        $degreelevel = DegreeLevel::all();
        return $degreelevel;
    }

    public function getStateList($country_id = null){
        if($country_id){
            $states = State::where('country_id',$country_id)->get();
        }else{
            $states = State::all();
        }


        return $states;
    }

    public function getStates($query = null){
        if($query){
            $states = State::where('name','like',$query.'%')->get();
        }else{
            $states = State::all();
        }
        return $states;
    }

    public function getCityList($state_id){
        $cities = City::where('state_id',$state_id)->get();

        return $cities;
    }

    public function getCities($query = null){
        if($query){
            $states = City::where('name','like',$query.'%')->get();
        }else{
            $states = City::all();
        }
        return $states;
    }


    public function isValidEmail($email){
        $user = User::where('email',$email)->orWhere('personal_email',$email)->first();

        if($user){
            return ['isValid' => false];
        }else{
            return ['isValid' => true];
        }

    }

    public function getFieldOfStudy($query = null){
        if($query){
            $fieldOfStudy = FieldOfStudy::where('name','like',$query.'%')->get();
        }else{
            $fieldOfStudy = FieldOfStudy::where('status','1')->get();
        }
// dd($fieldOfStudy);
        return $fieldOfStudy;
    }

    public function getBlogCategory($query){
        $blogCategory = BlogCategory::where('name','like',$query.'%')->get();

        return $blogCategory;
    }

    public function getAllBlogCategory(){
        $blogCategory = BlogCategory::all();

        return $blogCategory;
    }

    public function getBusinessType($query){
        $businessType = BusinessType::where('name','like',$query.'%')->get();

        return $businessType;
    }

    public function showBusinessType(){
        $businessType = BusinessType::all();

        return $businessType;
    }
    public function showReferenceType(){
        $referenceType = ReferenceType::all();

        return $referenceType;
    }

    public function getIndustry($query){
        $industries = Industry::where('name','like',$query.'%')->get();

        return $industries;
    }

    public function getIndustryList(){
        $industries = Industry::orderBy('name', 'asc')->get();

        return $industries;
    }

    public function getOnlineJobs(){
        //DB::enableQueryLog(); // Enable query log

        $jobDetails = Job::with([
            'title','company.user','qualification','country','state','city',
            'salaryTerm','salaryType'
        ])
            ->where('status',1)->offset(0)->limit(10)->inRandomOrder()
            ->groupBy('job_title_id')->get();
        //dd(DB::getQueryLog()); // Show results of log

        return $jobDetails;
    }

    public function getJobsList(){
        $jobDetails = Job::with(['title','company.user'=>function($select){
            $select->select('id','image');
        },'qualification','country','state','city','favourites'])
            ->select('id','job_key','job_title_id','qualification_id','company_id','country_id','state_id',
                'city_id','status')
            ->where('status',1)
            ->groupBy('job_title_id')->get();
        return $jobDetails;
    }

    public function jobInfo($job_id){

                   //For Guest Users
            $jobDetails = Job::with(['company.user' => function($select){
                $select->select('id','image');
            },
                'department','rewards.reward','skills.skill','category','experience','title',
                'qualification','jobType','salaryType','country','state','city','favourites'
            ])
                ->where('id',$job_id)
                ->first();
            return $jobDetails;
    }

    public function jobTemplateInfo($job_id){

        //For Guest Users
        $jobDetails = JobCompanyTemplate::with(['company.user' => function($select){
            $select->select('id','image');
        },
            'department','rewards.reward','skills.skill','category','experience','title',
            'qualification','jobType','salaryType','country','state','city'])
            ->where('id',$job_id)
            ->first();
        return $jobDetails;
    }

    public function createAdminRole(){

        $role = Role::create(['name' => 'admin']);
        $user = User::where('email','admin@gmail.com')->first();
        if($user){
            $user->assignRole('admin');
        }
        dd("ada");
    }
    public function createUserTiersRoles(){
        $role = Role::create(['name' => 'free']);
        $role = Role::create(['name' => 'growth']);
        $role = Role::create(['name' => 'enterprise']);

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $user_id
     * @return \Illuminate\Http\Response
     */
    public function showCompanyInfo($user_id)
    {
        $user = User::with(['company.businessType','company.industry','company.memberDetails','company.sections',
            'company.sections.images','company.profiles','company.department','country','state','city'])
            ->where('id', $user_id)->first();

        return $user;
    }

    public function getSubscriptionTierNodesList(){
        return SubscriptionTierNode::all();
    }
}
