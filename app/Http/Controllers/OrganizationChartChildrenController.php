<?php

namespace App\Http\Controllers;

use App\OrganizationChartChildren;
use Illuminate\Http\Request;

class OrganizationChartChildrenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrganizationChartChildren  $organizationChartChildren
     * @return \Illuminate\Http\Response
     */
    public function show(OrganizationChartChildren $organizationChartChildren)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrganizationChartChildren  $organizationChartChildren
     * @return \Illuminate\Http\Response
     */
    public function edit(OrganizationChartChildren $organizationChartChildren)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrganizationChartChildren  $organizationChartChildren
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrganizationChartChildren $organizationChartChildren)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrganizationChartChildren  $organizationChartChildren
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrganizationChartChildren $organizationChartChildren)
    {
        //
    }
}
