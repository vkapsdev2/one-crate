<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanyMember;
use App\Job;
use App\JobCompanyTemplate;
use App\OrganizationChart;
use App\User;
use App\UserMessage;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrganizationChartController extends Controller
{

    public function generateAvatar()
    {
        $avatarArray = [
            '/assets/images/avatar-1.png',
            '/assets/images/avatar-2.png',
            '/assets/images/avatar-3.png',
            '/assets/images/avatar-4.png',
            '/assets/images/avatar-5.png',
        ];
        return $avatarArray[array_rand($avatarArray)];
    }

    /**
     * get the specified resource from storage.
     *
     * @return Builder|Model|object|null
     */
    public function getCompanyChartNodes()
    {
        $user = Auth::user();
        $userCompany = Company::with(['user', 'charts.job.candidate.user', 'charts.job.title', 'charts.job.department'])->where('user_id', $user->id)->first();

        if ($userCompany->charts) {
            $charts = $userCompany->charts;
            $startChat = 0;
            foreach ($charts as $chartKey => $chart) {
                if ($chart->is_root) {
                    $checkMessage = UserMessage::where(function ($query) use ($user, $userCompany) {
                        $query->where("sender_id", $user->id)->where("receiver_id", $userCompany->user_id);
                    })->orWhere(function ($query) use ($user, $userCompany) {
                        $query->where("sender_id", $userCompany->user_id)->where("receiver_id", $user->id);
                    });
                    if ($checkMessage->count() > 0) {
                        $startChat = 1;
                    }
                    $userCompany->charts[$chartKey]->isMsgInit = $startChat;
                } else {
                    if (!empty($chart->job) && !empty($chart->job->candidate) && !empty($chart->job->candidate->user_id)) {
                        $checkMessage = UserMessage::where(function ($query) use ($user, $chart) {
                            $query->where("sender_id", $user->id)->where("receiver_id", $chart->job->candidate->user_id);
                        })->orWhere(function ($query) use ($user, $chart) {
                            $query->where("sender_id", $chart->job->candidate->user_id)->where("receiver_id", $user->id);
                        });

                        if ($checkMessage->count() > 0) {
                            $startChat = 1;
                        }
                        $userCompany->charts[$chartKey]->isMsgInit = $startChat;
                    }
                }
            }
        }
        return $userCompany;

    }

    /**
     * get the specified resource from storage.
     *
     * @return Builder|Model|object|null
     */
    public function getCandidateChartNodes()
    {
        $user = Auth::user();
        $user = User::with(['candidate.companyDetails.company'])->where('id', $user->id)->first();
        $memberStatus = $user->candidate->companyDetails->status;
        if ($memberStatus == 1) {
            $userCompany = Company::with(['user', 'charts.job.candidate.user', 'charts.job.title'])->where('id', $user->candidate->companyDetails->company->id)->first();
            $userCompany->loginUser = $user->id;
            if ($userCompany->charts) {
                $charts = $userCompany->charts;
                foreach ($charts as $chartKey => $chart) {
                    $startChat = 0;
                    if ($chart->is_root == 1) {
                        $checkMessage = UserMessage::where(function ($query) use ($user, $userCompany) {
                            $query->where("sender_id", $user->id)->where("receiver_id", $userCompany->user_id);
                        })->orWhere(function ($query) use ($user, $userCompany) {
                            $query->where("sender_id", $userCompany->user_id)->where("receiver_id", $user->id);
                        });
                        if ($checkMessage->count() > 0) {
                            $startChat = 1;
                        }
                        $userCompany->charts[$chartKey]->isMsgInit = $startChat;
                    } else {
                        if (!empty($chart->job) && !empty($chart->job->candidate) && !empty($chart->job->candidate->user_id)) {
                            $checkMessage = UserMessage::where(function ($query) use ($user, $chart) {
                                $query->where("sender_id", $user->id)->where("receiver_id", $chart->job->candidate->user_id);
                            })->orWhere(function ($query) use ($user, $chart) {
                                $query->where("sender_id", $chart->job->candidate->user_id)->where("receiver_id", $user->id);
                            });

                            if ($checkMessage->count() > 0) {
                                $startChat = 1;
                            }
                            $userCompany->charts[$chartKey]->isMsgInit = $startChat;
                        }
                    }
                }
            }
            return $userCompany;
        } else {
            return response()->json(['message' => 'Not company member']);
        }

    }

    /**
     * add the specified resource to storage.
     *
     * @param Request $request
     * @return Builder|Model|object|null
     */
    public function addNode(Request $request)
    {
        Validator::make($request->all(), [
            'parent_id' => ['required', 'numeric'],
        ])->validate();

        $user = Auth::user();
        $user = User::with(['company'])->where('id', $user->id)->first();
        $chartNode = new OrganizationChart();
        $chartNode->parent_id = $request->parent_id;
        $chartNode->company_id = $user->company->id;
        $chartNode->save();
        return $chartNode;
    }

    /**
     * duplicate the specified node and Job to storage.
     *
     * @param Request $request
     * @return Builder|Model|object|null
     */
    public function duplicateNode(Request $request)
    {
        Validator::make($request->all(), [
            'parent_id' => ['required', 'numeric'],
            'node_id' => ['required', 'numeric'],
        ])->validate();

        $user = Auth::user();
        $user = User::with(['company', 'tier.subscriptionTierNode'])->where('id', $user->id)->first();
        //checking for user node creation limit
        if ($user->tier) {
            $alreadyCreatedNodes = OrganizationChart::where('company_id', $user->company->id)->get();
            $nodesTotal = count($alreadyCreatedNodes) - 1; // minus 1 because we don't count root node
            $nodeLimit = $user->tier->subscriptionTierNode->amount;
            if ($nodesTotal >= $nodeLimit) {
                return response()->json(['error' => 'Cannot duplicate job, Tier Limit Reached.'], 500);
            }
        } else {
            $alreadyCreatedNodes = OrganizationChart::where('company_id', $user->company->id)->get();
            $nodesTotal = count($alreadyCreatedNodes) - 1; // minus 1 because we don't count root node
            $freeNodeLimit = env('FREE_NODES_AMOUNT', 10);
            if ($nodesTotal >= $freeNodeLimit) {
                return response()->json(['error' => 'Cannot duplicate job, Free tier limit reached.'], 500);
            }
        }
        //---------
        $nodeDetails = OrganizationChart::with(['company', 'job.title'])->where('id', $request->node_id)->first();
        if ($nodeDetails->job) {
            //replicating the job first
            $originalJob = Job::where('id', $nodeDetails->job->id)->first();
            $duplicateJob = $originalJob->replicate();
            $duplicateJob->candidate_id = null;
            $duplicateJob->status = 0;
            $duplicateJob->save();

            //creating new node with job
            $chartNode = new OrganizationChart();
            $chartNode->parent_id = $request->parent_id;
            $chartNode->company_id = $user->company->id;
            $chartNode->job_id = $duplicateJob->id;
            $chartNode->dp = $this->generateAvatar();
            $chartNode->save();
            $chartDetails = $chartNode;
            $chartDetails->jobTitle = $nodeDetails->job->title->name;
            return $chartNode;
        } else {
            return response()->json(['errors' => ['msg' => ['Must Have a Job to Replicate']]], 500);
        }
    }
    /**
     * add the specified resource to storage.
     *
     * @param Request $request
     * @return Builder|Model|object|null
     */
    public function changeSupervisor(Request $request)
    {
        Validator::make($request->all(), [
            'draggedNode' => ['required', 'numeric'],
            'droppedNode' => ['required', 'numeric'],
        ])->validate();
        $draggedNode = OrganizationChart::with(['children', 'job.title'])->where('id', $request->draggedNode)->first();
        $parents = [];
        $this->findAllParents($request->droppedNode, $parents);
        if (in_array($request->draggedNode, $parents)) {
            return response()->json(['errors' => ['msg' => ['You cannot move parent under child.']]], 500);
        } else {
            $draggedNode->parent_id = $request->droppedNode;
            $draggedNode->save();

            $droppedNode = OrganizationChart::with(['children', 'job.title', 'job.candidate.user'])->where('id', $request->droppedNode)->first();
            $droppedNodeChildren = [];
            $this->findAllChildren($droppedNode->id, $droppedNodeChildren);
            $droppedNode->childCount = count($droppedNodeChildren);

            $draggedNode = OrganizationChart::with(['children', 'job.title', 'job.candidate.user'])->where('id', $request->draggedNode)->first();
            $draggedNodeChildren = [];
            $this->findAllChildren($draggedNode->id, $draggedNodeChildren);
            $draggedNode->childCount = count($draggedNodeChildren);

            if ($droppedNode->job) {
                $droppedNode->jobTitle = $droppedNode->job->title->name;
            } else {
                $droppedNode->jobTitle = '-';
            }

            $draggedNode->droppedNode = $droppedNode;

            if ($draggedNode->job) {
                $draggedNode->jobTitle = $draggedNode->job->title->name;
            } else {
                $draggedNode->jobTitle = '-';
            }

            return $draggedNode;
        }
    }

    /**
     * change job status
     *
     * @param Request $request
     * @return Builder|Model|object|null
     */
    public function changeJobStatus(Request $request)
    {
        Validator::make($request->all(), [
            'node_id' => ['required', 'numeric'],
        ])->validate();
        $node = OrganizationChart::with(['job.candidate.user'])->where('id', $request->node_id)->first();
        if ($node->job) {
            $job = Job::with(['title'])->where('id', $node->job->id)->first();
            if (!$job->start_date) {
                $job->start_date = Carbon::now();
                $job->end_date = Carbon::now()->addDays(60);
            }
            $job->status == 0 ? $job->status = 1 : $job->status = 0;
            $job->save();

            $node->jobTitle = $job->title->name;
            $node->jobStatus = $job->status;
            return $node;
        } else {
            return response()->json(['errors' => ['msg' => ['You cannot move parent under child.']]], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function deleteNode(Request $request)
    {
        Validator::make($request->all(), [
            'id' => ['required', 'numeric'],
        ])->validate();
        $user = Auth::user();
        $chartNode = OrganizationChart::with(['children', 'parent'])->where('id', $request->id)->first();
        if (!$chartNode->is_root) {
            $updatedNodesList = [];
            foreach ($chartNode->children as $nodeChild) {
                $nodeChild->parent_id = $chartNode->parent->id;
                $nodeChild->save();
//                $nodeData = OrganizationChart::with(['job.title','job.candidate.user'])->where('id',$nodeChild->id)->first();
                //                $updatedNodesList[] = $nodeData;
            }
            $chartNode->delete();
            $chartJob = Job::where('id', $chartNode->job_id)->first();
            if ($chartJob) {
                $jobTemplate = JobCompanyTemplate::where([
                    'job_title_id' => $chartJob->job_title_id,
                    'company_id' => $chartJob->company_id,
                ])->delete();
                $chartJob->delete();
            }
            return response()->json(['deletedNode' => $chartNode, 'updatedNodesList' => $updatedNodesList], 200);
        } else {
            return response()->json(['errors' => ['msg' => ['Root Node Cannot be Deleted.']]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function deleteNodeBranch(Request $request)
    {
        Validator::make($request->all(), [
            'id' => ['required', 'numeric'],
        ])->validate();
        $user = Auth::user();
        $chartNode = OrganizationChart::with(['children', 'parent'])->where('id', $request->id)->first();
        if (!$chartNode->is_root) {
            $nodeChildren = [];
            $this->findAllChildren($chartNode->id, $nodeChildren);
            foreach ($nodeChildren as $nodeChild) {
                $nodeData = OrganizationChart::with(['job'])->where('id', $nodeChild)->first();
                if ($nodeData && $nodeData->job) {
                    $nodeChildJob = Job::where('id', $nodeData->job->id)->delete();
                }
                if ($nodeData) {
                    $nodeData->delete();
                }

            }
            $chartNode->delete();
            $chartJob = Job::where('id', $chartNode->job_id)->first();
            if ($chartJob) {
                $chartJob->delete();
            }
            return response()->json(['deletedNode' => $chartNode, 'children' => $nodeChildren], 200);
        } else {
            return response()->json(['errors' => ['msg' => ['Root Node Cannot be Deleted.']]], 500);
        }
    }

    /**
     * Remove the specified resource from Node and Job in Company.
     *
     * @param Request $request
     * @return Builder|Model|object|null
     */
    public function removeNodeMember(Request $request)
    {
        Validator::make($request->all(), [
            'node_id' => ['required', 'numeric'],
        ])->validate();
        $user = Auth::user();
        $chartNode = OrganizationChart::where('id', $request->node_id)->first();
        //if($chartNode->job_id){

        $job = Job::where('id', $chartNode->job_id)->first();
        $companyMember = CompanyMember::where('job_id', $chartNode->job_id)->first();
        if ($job && $companyMember) {
            $job->candidate_id = null;
            $companyMember->job_id = null;
            $job->save();
            $companyMember->save();
        }
        $chartNode = OrganizationChart::with(['children', 'job.title'])->where('id', $request->node_id)->first();
        $chartNode->jobTitle = $chartNode->job->title->name;
        return $chartNode;
//        }
        //        else{
        //            return response()->json(['errors' => ['msg' => ['No Candidate Assigned.']]], 500);
        //        }
    }

    public function findAllParents($nodeId, &$parents)
    {
        $currentNode = OrganizationChart::with(['parent'])->where('id', $nodeId)->first();
        if (isset($currentNode->parent) && isset($currentNode->parent->id)) {
            $parents[] = $currentNode->parent->id;
            return $this->findAllParents($currentNode->parent->id, $parents);
        }
    }
    public function findAllChildren($nodeId, &$children)
    {
        $node = OrganizationChart::with(['children'])->where('id', $nodeId)->first();
        if (($node->children)) {
            foreach ($node->children as $child) {
                $children[] = $child->id;
                $this->findAllChildren($child->id, $children);
            }
        }
    }
    public function showCompanyProfile($node_id)
    {
        $chart = OrganizationChart::with(['company.user'])->where('id', $node_id)->first();
        $user = User::with(['company.businessType', 'company.industry', 'company.memberDetails', 'company.sections',
            'company.sections.images', 'company.profiles', 'company.department', 'country', 'state', 'city'])
            ->where('id', $chart->company->user->id)->first();

        return $user;
    }

}
