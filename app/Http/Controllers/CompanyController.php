<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanyProfile;
use App\CompanyProfileSection;
use App\CompanyProfileSectionImage;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Image;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $user_id
     * @return \Illuminate\Http\Response
     */
    public function showCompanyInfo($user_id)
    {
        $user = User::with(['company.businessType', 'company.industry', 'company.memberDetails', 'company.sections',
            'company.sections.images', 'company.profiles', 'company.department', 'country', 'state', 'city'])
            ->where('id', $user_id)->first();

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $id
     * @return \Illuminate\Http\Response
     */
    public function showCompanyMap($id)
    {
        $user = User::with(['country', 'state', 'city'])
            ->where('id', $id)->first();

        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }

    public function updateCompanyInfo(Request $request)
    {

//        Validator::make($request->all(), [
        //            'objective_summary' => ['required'],
        //        ])->validate();
        //dd($request->all());
        $user = User::where('id', Auth::user()->id)->first();
        if (!$user) {
            return response()->json(['errors' => ['msg' => ['User does not exist.']]], 401);
        }
        // $user -> first_name = $request -> name;
        $user->username = $request->name;
        $user->email = $user->email;
        $user->address = $request->address;
        $user->phone_number = $request->phone_number;
        $user->city_id = $request->city_id;
        $user->state_id = $request->state_id;
        $user->country_id = $request->country_id;
        $user->zip_code = $request->zip_code;
        //print_r($user);
        $user->save();

        $company = Company::where('user_id', Auth::user()->id)->first();
        if (!$company) {
            return response()->json(['errors' => ['msg' => ['Company does not exist.']]], 401);
        }

        $date = Carbon::createFromDate($request->establish_date, 1, 1);
        $company->business_type_id = $request->business_typeid;
        $company->industry_id = $request->industry_id;
        $company->name = $request->name;
        $company->establish_date = $date;
        $company->total_employees = $request->total_employees;
        $company->save();

        $user = User::with(['company.businessType', 'company.industry', 'company.memberDetails', 'company.profiles', 'company.department', 'company.jobs', 'company.charts',
            'candidate.fieldOfStudy.blogCategory', 'candidate.companyDetails', 'candidate.job', 'candidate.jobApplications', 'roles', 'country', 'state', 'city'])
            ->where('email', $user->email)->first();

        return $user;

    }

    public function addCompanyProfileSection()
    {
        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        if ($user) {
            $newCompanyProfileSection = new CompanyProfileSection();
            $newCompanyProfileSection->heading = "Add title...";
            $newCompanyProfileSection->company_id = $user->company->id;
            $newCompanyProfileSection->save();

            $user = User::with(['company.sections.images'])->where('id', Auth::user()->id)->first();
            return $user->company->sections;
        }
        return null;
    }

    public function deleteCompanyProfileSection($secdionId)
    {
        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $companySection = CompanyProfileSection::where([['id', $secdionId], ['company_id', $user->company->id]])->first();
        if ($companySection) {
            $companyImages = CompanyProfileSectionImage::where('company_profile_section_id', $companySection->id)->get();
            foreach ($companyImages as $image) {
                try {
                    if (unlink(public_path($image->image_path))) {
                        $image->delete();
                    }
                } catch (\Exception $e) {
                    $image->delete();
                }
            }
            $companySection->delete();
            return response()->json(['errors' => ['msg' => ['Section Deleted Successfully.']]], 200);
        } else {
            return response()->json(['errors' => ['msg' => ['Not Authorized']]], 500);
        }
    }

    public function getCompanyProfileSection()
    {
        $user = User::with(['company.sections.images'])->where('id', Auth::user()->id)->first();
        if ($user) {
            return $user->company->sections;
        }
        return null;
    }

    public function getCompanyProfile()
    {
        $user = User::with(['company.profiles.companyMetas'])->where('id', Auth::user()->id)->first();
        if ($user) {
            return $user->company->profiles;
        }
        return null;
    }

    public function setCompanyProfileInfo(Request $request)
    {
        Validator::make($request->all(), [
            'meta' => ['required'],
            'type' => ['required'],
        ])->validate();

        $user = User::with(['company'])->where('id', Auth::user()->id)->first();
        $profile = CompanyProfile::where('company_id', $user->company->id)->first();
        if ($profile) {
            $section = CompanyProfile::where('id', $profile->id)->first();
            if ($request->type == 'heading') {
                if ($request->meta == 1) {
                    $section->meta_one = $request->section_heading;
                } else {
                    $section->meta_two = $request->section_heading;
                }

            } elseif ($request->type = 'desc') {
                if ($request->meta == 1) {
                    $section->meta_one_description = $request->section_desc;
                } else {
                    $section->meta_two_description = $request->section_desc;
                }

            }
            $section->save();
        }

        return $section;
    }
    public function setCompanyProfileSectionTitle(Request $request)
    {
        Validator::make($request->all(), [
            'section_id' => ['required'],
            'section_heading' => ['required'],
        ])->validate();

        $section = CompanyProfileSection::findOrFail($request->section_id);
        $section->heading = $request->section_heading;
        $section->save();

        return $section;
    }

    public function addCompanyProfileSectionImages(Request $request)
    {
        Validator::make($request->all(), [
            'section_id' => ['required'],
        ])->validate();

        if ($request->hasFile('file')) {
            $user = User::with(['company.sections.images'])->where('id', Auth::user()->id)->first();

            $path = public_path('images/companyImages/' . $user->company->id . '/');
            $imgpath = Str::uuid()->toString() . '.' . $request->file->extension();
            File::isDirectory($path) or File::makeDirectory($path, 0755, true, true);
            $img = Image::make($request->file)->save($path . $imgpath, 60);

            $companyProfileSectionImage = new CompanyProfileSectionImage();
            $companyProfileSectionImage->company_profile_section_id = $request->section_id;
            $companyProfileSectionImage->image_path = config('app.asset_path_company') . $user->company->id . '/' . $imgpath;
            $companyProfileSectionImage->save();

            $user = User::with(['company.sections.images'])->where('id', Auth::user()->id)->first();
            return $user->company->sections;
        } else {
            return response()->json(['errors' => ['msg' => ['No File Uploaded.']]], 500);
        }

    }

    public function removeCompanyProfileSectionImages(Request $request)
    {
        Validator::make($request->all(), [
            'image_id' => ['required'],
        ])->validate();

        $image = CompanyProfileSectionImage::where('id', $request->image_id)->first();
        if ($image) {
            $image->delete();
            $section = CompanyProfileSection::with(['images'])->where('id', $image->company_profile_section_id)->first();
            return $section;
        } else {
            return response()->json(['errors' => ['msg' => ['Image not found.']]], 500);
        }
    }

    public function getCompanyMembers($id)
    {
        $company = Company::find($id);
        if ($company) {
            $companyMembers = $company->getCompanyMembers;
            $users = [];
            foreach ($companyMembers as &$member) {
                $candidate = $member->candidate;
                $user = $candidate->user;
                array_push($users, $user);
            }
            return response()->json($users, 200);
        } else {
            return response()->json(['errors' => ['msg' => ['Company not found.']]], 500);
        }
    }

}
